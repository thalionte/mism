package interfaz.experiment;

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * Implementa el controlador de la ventana de di�logo para a�adir los datos de un experimento.
 * @author M� Angeles Broull�n Lozano y Ana M� Fern�ndez Hern�ndez.
 * @version 2.0
 */
public class ControlTest implements ActionListener{

	
	//ATRIBUTOS
	/**
	 * La ventana a controlar.
	 */
	private WindowTest ventanaControlada;
	
	//METODOS
	/**
	 * Constructor con un par�metro. 
	 * @param v la ventana a controlar.
	 */
	public ControlTest(WindowTest v){
		ventanaControlada = v;
		ventanaControlada.setModal(true);
	}
	
	/**
	 * El m�todo que controla las acciones de la ventana.
	 * @param el evento a manejar.
	 */
	public void actionPerformed(ActionEvent e) {
		//cambiamos el cursor por un reloj
		Cursor cur = new Cursor(Cursor.WAIT_CURSOR);
        ventanaControlada.setCursor(cur); 
        
        //ejecutamos la acci�n correspondiente   		
		if (e.getSource().equals(ventanaControlada.aceptar)){
			System.out.println("Ha pulsado aceptar.");
			System.out.println(ventanaControlada.toString());
		}else{
			if (e.getSource().equals(ventanaControlada.cancelar)){
				System.out.println("Ha pulsado cancelar.");
				ventanaControlada.dispose();
			}else{
				
			}
		}
		
		//dejamos el cursor como estaba
		cur = new Cursor(Cursor.DEFAULT_CURSOR);
		//ventanaControlada.desbloquearBotones();
        ventanaControlada.setCursor(cur);
	}
}
