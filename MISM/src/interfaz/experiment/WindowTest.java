package interfaz.experiment;

import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

/**
 * Implementa la ventana de di�logo para a�adir los datos de un experimento.
 * @author M� Angeles Broull�n Lozano y Ana M� Fern�ndez Hern�ndez.
 * @version 2.0
 */
public class WindowTest extends JDialog{
	
	//ATRIBUTOS
	/**
	 * El serial del JFrame.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * La etiqueta de nombre.
	 */
	private JLabel etiquetaNombre;
	/**
	 * La etiqueta de duraci�n.
	 */
	private JLabel etiquetaDuracion;
	/**
	 * La etiqueta de fecha de inicio.
	 */
	private JLabel etiquetaFechaInicio;
	/**
	 * La etiqueta de fecha de finalizaci�n.
	 */
	private JLabel etiquetaFechaFin;
	/**
	 * La etiqueta de ubicaci�n de los datos a guardar.
	 */
	private JLabel etiquetaPath;
	/**
	 * La etiqueta de calidad final.
	 */
	private JLabel etiquetaCalidad;
	/**
	 * El campo de nombre.
	 */
	private JTextField nombre;
	/**
	 * El campo de duraci�n.
	 */
	private JTextField duracion;
	/**
	 * El campo de unidades de duraci�n.
	 */
	private JTextField unidadDuracion;
	/**
	 * El campo de fecha de inicio.
	 */
	private JTextField fechaInicio;
	/**
	 * El campo de fecha de finalizaci�n.
	 */
	private JTextField fechaFin;
	/**
	 * El campo de ubicaci�n de los datos a guardar.
	 */
	private JTextField path;
	/**
	 * El campo de calidad final.
	 */
	private JTextField calidad;
	/**
	 * El boton para aceptar.
	 */
	protected JButton aceptar;
	/**
	 * El boton para cancelar.
	 */
	protected JButton cancelar;
	/**
	 * El idioma seleccionado.
	 */
	private String idioma;
	/**
	 * El controlador de la ventana.
	 */
	private ControlTest control;

	//METODOS
	/**
	 * Constructor con par�metro.
	 * @param s el idioma que emplear�.
	 */
	public WindowTest(String s){
		super();
		idioma = s;		
	}
	
	/**
	 * M�todoque a�ade el controlador a la ventana.
	 * @param control el controlador de la ventana.
	 */
	public void a�adirControlador(ControlTest c){
		control = c;
	}
	
	/**
	 * Genera el contanido de la ventana.
	 */
	public void crearVista(){
		Container container = getContentPane();
		container.setLayout(new FlowLayout());
		
		JPanel cuerpo = crearCuerpo();
		container.add(cuerpo);
		
		//separador
		JSeparator s1 = new JSeparator(SwingConstants.HORIZONTAL);
		container.add(s1);
		
		//a�ade los botones
		JPanel botones = new JPanel();
		botones.setLayout(new FlowLayout());
		aceptar = new JButton("Accept");
		aceptar.addActionListener(control);
		botones.add(aceptar);
		cancelar = new JButton("Cancel");
		cancelar.addActionListener(control);
		botones.add(cancelar);
		container.add(botones);
		
		//visualizaci�n
		setIdioma(idioma);
		if (idioma.equals("ingles")){
			this.setTitle("Data of the test");
		}else{
			this.setTitle("Datos del experimento");			
		}
		setSize(600, 350);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);
		this.pack();
	}
	
	/**
	 * Crea el cuerpo de la ventana.
	 * @return el cuerpo de la ventana.
	 */
	private JPanel crearCuerpo(){
		JPanel panelGrid = new JPanel();
		panelGrid.setLayout(new GridLayout(6,2));
		
		JPanel c1 = new JPanel();
		c1.setLayout(new FlowLayout());
		c1.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		etiquetaNombre = new JLabel("Nombre:");
		c1.add(etiquetaNombre);
		panelGrid.add(c1);
		
		JPanel c2 = new JPanel();
		c2.setLayout(new FlowLayout());
		c2.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		nombre = new  JTextField(20);
		c2.add(nombre);
		panelGrid.add(c2);
		
		JPanel c3 = new JPanel();
		c3.setLayout(new FlowLayout());
		c3.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		etiquetaDuracion = new JLabel("Duracion:");
		c3.add(etiquetaDuracion);
		panelGrid.add(c3);
		
		JPanel c4 = new JPanel();
		c4.setLayout(new FlowLayout());
		c4.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		JPanel panelDuracion = new JPanel();
		duracion = new  JTextField(20);
		unidadDuracion = new JTextField(3);
		panelDuracion.add(duracion);
		panelDuracion.add(unidadDuracion);
		c4.add(panelDuracion);
		panelGrid.add(c4);
		
		JPanel c5 = new JPanel();
		c5.setLayout(new FlowLayout());
		c5.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		etiquetaFechaInicio = new JLabel("Fecha de inicio:");
		c5.add(etiquetaFechaInicio);
		panelGrid.add(c5);
		
		JPanel c6 = new JPanel();
		c6.setLayout(new FlowLayout());
		c6.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		fechaInicio = new  JTextField(20);
		c6.add(fechaInicio);
		panelGrid.add(c6);
		
		JPanel c7 = new JPanel();
		c7.setLayout(new FlowLayout());
		c7.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		etiquetaFechaFin = new JLabel("Fecha de finalizaci�n:");
		c7.add(etiquetaFechaFin);
		panelGrid.add(c7);
		
		JPanel c8 = new JPanel();
		c8.setLayout(new FlowLayout());
		c8.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		fechaFin = new  JTextField(20);
		c8.add(fechaFin);
		panelGrid.add(c8);
		
		JPanel c9 = new JPanel();
		c9.setLayout(new FlowLayout());
		c9.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		etiquetaPath = new JLabel("Ubicacion (path):");
		c9.add(etiquetaPath);
		panelGrid.add(c9);
		
		JPanel c10 = new JPanel();
		c10.setLayout(new FlowLayout());
		c10.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		path = new  JTextField(20);
		c10.add(path);
		panelGrid.add(c10);
		
		JPanel c11 = new JPanel();
		c11.setLayout(new FlowLayout());
		c11.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		etiquetaCalidad = new JLabel("Calidad:");
		c11.add(etiquetaCalidad);
		panelGrid.add(c11);
		
		JPanel c12 = new JPanel();
		c12.setLayout(new FlowLayout());
		c12.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		calidad = new  JTextField(20);
		c12.add(calidad);
		panelGrid.add(c12);
		
		return panelGrid;
	}
	
	/**
	 * M�todo que traduce la interfaz.
	 * @param s el idioma a traducir: ingl�s o espa�ol.
	 */
	public void setIdioma(String s){
		if (s.equals("espa�ol")){
			aceptar.setText("Aceptar");
			cancelar.setText("Cancelar");
			etiquetaNombre.setText("Nombre:");
			etiquetaDuracion.setText("Duraci�n:");
			etiquetaFechaInicio.setText("Fecha de inicio:");
			etiquetaFechaFin.setText("Fecha de fin:");
			etiquetaPath.setText("Ubicaci�n (path):");
			etiquetaCalidad.setText("Calidad:");
			this.setTitle("Datos del experimento");	
		}else{
			aceptar.setText("Accept");
			cancelar.setText("Cancel");
			etiquetaNombre.setText("Name:");
			etiquetaDuracion.setText("Duration:");
			etiquetaFechaInicio.setText("Initial date:");
			etiquetaFechaFin.setText("End date:");
			etiquetaPath.setText("Path:");
			etiquetaPath.setText("Quality:");
			this.setTitle("Data of the test");
		}
	}
	
	/**
	 * Devuelve el estado actual del experimento en una cadena.
	 * @return el estado actual del experimento en una cadena.
	 */
	public String toString(){
		String s = "<Test> ";
		s = s + "<Name> " + nombre.getText() + " <\\Name> ";
		s = s +	"<TimeLength> " + duracion.getText() + " <\\TimeLength> ";
		s = s +	"<TimeLengthUnits> " + unidadDuracion.getText() + " <\\TimeLengthUnits> ";
		s = s + "<Start> " + fechaInicio.getText() + " <\\Start> ";
		s = s + "<End> " + path.getText() + " <\\End> ";
		s = s + "<\\Test> ";
		return s;
	}
	
}
