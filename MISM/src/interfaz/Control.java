package interfaz;

import interfaz.components.ControlAddComponent;
import interfaz.components.WindowAddComponent;
import interfaz.experiment.ControlTest;
import interfaz.experiment.WindowTest;
import interfaz.rules.ControlAddRule;
import interfaz.rules.DialogRulePartOf;
import interfaz.rules.DialogRuleMeassure;
import interfaz.rules.WindowAddRule;

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import model.Auxiliar;
import model.FileManager;
import model.drawDiagram.Vertex;

/**
 * Implementa el controlador de la ventana, que contiene las 4 pesta�as de las 4funcionalidaes del sistema.
 * @author M� Angeles Broull�n Lozano y Ana M� Fern�ndez Hern�ndez.
 * @version 2.0
 */
public class Control implements ActionListener {

	// ATRIBUTOS
	/**
	 * La ventana controlada.
	 */
	private Window ventanaControlada;

	// METODOS
	/**
	 * Constructor con par�metro.
	 * @param win la ventana que ser� controlada mediante esta instancia.
	 */
	public Control(Window win) {
		ventanaControlada = win;
	}

	/**
	 * El m�todo que controla las acciones de la ventana.
	 * @param el evento a manejar.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		// cambiamos el cursor por un reloj
		Cursor cur = new Cursor(Cursor.WAIT_CURSOR);
		ventanaControlada.setCursor(cur);

		// ejecutamos la acci�n correspondiente
		if (e.getSource().equals(ventanaControlada.rbMenuItem2)) {
			System.out.println("Puls� el bot�n de idioma espa�ol");
			ventanaControlada.setIdioma("espa�ol");
		} else {
			if (e.getSource().equals(ventanaControlada.rbMenuItem1)) {
				System.out.println("Puls� el bot�n de idioma ingles");
				ventanaControlada.setIdioma("ingles");
			} else {
				if (e.getSource().equals(ventanaControlada.tabDise�o.botonA�adirComponente)) {
					System.out.println("Puls� el bot�n de a�adir componente");
					WindowAddComponent v = new WindowAddComponent(
							ventanaControlada.getIdioma());
					v.a�adirControlador(new ControlAddComponent(v));
					v.crearVista();
				} else {
					if (e.getSource().equals(ventanaControlada.tabDise�o.botonBorrarComponente)) {
						System.out.println("Puls� el bot�n de borrar componente");
						String busqueda = (String) (ventanaControlada.tabDise�o.areaComponentes.getSelectedValue());
						
						String pregunta = "If you remove an element,\n" +
						"you will delete your whole diagram.\n" +
						"Are you sure?";
						String titulo = "Wait a minute!" ;
						if ((ventanaControlada.getIdioma()).equals("espa�ol")){
							pregunta = "Si elimina un elemento,\n" +
							"borrar� todo el diagrama.\n" +
							"�Est� seguro?";
							titulo = "�Un momento!" ;
						}

						int n = JOptionPane.showConfirmDialog(
								ventanaControlada,
								pregunta,
								titulo,
								JOptionPane.YES_NO_OPTION);
						if (n == 0){
							ventanaControlada.canvas.removeAllAristas();
							ventanaControlada.canvas.removeAllVertices();
							//TODO
							/*
							 * borrar la regla que reponde al nombre de la cadena
							 * en busqueda
							 */
						}
						
					} else {
						if (e.getSource().equals(ventanaControlada.tabDise�o.botonA�adirRegla)) {
							System.out.println("Puls� el bot�n de a�ador regla");
							WindowAddRule v = new WindowAddRule(ventanaControlada.getIdioma());
							v.a�adirControlador(new ControlAddRule(v));
							v.crearVista();
						} else {
							if (e.getSource().equals(ventanaControlada.tabDise�o.botonBorrarRegla)) {
								System.out.println("Puls� el bot�n de borrar regla");
								String busqueda = (String) (ventanaControlada.tabDise�o.areaComponentes.getSelectedValue());
					
								String pregunta = "If you remove a rule,\n" +
										"you will delete your whole diagram.\n" +
										"Are you sure?";
								String titulo = "Wait a minute!" ;
								if ((ventanaControlada.getIdioma()).equals("espa�ol")){
									pregunta = "Si elimina una regla,\n" +
									"borrar� todo el diagrama.\n" +
									"�Est� seguro?";
									titulo = "�Un momento!" ;
								}

								int n = JOptionPane.showConfirmDialog(
									    ventanaControlada,
									    pregunta,
									    titulo,
									    JOptionPane.YES_NO_OPTION);
								if (n == 0){
									ventanaControlada.canvas.removeAllAristas();
									ventanaControlada.canvas.removeAllVertices();
									//TODO
									/*
									 * borrar la regla que reponde al nombre de la cadena
									 * en busqueda
									 */
								}
			
								
							} else {
								if (e.getSource().equals(ventanaControlada.menuItem1)) {
									WindowTest v = new WindowTest(ventanaControlada.getIdioma());
									v.a�adirControlador(new ControlTest(v));
									v.crearVista();
								} else {
									if (e.getSource().equals(ventanaControlada.menuItem2)) {
										ventanaControlada.dispose();
									} else {
										if (e.getSource().equals(ventanaControlada.menuItem3)) {
											System.out.println("Puls� el bot�n de conectar");
											TabConnection v = new TabConnection(ventanaControlada);
											ControlConnection c = new ControlConnection(v);
											v.a�adirControlador(c);
											v.crearVista();
											v.setIdioma(ventanaControlada.getIdioma());
											v.pack();  // para darle tama�o autom�tico a la ventana.
											v.setVisible(true);
										} else {
											if (e.getSource().equals(ventanaControlada.menuItem4)) {
												System.out.println("Puls� el bot�n de exportar");
												TabSave v = new TabSave(ventanaControlada.getDescripcion(), ventanaControlada);
												ControlSave c = new ControlSave(v);
												v.a�adirControlador(c);
												v.crearVista();
												v.setIdioma(ventanaControlada.getIdioma());
												v.pack();  // para darle tama�o autom�tico a la ventana.
												v.setVisible(true);
											} else {
												if (e.getSource().equals(ventanaControlada.menuItem5)) {
													System.out.println("Puls� el bot�n de render");
													TabRender v = new TabRender(ventanaControlada.getNombresPiezas(), ventanaControlada);
													ControlRender c = new ControlRender(v);
													v.a�adirControlador(c);
													v.crearVista();
													v.setIdioma(ventanaControlada.getIdioma());
													v.pack();  // para darle tama�o autom�tico a la ventana.
													v.setVisible(true);
												} else {
													if (e.getSource().equals(ventanaControlada.tabDise�o.botonBorrar)) {
														System.out.println("Ha pulsado borrar canvas");
														ventanaControlada.canvas.removeAllAristas();
														ventanaControlada.canvas.removeAllVertices();
													} else {
														if (e.getSource().equals(ventanaControlada.tabDise�o.botonConsultarComponente)) {
															System.out.println("Ha pulsado consultar componente");
															String busqueda = (String) (ventanaControlada.tabDise�o.areaComponentes.getSelectedValue());
															JFrame consulta = new JFrame();
															//TODO
															/*
															 * consulta el Componente (o sensor o cantidad) que reponde al nombre de la cadena
															 * en busqueda, sustituir la cadena "consultar regla".
															 */
									    	    			JOptionPane.showMessageDialog(consulta,
									    	    				    busqueda,
									    	    				    getMensaje(ventanaControlada.getIdioma(), "consultar componente"),
									    	    				    JOptionPane.PLAIN_MESSAGE);
									    	    			/*
															 * Fin del segmento a modificar
															 */
														} else {
															if (e.getSource().equals(ventanaControlada.tabDise�o.botonConsultarRegla)) {
																System.out.println("Ha pulsado consultar regla");
																String busqueda = (String) (ventanaControlada.tabDise�o.areaReglas.getSelectedValue());
																JFrame consulta = new JFrame(); 
																//TODO
																/*
																 * consulta la regla que reponde al nombre de la cadena
																 * en busqueda, sustituir la cadena "consultar regla"
																 */
										    	    			JOptionPane.showMessageDialog(consulta,
										    	    				    busqueda,
										    	    				    getMensaje(ventanaControlada.getIdioma(), "consultar regla"),
										    	    				    JOptionPane.PLAIN_MESSAGE);
										    	    			/*
																 * Fin del segmento a modificar
																 */
										    	    			
															} else {
																if (e.getSource().equals(ventanaControlada.tabDise�o.botonMaquina)) {
																	System.out.println("Ha pulsado el boton de maquina");
																	JFrame consulta = new JFrame(); 
																	
																	//TODO
																	/*
																	 * el contenido de posibilidades debe ser los nombres de las maquinas en la
																	 * base de datos.
																	 */
																	Object[] posibilidades = {"machine1", "machine2", "machine3"};
																	/*
																	 * Fin del segmento a modificar
																	 */
																	
																	String s = (String)JOptionPane.showInputDialog(
																	                    consulta,
																	                    getMensaje(ventanaControlada.getIdioma(), "selecciona maquina"),
																	                    getMensaje(ventanaControlada.getIdioma(), "selecciona maquina"),
																	                    JOptionPane.PLAIN_MESSAGE,
																	                    null,
																	                    posibilidades,
																	                    "machine1");
																	//Qu� se devuelve.
																	if ((s != null) && (s.length() > 0)) {
																	    System.out.println("Seleccion� " + s + "!");
																	    ventanaControlada.canvas.addNuevoVerticeAuto("machine", s);
																	    setDescripcion();
																	}else{
																		//null/empty.
																		System.out.println("respuesta inv�lida!");
																	}
																} else {
																	if (e.getSource().equals(ventanaControlada.tabDise�o.botonHerramienta)) {
																		System.out.println("Ha pulsado el boton de herramienta");
																		JFrame consulta = new JFrame();    	    			
																		
																		//TODO
																		/*
																		 * el contenido de posibilidades debe ser los nombres de herramientas en la
																		 * base de datos.
																		 */
																		Object[] posibilidades = {"tool1", "tool2", "tool3"};
																		/*
																		 * Fin del segmento a modificar
																		 */
																		
																		String s = (String)JOptionPane.showInputDialog(
																		                    consulta,
																		                    getMensaje(ventanaControlada.getIdioma(), "selecciona herramienta"),
																		                    getMensaje(ventanaControlada.getIdioma(), "selecciona herramienta"),
																		                    JOptionPane.PLAIN_MESSAGE,
																		                    null,
																		                    posibilidades,
																		                    "tool1");
																		//Qu� se devuelve.
																		if ((s != null) && (s.length() > 0)) {
																		    System.out.println("Seleccion� " + s + "!");
																		    ventanaControlada.canvas.addNuevoVerticeAuto("tool", s);
																		    setDescripcion();
																		}else{
																			//null/empty.
																			System.out.println("respuesta inv�lida!");
																		}
																	} else {
																		if (e.getSource().equals(ventanaControlada.tabDise�o.botonPieza)) {
																			System.out.println("Ha pulsado el boton de pieza");
																			JFrame consulta = new JFrame();
																			
																			//TODO
																			/*
																			 * el contenido de posibilidades debe ser los nombres de piezas en la
																			 * base de datos.
																			 */
																			Object[] posibilidades = {"part1", "part2", "part3"};
																			/*
																			 * Fin del segmento a modificar
																			 */
																			
																			String s = (String)JOptionPane.showInputDialog(
																			                    consulta,
																			                    getMensaje(ventanaControlada.getIdioma(), "selecciona pieza"),
																			                    getMensaje(ventanaControlada.getIdioma(), "selecciona pieza"),
																			                    JOptionPane.PLAIN_MESSAGE,
																			                    null,
																			                    posibilidades,
																			                    "part1");
																			//Qu� se devuelve.
																			if ((s != null) && (s.length() > 0)) {
																			    System.out.println("Seleccion� " + s + "!");
																			    ventanaControlada.canvas.addNuevoVerticeAuto("part", s);
																			    setDescripcion();
																			}else{
																				//null/empty.
																				System.out.println("respuesta inv�lida!");
																			}
																		} else {
																			if (e.getSource().equals(ventanaControlada.tabDise�o.botonEnfriamiento)) {
																				System.out.println("Ha pulsado el boton de enfriamiento");
																				JFrame consulta = new JFrame();    	    			
																				
																				//TODO
																				/*
																				 * el contenido de posibilidades debe ser los nombres de sistemas de
																				 * regrigeracion en la base de datos																		 * base de datos.
																				 */
																				Object[] posibilidades = {"cooling1", "cooling2", "cooling3"};
																				/*
																				 * Fin del segmento a modificar
																				 */
																				
																				String s = (String)JOptionPane.showInputDialog(
																				                    consulta,
																				                    getMensaje(ventanaControlada.getIdioma(), "selecciona refrigeracion"),
																				                    getMensaje(ventanaControlada.getIdioma(), "selecciona refrigeracion"),
																				                    JOptionPane.PLAIN_MESSAGE,
																				                    null,
																				                    posibilidades,
																				                    "cooling1");
																				//Qu� se devuelve.
																				if ((s != null) && (s.length() > 0)) {
																				    System.out.println("Seleccion� " + s + "!");
																				    ventanaControlada.canvas.addNuevoVerticeAuto("cooling system", s);
																				    setDescripcion();
																				}else{
																					//null/empty.
																					System.out.println("respuesta inv�lida!");
																				}
																			} else {
																				if (e.getSource().equals(ventanaControlada.tabDise�o.botonSensor)) {
																					System.out.println("Ha pulsado el boton de sensor");
																					JFrame consulta = new JFrame();    	    			
																					
																					//TODO
																					/*
																					 * el contenido de posibilidades debe ser los nombres de sensores en la
																					 * base de datos.
																					 */
																					Object[] posibilidades = {"sensor1", "sensor2", "sensor3"};
																					/*
																					 * Fin del segmento a modificar
																					 */
																					
																					String s = (String)JOptionPane.showInputDialog(
																					                    consulta,
																					                    getMensaje(ventanaControlada.getIdioma(), "selecciona sensor"),
																					                    getMensaje(ventanaControlada.getIdioma(), "selecciona sensor"),
																					                    JOptionPane.PLAIN_MESSAGE,
																					                    null,
																					                    posibilidades,
																					                    "sensor1");
																					//Qu� se devuelve.
																					if ((s != null) && (s.length() > 0)) {
																					    System.out.println("Seleccion� " + s + "!");
																					    ventanaControlada.canvas.addNuevoVerticeAuto("sensor", s);
																					    setDescripcion();
																					}else{
																						//null/empty.
																						System.out.println("respuesta inv�lida!");
																					}
																				} else {
																					if (e.getSource().equals(ventanaControlada.tabDise�o.botonCantidad)) {
																						System.out.println("Ha pulsado el boton de cantidad");
																						JFrame consulta = new JFrame();   	    			
																						
																						//TODO
																						/*
																						 * el contenido de posibilidades debe ser los nombres de cantidades en la
																						 * base de datos.
																						 */
																						Object[] posibilidades = {"cantidad1", "cantidad2", "cantidad3"};
																						/*
																						 * Fin del segmento a modificar
																						 */
																						
																						String s = (String)JOptionPane.showInputDialog(
																						                    consulta,
																						                    getMensaje(ventanaControlada.getIdioma(), "selecciona cantidad"),
																						                    getMensaje(ventanaControlada.getIdioma(), "selecciona cantidad"),
																						                    JOptionPane.PLAIN_MESSAGE,
																						                    null,
																						                    posibilidades,
																						                    "cantidad1");
																						//Qu� se devuelve.
																						if ((s != null) && (s.length() > 0)) {
																						    System.out.println("Seleccion� " + s + "!");
																						    ventanaControlada.canvas.addNuevoVerticeAuto("quantity", s);
																						    setDescripcion();
																						}else{
																							//null/empty.
																							System.out.println("respuesta inv�lida!");
																						}
																					} else {
																						if (e.getSource().equals(ventanaControlada.tabDise�o.botonEsParteDe)) {
																							System.out.println("Ha pulsado el boton de regla es_parte_de");
																							DialogRulePartOf dialog = new DialogRulePartOf(ventanaControlada.getIdioma(), ventanaControlada);
																							dialog.pack();  // para darle tama�o autom�tico a la ventana.
																							dialog.setVisible(true);
																							
																						} else {
																							if (e.getSource().equals(ventanaControlada.tabDise�o.botonMide)) {
																								System.out.println("Ha pulsado el boton de regla mide");
																								DialogRuleMeassure dialog = new DialogRuleMeassure(ventanaControlada.getIdioma(), ventanaControlada);
																								dialog.pack();  // para darle tama�o autom�tico a la ventana.
																								dialog.setVisible(true);
																							}else {
																								if (e.getSource().equals(ventanaControlada.menuItem6)) {
																									System.out.println("Ha pulsado el boton cargar");
																									
																									JFileChooser jfc = new JFileChooser();
																						    		//jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY); 
																						    		
																									int returnVal = jfc.showOpenDialog(ventanaControlada);

																									if (returnVal == JFileChooser.APPROVE_OPTION) {
																										File file = jfc.getSelectedFile();        	                
																										String s = file.getAbsolutePath();
																										
																										cargar(s);
																						            
																									} else {
																										System.out.println("Apertura de fichero cancelada.");
																									}
																								}else {
																									if (e.getSource().equals(ventanaControlada.menuItem7)) {
																										System.out.println("Ha pulsado el boton guardar");
																										
																										JFileChooser jfc = new JFileChooser();
																							    		//jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
																							    		
																										int returnVal = jfc.showSaveDialog(ventanaControlada);

																										if (returnVal == JFileChooser.APPROVE_OPTION) {
																											File file = jfc.getSelectedFile();        	                
																											String s = file.getAbsolutePath();
																											guardar(s, ventanaControlada.getDatos());
																							            
																										} else {
																											System.out.println("Almacenamiento en el fichero cancelada.");
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}

		}

		// dejamos el cursor como estaba
		cur = new Cursor(Cursor.DEFAULT_CURSOR);
		// ventanaControlada.desbloquearBotones();
		ventanaControlada.setCursor(cur);
	}
	
	/**
	 * M�todo para obtener los mensajes en el dioma correspondiente.
	 * @param idioma ingles o espa�ol.
	 * @param tipoElemento el tipo de elemento cuyo mensaje queremos obtener.
	 * @return los mensajes en el dioma correspondiente.
	 */
	private String getMensaje(String idioma, String tipoElemento){
		String salida = "";
		//en espa�ol
		if (idioma.equals("espa�ol")){
			if (tipoElemento.equals("consultar componente")){
				salida = "Consultando un componente";
			}else{
				if (tipoElemento.equals("consultar regla")){
					salida = "Consultando una regla";
				}else{
					if (tipoElemento.equals("selecciona maquina")){
						salida = "Seleccione una m�quina";
					}else{
						if (tipoElemento.equals("selecciona herramienta")){
							salida = "Seleccione una herramienta";
						}else{
							if (tipoElemento.equals("selecciona pieza")){
								salida = "Seleccione una pieza";
							}else{
								if (tipoElemento.equals("selecciona refrigeracion")){
									salida = "Seleccione un sistema de refrigeraci�n";
								}else{
									if (tipoElemento.equals("selecciona sensor")){
										salida = "Seleccione un sensor";
									}else{
										if (tipoElemento.equals("selecciona cantidad")){
											salida = "Seleccione una cantidad";
										}
									}
								}
							}
						}
					}
				}
			}
		}else{
			//en ingles
			if (tipoElemento.equals("consultar componente")){
				salida = "Query: component";
			}else{
				if (tipoElemento.equals("consultar regla")){
					salida = "Query: rule";
				}else{
					if (tipoElemento.equals("selecciona maquina")){
						salida = "Select a machine";
					}else{
						if (tipoElemento.equals("selecciona herramienta")){
							salida = "Select a tool";
						}else{
							if (tipoElemento.equals("selecciona pieza")){
								salida = "Select a part";
							}else{
								if (tipoElemento.equals("selecciona refrigeracion")){
									salida = "Select a cooling system";
								}else{
									if (tipoElemento.equals("selecciona sensor")){
										salida = "Select a sensor";
									}else{
										if (tipoElemento.equals("selecciona cantidad")){
											salida = "Select a quantity";
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return salida;
	}

	/**
	 * Crea o sobreescribe un fichero indicado por una ruta con los datos pasados.
	 * @param ruta la ruta del fichero.
	 * @param datos los datos a escribir.
	 */
	private void guardar(String ruta, String datos){
		List<String> entrada = new LinkedList<String>();
		entrada.add(datos);

		//el fichero debe ser .txt
		String exten = Auxiliar.separarExtensionArchivo(ruta);
		if (exten.equals("txt")){
			try{
				FileManager.sobreescribir(ruta, entrada);
			}catch(Exception e){
				//mensaje de error
				String mensaje ="There's been an error accessing to the file"; 					
				if (ventanaControlada.getIdioma().equals("espa�ol")){
					mensaje = "Error al acceder al fichero";
				}
				JOptionPane.showMessageDialog(ventanaControlada,
				    mensaje,
				    "Error",
				    JOptionPane.ERROR_MESSAGE);
			}
		}else{
			//mensaje de error
			String mensaje ="Wrong file format.\nYou need a '.txt' file."; 					
			if (ventanaControlada.getIdioma().equals("espa�ol")){
				mensaje = "Formato de fichero err�neo.\nDebe ser '.txt.,";
			}
			JOptionPane.showMessageDialog(ventanaControlada,
			    mensaje,
			    "Error",
			    JOptionPane.ERROR_MESSAGE);
		}				
	}
	
	/**
	 * Carga los datos un fichero indicado por una ruta con los datos pasados.
	 * @param ruta la ruta del fichero.
	 */
	private void cargar(String ruta){
		
		//el fichero debe ser .txt
		String exten = Auxiliar.separarExtensionArchivo(ruta);
		if (exten.equals("txt")){
			ventanaControlada.canvas.removeAllAristas();
			ventanaControlada.canvas.removeAllVertices();			
			
			try{
				//lee los datos
				String s = FileManager.leerTodo(ruta);
				//separa
				String[] in = Auxiliar.separarTestCanvasDesc(s);
				
				//los datos del experimento
				ventanaControlada.setDatosExperimento(in[0]);
				
				//el contenido del canvas
				String[] canvas = Auxiliar.separarCanvas(in[1]);
				setCanvas(canvas[0], canvas[1]);
				
				//la descripcion del experimento
				ventanaControlada.setDescripcionExperimento(in[2]);
			}catch(Exception e){
				//mensaje de error
				String mensaje ="There's been an error accessing to the file"; 					
				if (ventanaControlada.getIdioma().equals("espa�ol")){
					mensaje = "Error al acceder al fichero";
				}
				JOptionPane.showMessageDialog(ventanaControlada,
				    mensaje,
				    "Error",
				    JOptionPane.ERROR_MESSAGE);
			}
		}else{
			//mensaje de error
			String mensaje ="Wrong file format.\nYou need a '.txt' file."; 					
			if (ventanaControlada.getIdioma().equals("espa�ol")){
				mensaje = "Formato de fichero err�neo.\nDebe ser '.txt'.";
			}
			JOptionPane.showMessageDialog(ventanaControlada,
			    mensaje,
			    "Error",
			    JOptionPane.ERROR_MESSAGE);
		}
		
	}
	
	/**
	 * Carga el contenido del canvas.
	 * @param vert la cadena con la lista de vertices.
	 * @param ar la cadena con la lista de aristas.
	 */
	private void setCanvas(String vert, String ar){
		String[] listV = Auxiliar.separarLista(vert);
		String[] listA = Auxiliar.separarLista(ar);
		
		//vertices
		for (int i=0; i<listV.length; i++){
			String[] v = Auxiliar.separarVertice(listV[i]);
			ventanaControlada.canvas.addVertice(Integer.parseInt(v[2]), Integer.parseInt(v[3]), v[0], v[1]);
		}
		
		//aristas
		for (int i=0; i<listA.length; i++){
			String[] a = Auxiliar.separarArista(listA[i]);
			String[] sv1 = Auxiliar.separarVertice(a[1]);
			Vertex v1 = new Vertex(Integer.parseInt(sv1[2]), Integer.parseInt(sv1[3]), sv1[0], sv1[1]);
			String[] sv2 = Auxiliar.separarVertice(a[2]);
			Vertex v2 = new Vertex(Integer.parseInt(sv2[2]), Integer.parseInt(sv2[3]), sv2[0], sv2[1]);
			ventanaControlada.canvas.addArista(v1, v2, a[0], a[3]);
		}
	}
	
	
	/**
	 * Actualiza la descrpcion del experimento,
	 */
	private void setDescripcion(){
		//TODO
		String desc = "descrip";
		//los nombres de los componentes
		String[] comp = ventanaControlada.getNombresComponentes();
		//los tipos de dichos componentes
		String[] tiposComp = new String[comp.length];
		for (int i=0; i<comp.length; i++){
			tiposComp[i] = Auxiliar.separarTipo(comp[i]);
		}
		//los nombres de los sensores
		String[] sen = ventanaControlada.getNombresSensores();
		//los tipos de dichos sensores
		String[] tiposSen = new String[sen.length];
		for (int i=0; i<sen.length; i++){
			tiposSen[i] = Auxiliar.separarTipo(sen[i]);
		}
		//los nombres de las cantidades
		String[] cantidades = ventanaControlada.getNombresComponentes();
		//los tipos de dichas cantidades
		String[] tiposCantidades = new String[cantidades.length];
		for (int i=0; i<cantidades.length; i++){
			tiposCantidades[i] = Auxiliar.separarTipo(cantidades[i]);
		}
		
		/*
		 * Habria que redactar el contenido: algo como "Elemento: nombre, tipo, y demas informacion
		 * adecuada de la base de datos", y volcarla en la variable desc
		 */
		
		/*
		 * A continuaci�n se deben obtener las aristas, expresando las relaciones entre los elementos.
		 * Reformatea a tu gusto desde el xml si quieres.
		 * //ventanaControlada.canvas.toString();
		 * Hay m�todos en auxiliar para desmontarlo todo y obtener los datos en String[],
		 * facilitando el trabajo
		 */		
		
		ventanaControlada.tabDise�o.setDescripcion(desc);		
	}
	
	/**
	 * M�todo principal para ejecutar la aplicaci�n.
	 * @param args argumentos irrelevantes en este caso.
	 */
	public static void main(String[] args) {
		Window v = new Window();
		Control c = new Control(v);
		v.a�adirControlador(c);
		v.crearVista();
	}

}
