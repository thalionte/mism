package interfaz;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JPanel;

import model.Auxiliar;
import model.render.PanelRender;

/**
 * Implementa la ventana de previsualizaci�n de la pieza.
 * @author M� Angeles Broull�n Lozano y Ana M� Fern�ndez Hern�ndez.
 * @version 2.0
 */
public class TabRender extends JDialog{

	//ATRIBUTOS
	/**
	 * El serial de Component.
	 */
	private static final long serialVersionUID = 1L;	
	/**
	 * El controlador del Tab.
	 */
	private ControlRender control;
	/**
	 * La lista de piezas.
	 */
	private String[] listaPiezas;
	/**
	 * Posibles piezas.
	 */
	protected JComboBox piezas;
	/**
	 * El bot�n para lanzar la previsualizaci�n.
	 */
	protected JButton botonPrevisualizar;
	/**
	 * Panel con los gr�ficos de previsualizaci�n.
	 */
	private JPanel graficoPrevio;
	/**
	 * El panel con el render.
	 */
	private PanelRender  figuras;
	
	//METODOS
	/**
	 * Constructor son un par�metro: genera un componente con el contenido de la pesta�a de previsualizar.
	 * @param lista la lista de piezas actual.
	 * @param v la ventana padre para hacerla modal. 
	*/
	public TabRender(String[] lista, Window v){
		super(v, true);
		listaPiezas = lista;
	}
	
	/**
	 * Genera la vista.
	 */
	public void crearVista(){
		
		//generamos y a�adimos los elementos de control de la seleccion del experimento.
		JPanel panelPre = new JPanel();
		panelPre.setLayout(new BorderLayout());
		
		JPanel panelSeleccion = new JPanel();
		panelSeleccion.setLayout(new FlowLayout());
		piezas = new JComboBox(listaPiezas);
		panelSeleccion.add(piezas);	
		ImageIcon icono1 = Auxiliar.crearIcono("preview_big.gif", "Design");
		botonPrevisualizar = new JButton("Generate preview", icono1);
		botonPrevisualizar.addActionListener(control);
		panelSeleccion.add(botonPrevisualizar);
		
		panelPre.add(panelSeleccion, BorderLayout.NORTH);
		
		graficoPrevio = new JPanel();
		//ImageIcon icono2 = Auxiliar.crearIcono("cubo.jpg", "dibujoCubo");
		//JLabel g = new JLabel(icono2);
		//graficoPrevio.add(g);		
		
		figuras = new PanelRender();
		graficoPrevio.add(figuras);
		
		//aqui se generar� el gr�fico previo
		panelPre.add(graficoPrevio, BorderLayout.SOUTH);
		
		this.add(panelPre);
		
		this.setTitle("MISM 1.0 ");
	}
	
	/**
	 * M�todo que le a�ade un controlador a la ventana.
	 * @param c el nuevo controlador.
	 */
	public void a�adirControlador(ControlRender c){
		control = c;
	}
	
	/**
	 * M�todo que traduce la interfaz.
	 * @param s el idioma a traducir: ingl�s o espa�ol.
	 */
	public void setIdioma(String s){
		if (s.equals("espa�ol")){
			botonPrevisualizar.setText("Generar previsualizaci�n");	
			this.setTitle("Renderizar");
		}else{
			botonPrevisualizar.setText("Generate preview");
			this.setTitle("Render");
		}
	}
	
	/**
	 * Cambia la figura tridimensional a mostrar.
	 * @param s debe ser "Cubo", "Prisma", "Cilindro" o "Esfera".
	 */
	public void setFigura(String s){
		figuras.cambiarFigura(s);
	}
	
	
	
}
