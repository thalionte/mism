package interfaz;

import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.Auxiliar;

/**
 * Implementa la ventana de control de conexi�n.
 * @author M� Angeles Broull�n Lozano y Ana M� Fern�ndez Hern�ndez.
 * @version 2.0
 */
public class TabConnection extends JDialog{
	
	//ATRIBUTOS
	/**
	 * El serial de Component.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * El controlador de la pesta�a.
	 */
	private ControlConnection control;	
	/**
	 * La etiqueta de valores de variables num�ricas.
	 */
	private JLabel etiquetaVariablesNumericas;
	/**
	 * El bot�n para lanzar la captura de datos.
	 */
	protected JButton botonCapturar;
	/**
	 * La etiqueta de velocidad de giro.
	 */
	private JLabel etiquetaGiro;
	/**
	 * Captura la velocidad de giro actual de la m�quina.
	 */
	private JTextField vGiro;
	/**
	 * La etiqueta de avance de la m�quina.
	 */
	private JLabel etiquetaAvance;
	/**
	 * Captura el avance de la m�quina.
	 */
	private JTextField avance;
	/**
	 * La etiqueta de la posicion del eje X de la m�quina.
	 */
	private JLabel etiquetaPosicionX;
	/**
	 * Captura la posicion del eje X de la m�quina.
	 */
	private JTextField posicionX;
	/**
	 * La etiqueta de la posicion del eje Y de la m�quina.
	 */
	private JLabel etiquetaPosicionY;
	/**
	 * Captura la posicion del eje Y de la m�quina.
	 */
	private JTextField posicionY;
	
	//METODOS
	
	/**
	 * Constructor con un par�metro: genera un componente con el contenido de la pesta�a de adquirir datos.
	 * @param v la ventana padre que sea modal.
	 */
	public TabConnection(Window v){
		super(v, true);
	}
	
	/**
	 * M�todo que le a�ade un controlador a la ventana.
	 * @param c el nuevo controlador.
	 */
	public void a�adirControlador(ControlConnection c){
		control = c;
	}
	
	/**
	 * Genera la vista.
	 */
	public void crearVista(){
		this.setLayout(new FlowLayout());
		
		//panel con las variables
		//titulo numerico
		JPanel variables = new JPanel();
		variables.setLayout(new BoxLayout(variables, BoxLayout.Y_AXIS));
		etiquetaVariablesNumericas = new JLabel("Numeric values:");
		variables.add(etiquetaVariablesNumericas);		
			
		//variables numericas	
		JPanel vars = new JPanel();
		vars.setLayout(new GridLayout(4,2));
		
		JPanel c1 = new JPanel();
		c1.setLayout(new FlowLayout());
		c1.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		etiquetaGiro = new JLabel("Spindle: ");
		c1.add(etiquetaGiro, Component.RIGHT_ALIGNMENT);
		vars.add(c1);
		JPanel c2 = new JPanel();
		c2.setLayout(new FlowLayout());
		c2.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		vGiro = new JTextField("0", 3);
		c2.add(vGiro, Component.LEFT_ALIGNMENT);
		vars.add(c2, Component.LEFT_ALIGNMENT);
		
		JPanel c3 = new JPanel();
		c3.setLayout(new FlowLayout());
		c3.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		etiquetaAvance = new JLabel("Feed: ");
		c3.add(etiquetaAvance, Component.RIGHT_ALIGNMENT);
		vars.add(c3);
		JPanel c4 = new JPanel();
		c4.setLayout(new FlowLayout());
		c4.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		avance = new JTextField("0", 3);
		c4.add(avance, Component.LEFT_ALIGNMENT);
		vars.add(c4, Component.LEFT_ALIGNMENT);
				
		JPanel c5 = new JPanel();
		c5.setLayout(new FlowLayout());
		c5.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		etiquetaPosicionX = new JLabel("Position X: ");
		c5.add(etiquetaPosicionX, Component.RIGHT_ALIGNMENT);
		vars.add(c5);
		JPanel c6 = new JPanel();
		c6.setLayout(new FlowLayout());
		c6.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		posicionX = new JTextField("0", 3);
		c6.add(posicionX, Component.LEFT_ALIGNMENT);		
		vars.add(c6, Component.LEFT_ALIGNMENT);
		
		JPanel c7 = new JPanel();
		c7.setLayout(new FlowLayout());
		c7.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		etiquetaPosicionY = new JLabel("Position Y: ");
		c7.add(etiquetaPosicionY, Component.RIGHT_ALIGNMENT);
		vars.add(c7);
		JPanel c8 = new JPanel();
		c8.setLayout(new FlowLayout());
		c8.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		posicionY = new JTextField("0", 3);
		c8.add(posicionY, Component.LEFT_ALIGNMENT);
		vars.add(c8, Component.LEFT_ALIGNMENT);
		
		variables.add(vars);
				
		this.add(variables);
				
		//panel con el bot�n de captura:
		JPanel captura = new JPanel();
		captura.setLayout(new FlowLayout());
		ImageIcon icono1 = Auxiliar.crearIcono("cable.png", "Design");
		botonCapturar = new JButton("Get data from the CNC", icono1);
		captura.add(botonCapturar, Component.CENTER_ALIGNMENT);
		botonCapturar.addActionListener(control);
		this.add(captura);
		
		this.setTitle("MISM 1.0 ");	
	}
	
	/**
	 * M�todo que traduce la interfaz.
	 * @param s el idioma a traducir: ingl�s o espa�ol.
	 */
	public void setIdioma(String s){
		if (s.equals("espa�ol")){
			etiquetaVariablesNumericas.setText("Valores de la variables num�ricas:");
			botonCapturar.setText("Capturar los valores desde el CNC");
			etiquetaGiro.setText("Velocidad de giro:");
			etiquetaAvance.setText("Avance:");
			etiquetaPosicionX.setText("Posici�n respecto al eje X:");
			etiquetaPosicionY.setText("Posici�n respecto al eje Y:");
			this.setTitle("Conectar");
		}else{
			etiquetaVariablesNumericas.setText("Numeric values:");
			botonCapturar.setText("Get data from the CNC");
			etiquetaGiro.setText("Spindle:");
			etiquetaAvance.setText("Feed:");
			etiquetaPosicionX.setText("Position X:");
			etiquetaPosicionY.setText("Position Y:");
			this.setTitle("Connect");
		}
	}

}
