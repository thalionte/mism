package interfaz;

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import model.FileManager;

/**
 * Implementa el controlador de la ventana de exportar.
 * @author M� Angeles Broull�n Lozano y Ana M� Fern�ndez Hern�ndez.
 * @version 2.0
 */
public class ControlSave implements ActionListener {
	
	// ATRIBUTOS
	/**
	 * La ventana controlada.
	 */
	private TabSave ventanaControlada;

	// METODOS
	/**
	 * Constructor con par�metro.
	 * @param win la ventana que ser� controlada mediante esta instancia.
	 */
	public ControlSave(TabSave win) {
		ventanaControlada = win;
	}

	/**
	 * El m�todo que controla las acciones de la ventana.
	 * @param el evento a manejar.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		// cambiamos el cursor por un reloj
		Cursor cur = new Cursor(Cursor.WAIT_CURSOR);
		ventanaControlada.setCursor(cur);

		// ejecutamos la acci�n correspondiente
		if (e.getSource().equals(ventanaControlada.botonGuardar)) {
			System.out.println("Puls� el bot�n de guardar");
			String s = ventanaControlada.getDatos();
			System.out.println(s);
			
			//guardar la descripcion.
			try{
				List<String> entrada = new LinkedList<String>();
				entrada.add(s);
				String ruta1 = ventanaControlada.getRuta() + "\\test.txt";
				FileManager.sobreescribir(ruta1, entrada);
				String ruta2 = ventanaControlada.getRuta() + "\\test.jpg";
				ventanaControlada.saveCanvasImage(ruta2);
			}catch(Exception e1){
				//mensaje de error
				String mensaje ="There's been an error accessing to the file"; 					
				if (ventanaControlada.getIdioma().equals("espa�ol")){
					mensaje = "Error al acceder al fichero";
				}
				JOptionPane.showMessageDialog(ventanaControlada,
				    mensaje,
				    "Error",
				    JOptionPane.ERROR_MESSAGE);
			}
			
		
			
		}else{
			if (e.getSource().equals(ventanaControlada.selectRuta)) {
				System.out.println("Ha pulsado seleccion de ruta.");
				System.out.println("Ha pulsado seleccionar ruta");
		    	JFileChooser jfc = new JFileChooser();
		    	jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY); 
		    		
				int returnVal = jfc.showOpenDialog(ventanaControlada);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = jfc.getSelectedFile();        	                
					String s = file.getAbsolutePath();
					ventanaControlada.setRuta(s);
	            
				} else {
					System.out.println("Apertura de fichero cancelada.");
				}
			}
		}


		// dejamos el cursor como estaba
		cur = new Cursor(Cursor.DEFAULT_CURSOR);
		// ventanaControlada.desbloquearBotones();
		ventanaControlada.setCursor(cur);
	}

}
