package interfaz;

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.Auxiliar;

/**
 * Implementa el controlador de la ventana de renderizado.
 * @author M� Angeles Broull�n Lozano y Ana M� Fern�ndez Hern�ndez.
 * @version 2.0
 */
public class ControlRender implements ActionListener{

	// ATRIBUTOS
	/**
	 * La ventana controlada.
	 */
	private TabRender ventanaControlada;

	// METODOS
	/**
	 * Constructor con par�metro.
	 * @param win la ventana que ser� controlada mediante esta instancia.
	 */
	public ControlRender(TabRender win) {
		ventanaControlada = win;
	}

	/**
	 * El m�todo que controla las acciones de la ventana.
	 * @param el evento a manejar.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		// cambiamos el cursor por un reloj
		Cursor cur = new Cursor(Cursor.WAIT_CURSOR);
		ventanaControlada.setCursor(cur);

		// ejecutamos la acci�n correspondiente
		if (e.getSource().equals(ventanaControlada.botonPrevisualizar)) {
			System.out.println("Puls� el bot�n de previsualizar");
			
			//TODO
			/*
			 * Con el nombre de la pieza seleccionada, se debe obtener la geometria de la misma.
			 */
			//String busqueda = (String) (ventanaControlada.piezas.getSelectedItem());
			//busqueda = Auxiliar.separarTipo(busqueda);
			/*
			 * alguna forma de obtener la forma en un String forma, siendo esta cubo, prisma,
			 * cilindro o esfera
			 */
			//ventanaControlada.setFigura(forma);
			/*
			 * Fin del segmento a modificar.
			 */
		}


		// dejamos el cursor como estaba
		cur = new Cursor(Cursor.DEFAULT_CURSOR);
		// ventanaControlada.desbloquearBotones();
		ventanaControlada.setCursor(cur);
	}
	
}
