package interfaz;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JRadioButtonMenuItem;

import model.Auxiliar;
import model.drawDiagram.MyCanvas;



/**
 * Implementa la ventana, que contiene las 4 pesta�as de las 4 funcionalidades del sistema.
 * @author M� Angeles Broull�n Lozano y Ana M� Fern�ndez Hern�ndez.
 * @version 2.0
 */
public class Window extends JFrame{

	//ATRIBUTOS
	/**
	 * El serial heredado de JFrame.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * El idioma en el que est� la aplicaci�n.
	 */
	private String idioma;
	
	//la cabecera
	/**
	 * El boton de control de idioma, que marca la opci�n de ingl�s.
	 */
	protected JRadioButton botonIngles;
	/**
	 * El boton de control de idioma, que marca la opci�n de espa�ol.
	 */
	protected JRadioButton botonEspa�ol;
	/**
	 * El menu de experimento.
	 */
	private JMenu menu1;
	/**
	 * El menu de idioma.
	 */
	private JMenu menu2;
	/**
	 * El menu de otras opciones.
	 */
	private JMenu menu3;
	/**
	 * El icono de menu de datos de experimento.
	 */
	protected JMenuItem menuItem1;
	/**
	 * El icono de menu de datos de salir.
	 */
	protected JMenuItem menuItem2;
	/**
	 * El icono de menu de datos de conexi�n.
	 */
	protected JMenuItem menuItem3;
	/**
	 * El icono de menu de datos de exportar datos.
	 */
	protected JMenuItem menuItem4;
	/**
	 * El icono de menu de datos de renderizar.
	 */
	protected JMenuItem menuItem5;
	/**
	 * El icono de menu de datos de cargar.
	 */
	protected JMenuItem menuItem6;
	/**
	 * El icono de menu de datos de guardar.
	 */
	protected JMenuItem menuItem7;
	/**
	 * Un boton radial para el idioma ingl�s.
	 */
	protected JRadioButtonMenuItem rbMenuItem1;
	/**
	 * Un boton radial para el idioma espa�ol.
	 */
	protected JRadioButtonMenuItem rbMenuItem2;
	/**
	 * El controlador de la ventana.
	 */
	private Control control;
	/**
	 * El canvas para dibujar el diagrama.
	 */
	protected MyCanvas canvas;	
	/**
	 * La pesta�a de dise�o.
	 */
	protected TabConfiguration tabDise�o;
	
	/**
	 * La pesta�a de adquisici�n de datos desde el CNC.
	 */
	protected TabConnection tabAdquirir;
	
	/**
	 * La pesta�a de almacenamiento de datos.
	 */
	protected TabSave tabAlmacenar;
	
	/**
	 * La pesta�a deprevisulaizaci�nde la pieza.
	 */
	protected TabRender tabPrevisualizacion;
	/**
	 * Los datos del experimentos.
	 */
	private String datosExperimento;
	
	
	
	//METODOS
	/**
	 * M�todo constructor sin par�metros.
	 */
	public Window(){
		super();
		idioma = "ingles";
		
		String s = "<Test> ";
		s = s + "<Name> " + " " + " <\\Name> ";
		s = s +	"<TimeLength> " + " "  + " <\\TimeLength> ";
		s = s +	"<TimeLengthUnits> " + " "  + " <\\TimeLengthUnits> ";
		s = s + "<Start> " + " " + " <\\Start> ";
		s = s + "<End> " + " " + " <\\End> ";
		s = s + "<\\Test> ";
		datosExperimento = s;
	}
	
	/**
	 * M�todoque a�ade el controlador a la ventana.
	 * @param control el controlador de la ventana.
	 */
	public void a�adirControlador(Control c){
		control = c;
	}
	
	/**
	 * Construye la ventana, haci�ndola visible.
	 */
	public void crearVista() {
		
		//el panel de contenidos
		Container container = getContentPane();
		this.setJMenuBar(crearMenu());
		
		crearDise�o();
		canvas = new MyCanvas();
		JPanel panelElem = new JPanel();
		panelElem.add(canvas);
		panelElem.add(tabDise�o);
		container.add(panelElem);
		
		//control de cierre
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		
		//visualizaci�n
		this.setTitle("MISM 1.0");
		setSize(950, 590);
		setVisible(true);
		this.setMinimumSize(new Dimension(950, 590));
		this.setResizable(false);
		this.pack();
	}
	
	/**
	 * Genera la barra de menu.
	 * @return la barra de menu generada.
	 */
	private JMenuBar crearMenu(){
		JMenuBar menuBar = new JMenuBar();
		
		//le menu de experimentos
		menu1 = new JMenu("Test");
		menu1.setMnemonic(KeyEvent.VK_T);
		
		menuItem1 = new JMenuItem("Data of the test", KeyEvent.VK_D);
		menuItem2 = new JMenuItem("Exit", KeyEvent.VK_I);
		menuItem6 = new JMenuItem("Load test", KeyEvent.VK_A);
		menuItem7 = new JMenuItem("Save test", KeyEvent.VK_E);
		menuItem1.addActionListener(control);
		menuItem2.addActionListener(control);
		menuItem6.addActionListener(control);
		menuItem7.addActionListener(control);		
		
		menu1.add(menuItem1);
		menu1.add(menuItem6);
		menu1.add(menuItem7);
		menu1.add(menuItem2);
		
		//el menu de idioma
		menu2 = new JMenu("Language");
		menu2.setMnemonic(KeyEvent.VK_A);
		//menu2.add(botonIngles);
		//menu2.add(botonEspa�ol);
		ButtonGroup group = new ButtonGroup();
		String origen1 = "en.gif";
		ImageIcon imagenIngles = Auxiliar.crearIcono(origen1, "English");
		rbMenuItem1 = new JRadioButtonMenuItem("English", imagenIngles);
		rbMenuItem1.setSelected(true);
		rbMenuItem1.addActionListener(control);
		group.add(rbMenuItem1);
		menu2.add(rbMenuItem1);
		String origen2 = "es.gif";
		ImageIcon imagenEspa�ol = Auxiliar.crearIcono(origen2, "English");
		rbMenuItem2 = new JRadioButtonMenuItem("Spanish", imagenEspa�ol);
		rbMenuItem2.addActionListener(control);
		group.add(rbMenuItem2);
		menu2.add(rbMenuItem2);
		
		//el menu de otras opciones
		menu3 = new JMenu("Other options");
		menu3.setMnemonic(KeyEvent.VK_O);
		
		ImageIcon icono3 = Auxiliar.crearIcono("get.gif", "Get");
		menuItem3 = new JMenuItem("Connection", icono3);
		ImageIcon icono4 = Auxiliar.crearIcono("save.gif", "Save");
		menuItem4 = new JMenuItem("Export", icono4);
		ImageIcon icono5 = Auxiliar.crearIcono("preview.gif", "Preview");
		menuItem5 = new JMenuItem("Render", icono5);
		menuItem3.addActionListener(control);
		menuItem4.addActionListener(control);
		menuItem5.addActionListener(control);
		
		menu3.add(menuItem3);
		menu3.add(menuItem4);
		menu3.add(menuItem5);
		
		menuBar.add(menu1);
		menuBar.add(menu2);
		menuBar.add(menu3);
		return menuBar;
	}
	
	/**
	 * Genera un componente con el contenido de la pesta�a de dise�ar experimento.
	*/
	private void crearDise�o(){
		tabDise�o = new TabConfiguration(control, idioma);
	}
	
	/**
	 * M�todo que traduce la interfaz.
	 * @param s el idioma a traducir: ingl�s o espa�ol.
	 */
	public void setIdioma(String s){
		idioma = s;
		if (s.equals("espa�ol")){
			tabDise�o.setIdioma("espa�ol");
			//tabAdquirir.setIdioma("espa�ol");
			//tabAlmacenar.setIdioma("espa�ol");
			//tabPrevisualizacion.setIdioma("espa�ol");
			menu1.setText("Experimento");
			menuItem1.setText("Datos del experimento");
			menuItem6.setText("Cargar experimento");
			menuItem7.setText("Guardar experimento");
			menuItem2.setText("Salir");
			menu2.setText("Idioma");
			rbMenuItem1.setText("Ingl�s");
			rbMenuItem2.setText("Espa�ol");
			menuItem3.setText("Conectar");
			menuItem4.setText("Exportar");
			menuItem5.setText("Renderizar");
		}else{
			tabDise�o.setIdioma("ingles");
			//tabAdquirir.setIdioma("ingles");
			//tabAlmacenar.setIdioma("ingles");
			//tabPrevisualizacion.setIdioma("ingles");
			menu1.setText("Test");
			menuItem1.setText("Data of the test");
			menuItem6.setText("Load test");
			menuItem7.setText("Save test");
			menuItem2.setText("Exit");
			menu2.setText("Language");
			rbMenuItem1.setText("English");
			rbMenuItem2.setText("Spanish");
			menuItem3.setText("Connect");
			menuItem4.setText("Export");
			menuItem5.setText("Render");
		}
	}
	
	/**
	 * Selecciona la ruta del archivo.
	 * @param s la ruta a almacenar.
	 */
	protected void setRuta(String s){
		tabAlmacenar.setRuta(s);
	}
	
	/**
	 * Almacena los datos del experimento.
	 * @param s los datos a almacenar.
	 */
	protected void setDatosExperimento(String s){
		datosExperimento = s;
	}
	
	
	/**
	 * Almacena la descripcion del experimento.
	 * @param s los datos a almacenar.
	 */
	protected void setDescripcionExperimento(String s){
		tabDise�o.setDescripcion(s);
	}
	/**
	 * M�todo observador que devuelve el idioma actual de la aplicaci�n.
	 * @return el idioma actual de la aplicaci�n.
	 */
	public String getIdioma(){
		return idioma;
	}
	
	/**
	 * M�todo observador que devuelve los nombres de los v�rtices del canvas.
	 * @return los nombres de los v�rtices del canvas.
	 */
	public String[] getNombresVertices(){
		String[] salida = canvas.getListaVertices();		
		return salida;
	}
	
	/**
	 * M�todo observador que devuelve los nombres de los v�rtices de sensores del canvas.
	 * @return los nombres de los v�rtices de sensores del canvas.
	 */
	public String[] getNombresComponentes(){
		String[] salida = canvas.getListaComponentes();		
		return salida;
	}
	
	/**
	 * M�todo observador que devuelve los nombres de los v�rtices de sensores del canvas.
	 * @return los nombres de los v�rtices de sensores del canvas.
	 */
	public String[] getNombresSensores(){
		String[] salida = canvas.getListaSensores();		
		return salida;
	}
	
	/**
	 * M�todo observador que devuelve los nombres de los v�rtices de cantidades del canvas.
	 * @return los nombres de los v�rtices de cantidades del canvas.
	 */
	public String[] getNombresCantidades(){
		String[] salida = canvas.getListaCantidades();		
		return salida;
	}
	
	/**
	 * M�todo observador que devuelve los nombres de los v�rtices de piezas del canvas.
	 * @return los nombres de los v�rtices de piezas del canvas.
	 */
	public String[] getNombresPiezas(){
		String[] salida = canvas.getListaPiezas();		
		return salida;
	}
	
	/**
	 * Obtiene los datos del experimento para guardarlos.
	 * @return los datos del experimento para guardarlos.
	 */
	public String getDatos(){
		String s = datosExperimento;
		s = s + canvas.toString();
		s = s + "<Desc> " + tabDise�o.getDescripcion() + "  <\\Desc> "; 
		return s;
	}
	
	/**
	 * Obtiene los datos de descripcion del experimento.
	 * @return los datos de descripcion del experimento.
	 */
	public String getDescripcion(){
		return tabDise�o.getDescripcion() + " "; 
	}
	
	/**
	 * Guarda el contenido del canvas como un jpg.
	 * @param dir la direccion donde guardar.
	 */
	public void saveCanvasImage(String dir){
		canvas.save(dir);
	}
	
	/**
     * A�ade una arista a la lista de aristas a dibujar.
     * @param v1 el nombre del primer v�rtice de la arista.
     * @param v2 el nombre del segundo v�rtice de la arista.
     * @param tipoArista el tipo de la arista para ponerle el color apropiado.
     */
    public void addArista(String v1, String v2, String tipoArista){
    	canvas.addArista(v1, v2, tipoArista);
    }
}

