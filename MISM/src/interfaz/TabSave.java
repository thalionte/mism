package interfaz;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.Auxiliar;
import model.drawDiagram.MyCanvas;

/**
 * Implementa la ventana de almacenamiento de datos.
 * @author M� Angeles Broull�n Lozano y Ana M� Fern�ndez Hern�ndez.
 * @version 2.0
 */
public class TabSave extends JDialog{
	
	//ATRIBUTOS
	/**
	 * El serial de Component.
	 */
	private static final long serialVersionUID = 1L;	
	/**
	 * El Controlador de la pesta�a.
	 */
	private ControlSave control;
	/**
	 * La etiqueta con la explicaci�n del fucnionamiento de esta pesta�a.
	 */
	private JLabel explicacion;
	/**
	 * La etiqueta que se�alar� la ruta.
	 */
	private JLabel etiquetaRuta;
	/**
	 * Contendra la ruta del archivo en la que se espera almacenar el experimento.
	 */
	private JTextField rutaExperimento;
	/**
	 * Boton que lanzar� el di�logo para seleccionar la ruta de almacenamiento.
	 */
	protected JButton selectRuta;
	/**
	 * Boton que ejecutar� la guarda de la informaci�n adquirida.
	 */
	protected JButton botonGuardar;
	/**
	 * Los datos del experimentos.
	 */
	private String datosExperimento;
	/**
	 * El canvas para guardar.
	 */
	private MyCanvas canvas;
	/**
	 * El idioma actual. 
	 */
	private String idioma;
	
	
	//METODOS
	/**
	 * Constructor con un par�metro: genera un componente con el contenido de la pesta�a de almacenar datos.
	 * @param datos los datos del experimento;
	 * @param v la ventana padre para que sea modal.
	 */
	public TabSave(String datos, Window v){
		super(v, true);
		canvas = v.canvas;
		datosExperimento = datos;
		idioma = "ingles";
	}
	
	/**
	 * Crea la vista.
	 */
	public void crearVista(){		
		JPanel panelRuta = new JPanel();		
		panelRuta.setLayout(new BorderLayout());
		explicacion = new JLabel("                                  Save the information in a file:\n");
		panelRuta.add(explicacion, BorderLayout.NORTH);
		//generamos y a�adimos los elementos de control de la ruta.
		JPanel pRuta = new JPanel();
		pRuta.setLayout(new FlowLayout());
		etiquetaRuta = new JLabel("Path: ");
		pRuta.add(etiquetaRuta);
		rutaExperimento = new JTextField("C:\\", 50);
		pRuta.add(rutaExperimento);
		selectRuta =  new JButton("...");
		selectRuta.addActionListener(control);
		pRuta.add(selectRuta);
		panelRuta.add(pRuta, BorderLayout.NORTH);
		this.add(panelRuta, BorderLayout.NORTH);
		
		//generamos y a�adimos los elementos de control de la seleccion del experimento.
		JPanel bGuardar = new JPanel();
		bGuardar.setLayout(new FlowLayout());
		ImageIcon icono2 = Auxiliar.crearIcono("save.gif", "Save");
		botonGuardar = new JButton("Save", icono2);
		bGuardar.add(botonGuardar, Component.CENTER_ALIGNMENT);
		botonGuardar.addActionListener(control);
		panelRuta.add(bGuardar, BorderLayout.CENTER);
		
		this.setTitle("MISM 1.0 ");
	}
	
	/**
	 * M�todo que le a�ade un controlador a la ventana.
	 * @param c el nuevo controlador.
	 */
	public void a�adirControlador(ControlSave c){
		control = c;
	}
	
	/**
	 * M�todo que traduce la interfaz.
	 * @param s el idioma a traducir: ingl�s o espa�ol.
	 */
	public void setIdioma(String s){
		idioma = s;
		if (s.equals("espa�ol")){
			explicacion.setText("Guardar la informaci�n en un archivo:");
			etiquetaRuta.setText("Ruta:");
			botonGuardar.setText("Guardar");
			this.setTitle("Exportar");
		}else{
			explicacion.setText("Save the information in a file:");
			etiquetaRuta.setText("Path:");
			botonGuardar.setText("Save");
			this.setTitle("Export");
		}
	}
	
	/**
	 * Recupera el idioma en el que se muestra la ventana actualmente.
	 * @return el idioma en el que se muestra la ventana actualmente.
	 */
	public String getIdioma(){
		return idioma;
	}
	
	/**
	 * Selecciona la ruta del archivo.
	 * @param s la ruta a almacenar.
	 */
	protected void setRuta(String s){
		rutaExperimento.setText(s);
	}
	
	/**
	 * Recupera la ruta del archivo.
	 * @return la ruta del archivo.
	 */
	protected String getRuta(){
		return rutaExperimento.getText();
	}
	
	/**
	 * Obtiene los datos del experimento para guardarlos.
	 * @return los datos del experimento para guardarlos.
	 */
	public String getDatos(){
		String s = datosExperimento;
		return s;
	}
	
	/**
	 * Guarda el contenido del canvas como un jpg.
	 * @param dir la direccion donde guardar.
	 */
	public void saveCanvasImage(String dir){
		canvas.save(dir);
	}

}
