package interfaz.rules;

import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;

/**
 * Implementa una ventana de di�logo para a�adir una nueva regla.
 * @author M� Angeles Broull�n Lozano y Ana M� Fern�ndez Hern�ndez.
 * @version 2.0
 */
public class WindowAddRule extends JDialog{

	//ATRIBUTOS
	/**
	 * El serial de JDialog.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * El panel con las pesta�as.
	 */
	private JTabbedPane panel;
	/**
	 * El controlador.
	 */
	private ControlAddRule control;
	/**
	 * El idioma de la ventana.
	 */
	private String idioma;
	/**
	 * Nombre del primer tipo de reglas.
	 */
	private String reglas1;
	/**
	 * Nombre del segundo tipo de reglas.
	 */
	private String reglas2;
	/**
	 * El panel para insertar una regla "es parte de"
	 */
	protected TabPartOf panelParteDe;
	/**
	 * El panel para insertar una regla de "medida"
	 */
	protected TabMeassure panelMedida;
	/**
	 * El boton de aceptar.
	 */
	protected JButton aceptar;
	/**
	 * El boton de cancelar.
	 */
	protected JButton cancelar;

	//METODOS
	/**
	 * M�todo constructor con un par�metro.
	 * @param s el idioma en que debe mostrarse. 
	 */
	public WindowAddRule(String s){
		super();
		idioma  = s;
	}
	
	/**
	 * M�todoque a�ade el controlador a la ventana.
	 * @param control el controlador de la ventana.
	 */
	public void a�adirControlador(ControlAddRule c){
		control = c;
	}
	
	/**
	 * Genera el contanido de la ventana.
	 */
	public void crearVista(){
		Container container = getContentPane();
		container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
		
		container.add(crearCuerpo());
		
		//separador
		JSeparator s1 = new JSeparator(SwingConstants.HORIZONTAL);
		this.add(s1);
		
		//a�ade los botones
		JPanel botones = new JPanel();
		botones.setLayout(new FlowLayout());
		aceptar = new JButton("Accept");
		aceptar.addActionListener(control);
		botones.add(aceptar);
		cancelar = new JButton("Cancel");
		cancelar.addActionListener(control);
		botones.add(cancelar);
		container.add(botones);
		//visualizaci�n
		if (idioma.equals("ingles")){
			this.setTitle("Add rule");
		}else{
			this.setTitle("A�adir regla");
		}
		setIdioma(idioma);
		setSize(1000, 200);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);
	}
	
	/**
	 * Crea el cuerpo de la ventana.
	 * @return el cuerpo de la ventana.
	 */
	private Component crearCuerpo(){
		reglas1 = "";
		reglas2 = "";
		panel = new JTabbedPane();
		panelParteDe = new TabPartOf(idioma);
		panelParteDe.a�adirControlador(control);
		panelParteDe.crearVista();
		panelMedida = new TabMeassure(idioma);
		panelMedida.a�adirControlador(control);
		panelMedida.crearVista();
		panel.add(reglas1, panelParteDe);
		panel.add(reglas2, panelMedida);
		return panel;
	}
	
	
	
	/**
	 * M�todo que traduce la interfaz.
	 * @param s el idioma a traducir: ingl�s o espa�ol.
	 */
	public void setIdioma(String s){
		idioma = s;
		if (s.equals("ingles")){
			reglas1 = "is part of";
			reglas2 = "meassures";
			aceptar.setText("OK");
			cancelar.setText("Cancel");
			panel.setTitleAt(0, reglas1);
			panel.setTitleAt(1, reglas2);
		}else{
			reglas1 = "es parte de";
			reglas2 = "mide";
			aceptar.setText("Aceptar");
			cancelar.setText("Cancelar");
			panel.setTitleAt(0, reglas1);
			panel.setTitleAt(1, reglas2);
		}
	}
	
	/**
	 * Devuelve en una cadena de caracteres el estado actual de la ventana.
	 * @return el estado actual de la ventana en una cadena.
	 */
	public String toString(){
		int indice = panel.getSelectedIndex();
		String s = panel.getTitleAt(indice) + ":\n";
		s = s + (panel.getComponentAt(indice)).toString();		
		return s;
	}
	
}

