package interfaz.rules;


import interfaz.Window;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Implementa el di�logo para establecer una relaci�n meassure entre 3 componentes.
 * @author M� Angeles Broull�n Lozano y Ana M� Fern�ndez Hern�ndez.
 * @version 2.0
 */
public class DialogRuleMeassure extends JDialog implements ActionListener{
	
	//ATRIBUTOS
	/**
	 * El serial por defecto.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * La ventana controlada.
	 */
	private Window ventanaControlada;
	/**
	 * Componente primero.
	 */
	private JComboBox elemento1;
	/**
	 * La etiqueta de regla entre los 2 primeros elementos.
	 */
	private JLabel regla1;
	/**
	 * Componente segundo.
	 */
	private JComboBox elemento2;
	/**
	 * La etiqueta de regla entre los 2 ultimos elementos.
	 */
	private JLabel regla2;
	/**
	 * Componente tercero.
	 */
	private JComboBox elemento3;
	/**
	 * El bot�n para aceptar.
	 */
	private JButton botonAceptar;
	/**
	 * El bot�n para cancelar.
	 */
	private JButton botonCancelar;
	
	//M�TODOS
	/**
	 * Constructor con un par�metro.
	 * @param id el idioma seleccionado.
	 * @param els los elementos con los que se trabajar�.
	 */
	public DialogRuleMeassure(String id, Window v){
		super(v, true);
		ventanaControlada = v;
		this.setLayout(new BorderLayout());
		
		//componentes
		JPanel comp = new JPanel();
		comp.setLayout(new FlowLayout());
		elemento1 = new JComboBox(ventanaControlada.getNombresSensores());
		comp.add(elemento1);
		regla1 = new JLabel("");
		comp.add(regla1);
		elemento2 = new JComboBox(ventanaControlada.getNombresComponentes());
		comp.add(elemento2);
		regla2 = new JLabel("");
		comp.add(regla2);
		elemento3 = new JComboBox(ventanaControlada.getNombresCantidades());
		comp.add(elemento3);
		this.add(comp, BorderLayout.NORTH);
		
		//botones
		JPanel botones = new JPanel();
		botones.setLayout(new FlowLayout());
		botonAceptar = new JButton();
		botonAceptar.addActionListener(this);
		botones.add(botonAceptar);
		botonCancelar = new JButton();
		botonCancelar.addActionListener(this);
		botones.add(botonCancelar);
		this.getContentPane().add(botones, BorderLayout.SOUTH);
		
		//idioma y tama�o
		setIdioma(id);
	}
	
	/**
	 * Traduce el texto al idioma seleccionado.
	 * @param id el idioma al que queremos traducir.
	 */
	private void setIdioma(String id){
		if (id.equals("espa�ol")){
			this.setTitle("Establecer una regla 'mide'");
			regla1.setText("mide en");
			regla2.setText("una cantidad de");
			botonAceptar.setText("Aceptar");
			botonCancelar.setText("Cancelar");
		}else{
			this.setTitle("Set a rule 'meassures'");
			regla1.setText("meassures in");
			regla2.setText("a guantity of");
			botonAceptar.setText("Accept");
			botonCancelar.setText("Cancel");
		}
	}
	
	

	/**
	 * M�todo que controla las acciones de la ventana. 
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(botonAceptar)) {
			System.out.println("Acepto");
			String v1 = (String) elemento1.getSelectedItem();
			String v2 = (String) elemento2.getSelectedItem();
			String v3 = (String) elemento3.getSelectedItem();
			ventanaControlada.addArista(v1, v2, "mide");
			ventanaControlada.addArista(v2, v3, "mide");	
			this.dispose();
		}else{
			if (e.getSource().equals(botonCancelar)) {
				System.out.println("Cancelo");
				this.dispose();
			}
		}
		
	}

}
