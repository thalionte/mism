package interfaz.rules;

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Implementa el controlador de una ventana de di�logo para a�adir una nueva regla.
 * @author M� Angeles Broull�n Lozano y Ana M� Fern�ndez Hern�ndez.
 * @version 2.0
 */
public class ControlAddRule implements ActionListener{

	//ATRIBUTOS
	/**
	 * La ventana a controlar.
	 */
	private WindowAddRule ventanaControlada;

	//METODOS
	/**
	 * Constructor con un par�metro. 
	 * @param v la ventana a controlar.
	 */
	public ControlAddRule(WindowAddRule v){
		ventanaControlada = v;
		ventanaControlada.setModal(true);
	}
	
	/**
	 * El m�todo que controla las acciones de la ventana.
	 * @param el evento a manejar.
	 */
	public void actionPerformed(ActionEvent e) {
		//cambiamos el cursor por un reloj
		Cursor cur = new Cursor(Cursor.WAIT_CURSOR);
        ventanaControlada.setCursor(cur); 
        
        //ejecutamos la acci�n correspondiente   		
		if (e.getSource().equals(ventanaControlada.aceptar)){
			System.out.println("Ha pulsado aceptar.");
			System.out.println(ventanaControlada.toString());
			//TODO
			/*
			 * Lo que tenemos en el toString que imprimimos en pantalla
			 * en la linea anterior debe ir a la base de datos, cambia el formato
			 * si te place
			 */
			
		}else{
			if (e.getSource().equals(ventanaControlada.cancelar)){
				System.out.println("Ha pulsado cancelar.");
				ventanaControlada.dispose();
			}
		}
		
		//dejamos el cursor como estaba
		cur = new Cursor(Cursor.DEFAULT_CURSOR);
		//ventanaControlada.desbloquearBotones();
        ventanaControlada.setCursor(cur);
	}

}

