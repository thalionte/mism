package interfaz.rules;

import java.awt.Component;
import java.awt.GridLayout;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * El panel para actualizar reglas de relacion entre 2 componentes.
 * @author M� Angeles Broull�n Lozano y Ana M� Fern�ndez Hern�ndez.
 * @version 2.0
 */
public class TabPartOf extends JPanel{
	
	//ATRIBUTOS
	/**
	 * El serial de JDialog.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * El controlador.
	 */
	@SuppressWarnings("unused")
	private ControlAddRule control;
	/**
	 * El idioma de la ventana.
	 */
	private String idioma;
	/**
	 * La etiqueta del primer tipo de elemento.
	 */
	private JLabel etiquetaTipoElemento11;
	/**
	 * Componente primero.
	 */
	protected JComboBox tipoElemento11;
	/**
	 * La etiqueta con la lista de elementos de un mismo determinado tipo.
	 */
	private JLabel etiquetaListaElementos11;
	/**
	 * La los elementos de un determinado tipo.
	 */
	protected JComboBox listaElementos11;
	/**
	 * La etiqueta de accion a definir.
	 */
	private JLabel etiquetaAccion11;
	/**
	 * La etiqueta del segundo tipo de elemento.
	 */
	private JLabel etiquetaTipoElemento12;
	/**
	 * Componente segundo.
	 */
	protected JComboBox tipoElemento12;
	/**
	 * La etiqueta con la lista de elementos disponibles para una acci�n.
	 */
	private JLabel etiquetaListaElementos12;
	/**
	 * La lista de elementos disponibles para una acci�n.
	 */
	protected JComboBox listaElementos12;	

	//METODOS
	/**
	 * M�todo constructor con un par�metro.
	 * @param s el idioma en que debe mostrarse. 
	 */
	public TabPartOf(String s){
		super();
		idioma  = s;
	}
	
	/**
	 * M�todoque a�ade el controlador a la ventana.
	 * @param control el controlador de la ventana.
	 */
	public void a�adirControlador(ControlAddRule c){
		control = c;
	}
	
	/**
	 * Genera el contanido de la ventana.
	 */
	public void crearVista(){
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(2,5));
		
		etiquetaTipoElemento11 = new JLabel("Tipo de elemento:");
		panel.add(etiquetaTipoElemento11, Component.CENTER_ALIGNMENT);
		etiquetaListaElementos11 = new JLabel("Lista de elementos:");
		panel.add(etiquetaListaElementos11, Component.CENTER_ALIGNMENT);
		panel.add(new JLabel(), Component.CENTER_ALIGNMENT);
		etiquetaTipoElemento12 = new JLabel("Tipo de elemento:");
		panel.add(etiquetaTipoElemento12, Component.CENTER_ALIGNMENT);
		etiquetaListaElementos12 = new JLabel("Lista de elementos:");
		panel.add(etiquetaListaElementos12, Component.CENTER_ALIGNMENT);
		
		tipoElemento11 = new JComboBox();
		panel.add(tipoElemento11, Component.CENTER_ALIGNMENT);		
		listaElementos11 = new JComboBox();
		panel.add(listaElementos11, Component.CENTER_ALIGNMENT);
		etiquetaAccion11 = new JLabel("usa:");
		panel.add(etiquetaAccion11, Component.CENTER_ALIGNMENT);
		tipoElemento12 = new JComboBox();
		panel.add(tipoElemento12, Component.CENTER_ALIGNMENT);
		listaElementos12 = new JComboBox();
		panel.add(listaElementos12, Component.CENTER_ALIGNMENT);
		
		setIdioma(idioma);
		this.add(panel);
	}
	
	
	/**
	 * M�todo que traduce la interfaz.
	 * @param s el idioma a traducir: ingl�s o espa�ol.
	 */
	public void setIdioma(String s){
		idioma = s;
		if (s.equals("ingles")){
			tipoElemento11.removeAllItems();
			tipoElemento11.addItem("Machine");
			tipoElemento11.addItem("Tool");
			tipoElemento11.addItem("Part");
			tipoElemento11.addItem("Cooling system");
			etiquetaTipoElemento11.setText("Type 1 :");
			etiquetaListaElementos11.setText("Element 1:");
			etiquetaAccion11.setText("is part of:");
			tipoElemento12.removeAllItems();
			tipoElemento12.addItem("Machine");
			tipoElemento12.addItem("Tool");
			tipoElemento12.addItem("Part");
			tipoElemento12.addItem("Cooling system");
			etiquetaTipoElemento12.setText("Type 2:");
			etiquetaListaElementos12.setText("Element 2:");
		}else{
			tipoElemento11.removeAllItems();
			tipoElemento11.addItem("M�quina");
			tipoElemento11.addItem("Herramienta");
			tipoElemento11.addItem("Pieza");
			tipoElemento11.addItem("Sistema de enfriamiento");
			etiquetaTipoElemento11.setText("Tipo de elemento 1:");
			etiquetaListaElementos11.setText("Lista de elementos 1:");
			etiquetaAccion11.setText("es parte de:");
			tipoElemento12.removeAllItems();
			tipoElemento12.addItem("M�quina");
			tipoElemento12.addItem("Herramienta");
			tipoElemento12.addItem("Pieza");
			tipoElemento12.addItem("Sistema de enfriamiento");
			etiquetaTipoElemento12.setText("Tipo de elemento 2:");
			etiquetaListaElementos12.setText("Lista de elementos 2:");
		}
	}
	
	/**
	 * Devuelve el estado actual del tab en una cadena.
	 * @return el estado actual del tab en una cadena.
	 */
	public String toString(){
		String s = tipoElemento11.getSelectedItem().toString() + " ";
		s = s +	listaElementos11.getSelectedItem().toString() + " " + etiquetaAccion11.getText() + " ";
		s = s + tipoElemento12.getSelectedItem().toString() + " ";;
		s = s +	listaElementos12.getSelectedItem().toString() + " " + etiquetaAccion11.getText() + "\n";
		return s;
	}

}
