package interfaz.rules;


import interfaz.Window;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * Implementa el di�logo para establecer una relaci�n es_parte_de entre 2 componentes.
 * @author M� Angeles Broull�n Lozano y Ana M� Fern�ndez Hern�ndez.
 * @version 2.0
 */
public class DialogRulePartOf extends JDialog implements ActionListener{

	//ATRIBUTOS
	/**
	 * El serial por defecto.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * El idioma actual de la ventana. 
	 */
	private String idioma;
	/**
	 * La ventana controlada.
	 */
	private Window ventanaControlada;
	/**
	 * Componente primero.
	 */
	private JComboBox elemento1;
	/**
	 * La etiqueta de regla entre los 2 elementos.
	 */
	private JLabel regla;
	/**
	 * Componente segundo.
	 */
	private JComboBox elemento2;
	/**
	 * El bot�n para aceptar.
	 */
	private JButton botonAceptar;
	/**
	 * El bot�n para cancelar.
	 */
	private JButton botonCancelar;
	
	//M�TODOS
	/**
	 * Constructor con un par�metro.
	 * @param id el idioma seleccionado.
	 * @param els los elementos con los que se trabajar�.
	 */
	public DialogRulePartOf(String id, Window v){
		super(v, true);
		ventanaControlada = v;
		idioma = "ingles";
		this.setLayout(new BorderLayout());
		
		//componentes
		JPanel comp = new JPanel();
		comp.setLayout(new FlowLayout());
		elemento1 = new JComboBox(ventanaControlada.getNombresComponentes());
		comp.add(elemento1);
		regla = new JLabel("");
		comp.add(regla);
		elemento2 = new JComboBox(ventanaControlada.getNombresComponentes());
		comp.add(elemento2);
		this.add(comp, BorderLayout.NORTH);
		
		//botones
		JPanel botones = new JPanel();
		botones.setLayout(new FlowLayout());
		botonAceptar = new JButton();
		botonAceptar.addActionListener(this);
		botones.add(botonAceptar);
		botonCancelar = new JButton();
		botonCancelar.addActionListener(this);
		botones.add(botonCancelar);
		this.getContentPane().add(botones, BorderLayout.SOUTH);
		
		//idioma y tama�o
		setIdioma(id);
	}
	
	/**
	 * Traduce el texto al idioma seleccionado.
	 * @param id el idioma al que queremos traducir.
	 */
	private void setIdioma(String id){
		idioma = id;
		if (id.equals("espa�ol")){
			this.setTitle("Establecer una regla 'es parte de'");
			regla.setText("es parte de");
			botonAceptar.setText("Aceptar");
			botonCancelar.setText("Cancelar");
		}else{
			this.setTitle("Set a rule 'is part of'");
			regla.setText("is part of");
			botonAceptar.setText("Accept");
			botonCancelar.setText("Cancel");
		}
	}
	
	/**
	 * Comprueba si las opciones son correctas para proceder con la ejecucion. 
	 * @param s1 la primera opcion.
	 * @param s2 la segunda opcion.
	 * @return si las opciones son correctas para proceder con la ejecucion.
	 */
	private boolean esCorrecto(String s1, String s2){
		return (!(s1.equals(s2)));
	}
	

	/**
	 * M�todo que controla las acciones de la ventana. 
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(botonAceptar)) {
			if (esCorrecto( (String) elemento1.getSelectedItem(), (String) elemento2.getSelectedItem() )){
				System.out.println("Acepto");
				String v1 = (String) elemento1.getSelectedItem();
				String v2 = (String) elemento2.getSelectedItem();
				ventanaControlada.addArista(v1, v2, "parte");
				this.dispose();
			}else{
				//mensaje de error
				String mensaje ="You must relate 2 diferent items."; 					
				if (idioma.equals("espa�ol")){
					mensaje = "Debe relacionar 2 componentes diferentes.";
				}
				JOptionPane.showMessageDialog(ventanaControlada,
				    mensaje,
				    "Error",
				    JOptionPane.ERROR_MESSAGE);
			}
			
		}else{
			if (e.getSource().equals(botonCancelar)) {
				System.out.println("Cancelo");
				this.dispose();
			}
		}
		
	}
}
