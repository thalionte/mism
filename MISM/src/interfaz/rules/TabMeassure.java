package interfaz.rules;

import java.awt.Component;
import java.awt.GridLayout;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;


/**
 * El panel para actualizar reglas de relacion entre un sensor y un componente, midiendo.
 * @author M� Angeles Broull�n Lozano y Ana M� Fern�ndez Hern�ndez.
 * @version 2.0
 */
public class TabMeassure extends JPanel{
	
	//ATRIBUTOS
	/**
	 * El serial de JDialog.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * El controlador.
	 */
	@SuppressWarnings("unused")
	private ControlAddRule control;
	/**
	 * El idioma de la ventana.
	 */
	private String idioma;
	/**
	 * La etiqueta con la lista de elementos de un mismo determinado tipo.
	 */
	private JLabel etiquetaListaElementos11;
	/**
	 * La los elementos de un determinado tipo.
	 */
	protected JComboBox listaElementos11;
	/**
	 * La etiqueta de accion a definir.
	 */
	private JLabel etiquetaAccion11;
	/**
	 * La etiqueta del segundo tipo de elemento.
	 */
	private JLabel etiquetaTipoElemento12;
	/**
	 * Componente segundo.
	 */
	protected JComboBox tipoElemento12;
	/**
	 * La etiqueta con la lista de elementos disponibles para una acci�n.
	 */
	private JLabel etiquetaListaElementos12;
	/**
	 * La lista de elementos disponibles para una acci�n.
	 */
	protected JComboBox listaElementos12;
	/**
	 * La etiqueta de la 2� accion a definir.
	 */
	private JLabel etiquetaAccion12;
	/**
	 * La etiqueta con la lista de cantidades disponibles para una medici�n.
	 */
	private JLabel etiquetaListaElementos13;
	/**
	 * La lista de cantidades disponibles para una medici�n.
	 */
	protected JComboBox listaElementos13;

	//METODOS
	/**
	 * M�todo constructor con un par�metro.
	 * @param s el idioma en que debe mostrarse. 
	 */
	public TabMeassure(String s){
		super();
		idioma  = s;
	}
	
	/**
	 * M�todoque a�ade el controlador a la ventana.
	 * @param control el controlador de la ventana.
	 */
	public void a�adirControlador(ControlAddRule c){
		control = c;
	}
	
	/**
	 * Genera el contanido de la ventana.
	 */
	public void crearVista(){
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(2,5));
		
		etiquetaListaElementos11 = new JLabel("Sensor:");
		panel.add(etiquetaListaElementos11, Component.CENTER_ALIGNMENT);
		panel.add(new JLabel(), Component.CENTER_ALIGNMENT);
		
		etiquetaTipoElemento12 = new JLabel("Tipo de componente:");
		panel.add(etiquetaTipoElemento12, Component.CENTER_ALIGNMENT);				
		etiquetaListaElementos12 = new JLabel("Lista de elementos:");
		panel.add(etiquetaListaElementos12, Component.CENTER_ALIGNMENT);
		panel.add(new JLabel(), Component.CENTER_ALIGNMENT);
		etiquetaListaElementos13 = new JLabel("Lista de cantidades:");
		panel.add(etiquetaListaElementos13, Component.CENTER_ALIGNMENT);
		
		
		
		listaElementos11 = new JComboBox();
		panel.add(listaElementos11, Component.CENTER_ALIGNMENT);
		
		etiquetaAccion11 = new JLabel("mide en:");
		panel.add(etiquetaAccion11, Component.CENTER_ALIGNMENT);
		tipoElemento12 = new JComboBox();
		panel.add(tipoElemento12, Component.CENTER_ALIGNMENT);
		listaElementos12 = new JComboBox();
		panel.add(listaElementos12, Component.CENTER_ALIGNMENT);

		etiquetaAccion12 = new JLabel("una cantidad de:");
		panel.add(etiquetaAccion12, Component.CENTER_ALIGNMENT);
		
		listaElementos13 = new JComboBox();
		panel.add(listaElementos13, Component.CENTER_ALIGNMENT);
		
		setIdioma(idioma);
		this.add(panel);
	}
	
	
	/**
	 * M�todo que traduce la interfaz.
	 * @param s el idioma a traducir: ingl�s o espa�ol.
	 */
	public void setIdioma(String s){
		idioma = s;
		if (s.equals("ingles")){
			etiquetaListaElementos11.setText("Sensor:");
			etiquetaAccion11.setText("meassures in:");
			tipoElemento12.removeAllItems();
			tipoElemento12.addItem("Machine");
			tipoElemento12.addItem("Tool");
			tipoElemento12.addItem("Part");
			tipoElemento12.addItem("Cooling system");
			etiquetaTipoElemento12.setText("Type:");
			etiquetaListaElementos12.setText("Element:");
			etiquetaAccion12.setText("a quantity of:");
			etiquetaListaElementos13.setText("Quantity:");
		}else{
			etiquetaListaElementos11.setText("Sensor:");
			etiquetaAccion11.setText("mide en:");
			tipoElemento12.removeAllItems();
			tipoElemento12.addItem("M�quina");
			tipoElemento12.addItem("Herramienta");
			tipoElemento12.addItem("Pieza");
			tipoElemento12.addItem("Sistema de enfriamiento");
			etiquetaTipoElemento12.setText("Tipo de componente:");
			etiquetaListaElementos12.setText("Lista de componentes:");
			etiquetaAccion12.setText("una cantidad de:");
			etiquetaListaElementos13.setText("Cantidad:");
		}
	}
	
	/**
	 * Devuelve el estado actual del tab en una cadena.
	 * @return el estado actual del tab en una cadena.
	 */
	public String toString(){
		String s = "";
		s = s +	listaElementos11.getSelectedItem().toString() + " " + etiquetaAccion11.getText() + " ";
		s = s + tipoElemento12.getSelectedItem().toString() + " ";
		s = s +	listaElementos12.getSelectedItem().toString() + " " + etiquetaAccion11.getText() + "\n";
		return s;
	}

}
