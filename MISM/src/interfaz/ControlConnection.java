package interfaz;

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Implementa el controlador de la ventana de cargar datos.
 * @author M� Angeles Broull�n Lozano y Ana M� Fern�ndez Hern�ndez.
 * @version 2.0
 */
public class ControlConnection implements ActionListener{
	
	// ATRIBUTOS
	/**
	 * La ventana controlada.
	 */
	private TabConnection ventanaControlada;

	// METODOS
	/**
	 * Constructor con par�metro.
	 * @param win la ventana que ser� controlada mediante esta instancia.
	 */
	public ControlConnection(TabConnection win) {
		ventanaControlada = win;
	}

	/**
	 * El m�todo que controla las acciones de la ventana.
	 * @param el evento a manejar.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		// cambiamos el cursor por un reloj
		Cursor cur = new Cursor(Cursor.WAIT_CURSOR);
		ventanaControlada.setCursor(cur);

		// ejecutamos la acci�n correspondiente
		if (e.getSource().equals(ventanaControlada.botonCapturar)) {
			System.out.println("Puls� el bot�n de capturar");
		}


		// dejamos el cursor como estaba
		cur = new Cursor(Cursor.DEFAULT_CURSOR);
		// ventanaControlada.desbloquearBotones();
		ventanaControlada.setCursor(cur);
	}
}
