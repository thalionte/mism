package interfaz.components;

import java.awt.ComponentOrientation;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.Auxiliar;

/**
 * Implementa la pesta�a de nuevo sensor.
 * @author M� Angeles Broull�n Lozano y Ana M� Fern�ndez Hern�ndez.
 * @version 2.0
 */
public class TabAddSensor extends JPanel{
	
	//ATRIBUTOS
	/**
	 * El serial de Component.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * El idioma actual de la ventana,
	 */
	private String idioma;
	/**
	 * El nombre del tab de m�quina.
	 */
	private String nombreTab;
	/**
	 * La etiqueta de nombre del componente.
	 */
	private JLabel etiquetaNombre;
	/**
	 * El nombre del componente.
	 */
	private JTextField nombre;
	/**
	 * Etiqueta del campo m�nimo.
	 */
	private JLabel etiquetaMinimo;
	/**
	 * Etiqueta del campo maximo.
	 */
	private JLabel etiquetaMaximo;
	/**
	 * Etiqueta del campo unidad de medida.
	 */
	private JLabel etiquetaUnidadMedida;	
	/**
	 * Campo minimo.
	 */
	private JTextField minimo;
	/**
	 * Campo forma de maximo.
	 */
	private JTextField maximo;
	/**
	 * Campo forma unidad de medida.
	 */
	private JTextField unidadMedida;
	
	/**
	 * El controlador de la pesta�a.
	 */
	@SuppressWarnings("unused")
	private ControlAddComponent control;
	
	
	//METODOS
	/**
	 * Constructor con par�metros: genera una pieza con el contenido de la pesta�a de almacenar datos.
	 * @param s el dioma en que debe mostrarse.	
	 * @param c el constrolador asignado. 
	 */
	public TabAddSensor(String s, ControlAddComponent c){
		control = c;
		
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(4,2));
		
		JPanel c1 = new JPanel();
		c1.setLayout(new FlowLayout());
		c1.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		etiquetaNombre = new JLabel("Nombre:");
		c1.add(etiquetaNombre);
		panel.add(c1);
		
		JPanel c2 = new JPanel();
		c2.setLayout(new FlowLayout());
		c2.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		nombre = new  JTextField("", 20);
		c2.add(nombre);
		panel.add(c2);
		
		JPanel c3 = new JPanel();
		c3.setLayout(new FlowLayout());
		c3.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		etiquetaMinimo = new JLabel("M�nimo:");
		c3.add(etiquetaMinimo);
		panel.add(c3);
		
		JPanel c4 = new JPanel();
		c4.setLayout(new FlowLayout());
		c4.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		minimo = new  JTextField("", 20);
		c4.add(minimo);
		panel.add(c4);
		
		JPanel c5 = new JPanel();
		c5.setLayout(new FlowLayout());
		c5.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		etiquetaMaximo = new JLabel("Maximo:");
		c5.add(etiquetaMaximo);
		panel.add(c5);
		
		JPanel c6 = new JPanel();
		c6.setLayout(new FlowLayout());
		c6.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		maximo = new  JTextField("", 20);
		c6.add(maximo);
		panel.add(c6);
		
		JPanel c7 = new JPanel();
		c7.setLayout(new FlowLayout());
		c7.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		etiquetaUnidadMedida = new JLabel("Unidad de medida:");
		c7.add(etiquetaUnidadMedida);
		panel.add(c7);
		
		JPanel c8 = new JPanel();
		c8.setLayout(new FlowLayout());
		c8.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		unidadMedida = new  JTextField("", 20);
		c8.add(unidadMedida);
		panel.add(c8);
		
		this.add(panel);
		setIdioma(s);
	
	}
	
	
	/**
	 * M�todo que traduce la interfaz.
	 * @param s el idioma a traducir: ingl�s o espa�ol.
	 */
	public void setIdioma(String s){
		idioma = s;
		if (s.equals("espa�ol")){
			nombreTab = "sensor";
			etiquetaNombre.setText("Nombre:");
			etiquetaMinimo.setText("M�nimo:");
			etiquetaMaximo.setText("M�ximo:");
			etiquetaUnidadMedida.setText("Unidad de medida:");
		}else{
			nombreTab = "sensor";
			etiquetaNombre.setText("Name:");
			etiquetaMinimo.setText("Minimum:");
			etiquetaMaximo.setText("Maximum:");
			etiquetaUnidadMedida.setText("Meassurement unit:");
		}
	}
	
	/**
	 * Comprueba si el formato de la ventana es v�lido.
	 * @return si el formato del contenido de la ventana es v�lido.
	 */
	public String esFormatoValido(){
		String salida = "";
		String min = minimo.getText();
		if (!Auxiliar.esNumerica(min)){
			if (idioma.equals("espa�ol")){
				salida = salida + "El valor de minimo debe ser numerico.\n";
			}else{
				salida = salida + "Minimum must be a number.\n";
			}
		}
		String max = maximo.getText();
		if (!Auxiliar.esNumerica(max)){
			if (idioma.equals("espa�ol")){
				salida = salida + "El valor de maximo debe ser numerico.\n";
			}else{
				salida = salida + "Maximum must be a number.\n";
			}
		}
		salida = salida + rellenarCampos();
		return salida;
	}
	
	/**
	 * Comprueba si han quedado campos sin rellenar.
	 * @return si han quedado campos sin rellenar.
	 */
	private String rellenarCampos(){
		String salida = "";
		if (nombre.getText().equals("") || minimo.getText().equals("")
				|| maximo.getText().equals("") || unidadMedida.getText().equals("") ){
			if (idioma.equals("espa�ol")){
				salida =  "Le faltan campos por rellenar.\n";
			}else{
				salida =  "You must complete all the text fields.\n";
			}
		}
		return salida;
	}
	
	/**
	 * Devuelve el estado actual del tab en una cadena.
	 * @return el estado actual del tab en una cadena.
	 */
	public String toString(){
		String s = nombreTab + "\n";
		s = s + etiquetaNombre.getText() + " " + nombre.getText() + "\n";
		s = s +	etiquetaMinimo.getText() + " " + minimo.getText() + "\n";
		s = s + etiquetaMaximo.getText() + " " + maximo.getText() + "\n";
		s = s + etiquetaUnidadMedida.getText() + " " + unidadMedida.getText() + "\n";
		return s;
	}

}
