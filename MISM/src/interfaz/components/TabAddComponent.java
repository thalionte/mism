package interfaz.components;


import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;

/**
 * Implementa la ventana de di�logo para a�adir un nuevo componente o una nueva herramienta..
 * @author M� Angeles Broull�n Lozano y Ana M� Fern�ndez Hern�ndez.
 * @version 2.0
 */
public class TabAddComponent extends JPanel{
	
	//ATRIBUTOS
	/**
	 * El serial del JFrame.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * El controlador.
	 */
	private ControlAddComponent control;
	/**
	 * El idioma de la ventana.
	 */
	private String idioma;
	/**
	 * Contiene los 4 paneles de componentes posibles.
	 */
	private ButtonGroup botones;
	/**
	 * El boton de selecci�n de Maquina.
	 */
	protected JRadioButton botonMaquina;
	/**
	 * El boton de selecci�n de Herramienta.
	 */
	protected JRadioButton botonHerramienta;
	/**
	 * El boton de selecci�n de Pieza.
	 */
	protected JRadioButton botonPieza;
	/**
	 * El boton de selecci�n de Sistema de enfriamiento.
	 */
	protected JRadioButton botonEnfriamiento;
	/**
	 * Selecciona la opci�n nuevo componente.
	 */
	protected TabMachine tabMaquina;
	/**
	 * Selecciona la opci�n nueva herramienta.
	 */
	protected TabTool tabHerramienta;
	/**
	 * Selecciona la opci�n nueva hpieza a mecanizar.
	 */
	protected TabPart tabPieza;
	/**
	 * Selecciona la opci�n nuevo sistema de enfriamiento.
	 */
	protected TabCooling tabEnfriamiento;
	/**
	 * El boton de aceptar.
	 */
	protected JButton aceptar;
	/**
	 * El boton de cancelar.
	 */
	protected JButton cancelar;
	
	
	//METODOS
	/**
	 * M�todo constructor con un par�metro.
	 * @param s el idioma en que debe mostrarse.
	 */
	public TabAddComponent(String s){
		super();
		idioma  = s;
	}
	
	/**
	 * M�todoque a�ade el controlador a la ventana.
	 * @param control el controlador de la ventana.
	 */
	public void a�adirControlador(ControlAddComponent c){
		control = c;
	}
	
	/**
	 * Genera el contanido de la ventana.
	 */
	public void crearVista(){		
		
		//el panel de contenidos
		botonMaquina = new JRadioButton("Maquina");
		botonHerramienta = new JRadioButton("Herramienta");
		botonPieza = new JRadioButton("Pieza");
		botonEnfriamiento = new JRadioButton("Sistema de enfriamiento");
		botones = new ButtonGroup();
		botones.add(botonMaquina);
		botones.add(botonHerramienta);
		botones.add(botonPieza);
		botones.add(botonEnfriamiento);
		botonMaquina.setSelected(true);
		botonMaquina.addActionListener(control);
		botonHerramienta.addActionListener(control);
		botonPieza.addActionListener(control);
		botonEnfriamiento.addActionListener(control);
		
		JPanel panelBotones = new JPanel();
		panelBotones.add(botonMaquina);
		panelBotones.add(botonHerramienta);
		panelBotones.add(botonPieza);
		panelBotones.add(botonEnfriamiento);
		tabMaquina =  new TabMachine(control);
		tabHerramienta =  new TabTool(control);
		tabPieza =  new TabPart(control);
		tabEnfriamiento =  new TabCooling(control);
		this.add(panelBotones);
		
		//separador
		JSeparator s2 = new JSeparator(SwingConstants.HORIZONTAL);
		this.add(s2);

		//el panel central
		this.add(tabMaquina);
		this.add(tabHerramienta);
		this.add(tabPieza);
		this.add(tabEnfriamiento);
		mostrarElementos();
		
		//visualizaci�n
		setIdioma(idioma);
		setVisible(true);
	}
	
	/**
	 * Muestra los componetes apropiados si se da la condici�n.
	 */
	public void mostrarElementos(){
		if (botonMaquina.isSelected()){
			tabMaquina.setVisible(true);
			tabHerramienta.setVisible(false);
			tabPieza.setVisible(false);
			tabEnfriamiento.setVisible(false);
		}else{
			if (botonHerramienta.isSelected()){
				tabMaquina.setVisible(false);
				tabHerramienta.setVisible(true);
				tabPieza.setVisible(false);
				tabEnfriamiento.setVisible(false);
			}else{
				if (botonPieza.isSelected()){
					tabMaquina.setVisible(false);
					tabHerramienta.setVisible(false);
					tabPieza.setVisible(true);
					tabEnfriamiento.setVisible(false);
				}else{
					tabMaquina.setVisible(false);
					tabHerramienta.setVisible(false);
					tabPieza.setVisible(false);
					tabEnfriamiento.setVisible(true);
				}
			}
		}
	}
	
	
	/**
	 * M�todo que traduce la interfaz.
	 * @param s el idioma a traducir: ingl�s o espa�ol.
	 */
	public void setIdioma(String s){
		idioma = s;
		if (s.equals("espa�ol")){
			botonMaquina.setText("Maquina");
			botonHerramienta.setText("Herramienta");
			botonPieza.setText("Pieza");
			botonEnfriamiento.setText("Sistema de enfriamiento");
			tabMaquina.setIdioma("espa�ol");
			tabHerramienta.setIdioma("espa�ol");
			tabPieza.setIdioma("espa�ol");
			tabEnfriamiento.setIdioma("espa�ol");
		}else{
			botonMaquina.setText("Machine");
			botonHerramienta.setText("Tool");
			botonPieza.setText("Part");
			botonEnfriamiento.setText("Cooling system");
			tabMaquina.setIdioma("ingles");
			tabHerramienta.setIdioma("ingles");
			tabPieza.setIdioma("ingles");
			tabEnfriamiento.setIdioma("ingles");
		}
	}
	
	/**
	 * Comprueba si el formato de la ventana es v�lido.
	 * @return si el formato del contenido de la ventana es v�lido.
	 */
	public String esFormatoValido(){
		String s = "";
		if (botonMaquina.isSelected()){
			s = s + tabMaquina.esFormatoValido();
		}else{
			if (botonHerramienta.isSelected()){
				s = s + tabHerramienta.esFormatoValido();
			}else{
				if (botonPieza.isSelected()){
					s = s + tabPieza.esFormatoValido();
				}else{
					if (botonEnfriamiento.isSelected()){
						s = s + tabEnfriamiento.esFormatoValido();
					}
				}
			}
		}
		return s;
	}
	
	/**
	 * Devuelve el contenido de la pesta�a activa actualmente en una cadena.
	 * @return el contenido de la pesta�a activa actualmente en una cadena.
	 */
	public String toString(){
		String s = "";
		if (botonMaquina.isSelected()){
			s = s + tabMaquina.toString();
		}else{
			if (botonHerramienta.isSelected()){
				s = s + tabHerramienta.toString();
			}else{
				if (botonPieza.isSelected()){
					s = s + tabPieza.toString();
				}else{
					if (botonEnfriamiento.isSelected()){
						s = s + tabEnfriamiento.toString();
					}
				}
			}
		}
		return s;
	}
}

