package interfaz.components;


import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

/**
 * Implementa el controlador de una ventana de di�logo para a�adir un nuevo componente o sensor.
 * @author M� Angeles Broull�n Lozano y Ana M� Fern�ndez Hern�ndez.
 * @version 2.0
 */
public class ControlAddComponent implements ActionListener{

	//ATRIBUTOS
	/**
	 * La ventana a controlar.
	 */
	private WindowAddComponent ventanaControlada;
	
	//METODOS
	/**
	 * Constructor con un par�metro. 
	 * @param v la ventana a controlar.
	 */
	public ControlAddComponent(WindowAddComponent v){
		ventanaControlada = v;
		ventanaControlada.setModal(true);
	}
	
	/**
	 * El m�todo que controla las acciones de la ventana.
	 * @param el evento a manejar.
	 */
	public void actionPerformed(ActionEvent e) {
		//cambiamos el cursor por un reloj
		Cursor cur = new Cursor(Cursor.WAIT_CURSOR);
        ventanaControlada.setCursor(cur); 
        
        //ejecutamos la acci�n correspondiente   		
		if (e.getSource().equals(ventanaControlada.aceptar)){
			System.out.println("Ha pulsado aceptar.");
			String validez = ventanaControlada.esFormatoValido();
			if ( validez.equals("") ){
				System.out.println(ventanaControlada.toString());
				//TODO
				/*
				 * Lo que tenemos en el toString que imprimimos en pantalla
				 * en la linea anterior debe ir a la base de datos, cambia el formato
				 * si te place
				 */
				ventanaControlada.dispose();
			}else{
				JOptionPane.showMessageDialog(ventanaControlada,
    				    validez,
    				    "Error",
    				    JOptionPane.WARNING_MESSAGE);
			}
			
					
		}else{
			if (e.getSource().equals(ventanaControlada.cancelar)){
				System.out.println("Ha pulsado cancelar.");
				ventanaControlada.dispose();
			}else{
				if (e.getSource().equals(ventanaControlada.tabComponente.botonMaquina)){
					ventanaControlada.tabComponente.mostrarElementos();
				}else{
					if (e.getSource().equals(ventanaControlada.tabComponente.botonHerramienta)){
						ventanaControlada.tabComponente.mostrarElementos();
					}else{
						if (e.getSource().equals(ventanaControlada.tabComponente.botonPieza)){
							ventanaControlada.tabComponente.mostrarElementos();
						}else{
							if (e.getSource().equals(ventanaControlada.tabComponente.botonEnfriamiento)){
								ventanaControlada.tabComponente.mostrarElementos();
							}else{
								
							}
						}
					}
				}
			}
		}
		
		//dejamos el cursor como estaba
		cur = new Cursor(Cursor.DEFAULT_CURSOR);
		//ventanaControlada.desbloquearBotones();
        ventanaControlada.setCursor(cur);
	}
	
	/*
	public static void main(String[] args){
		WindowAddComponent v = new WindowAddComponent("ingles");
		v.a�adirControlador(new ControlAddComponent(v));
		v.crearVista();
	}
	*/

}
