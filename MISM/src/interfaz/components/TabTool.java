package interfaz.components;

import java.awt.ComponentOrientation;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.Auxiliar;

/**
 * Implementa la pesta�a de nuevo componente (herramienta).
 * @author M� Angeles Broull�n Lozano y Ana M� Fern�ndez Hern�ndez.
 * @version 2.0
 */
public class TabTool extends JPanel{
	//ATRIBUTOS
	/**
	 * El serial de Component.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * El idioma actual de la ventana.
	 */
	private String idioma;
	/**
	 * El nombre del tab de m�quina.
	 */
	private String nombreTab;
	/**
	 * La etiqueta de nombre del componente.
	 */
	private JLabel etiquetaNombre;
	/**
	 * El nombre del componente.
	 */
	private JTextField nombre;
	/**
	 * Etiqueta del campo material.
	 */
	private JLabel etiquetaMaterial;
	/**
	 * Etiqueta del campo geometr�a de la pieza.
	 */
	private JLabel etiquetaGeometria;
	/**
	 * Etiqueta del campo radio-nariz.
	 */
	private JLabel etiquetaRadioNariz;
	/**
	 * Etiqueta del campo n�mero de dientes.
	 */
	private JLabel etiquetaNumeroDientes;
	/**
	 * Etiqueta del campo n�mero de filos.
	 */
	private JLabel etiquetaFilos;
	/**
	 * Etiqueta del campo longitud.
	 */
	private JLabel etiquetaLongitud;
	/**
	 * Etiqueta del campo recubrimiento.
	 */
	private JLabel etiquetaRecubrimiento;
	/**
	 * Campo material.
	 */
	private JTextField material;
	/**
	 * Campo geometr�a.
	 */
	private JTextField geometria;
	/**
	 * Campo radio-nariz.
	 */
	private JTextField radioNariz;
	/**
	 * Campo de unidades de radio-nariz.
	 */
	private JTextField unidadesRadioNariz;
	/**
	 * Campo n�mero de dientes.
	 */
	private JTextField numeroDientes;
	/**
	 * Campo n�mero de filos.
	 */
	private JTextField filos;
	/**
	 * Campo longitud.
	 */
	private JTextField longitud;
	/**
	 * Campo de unidades de longitud.
	 */
	private JTextField unidadesLongitud;
	/**
	 * Campo recubrimiento.
	 */
	private JTextField recubrimiento;
	
	/**
	 * El controlador de la pesta�a.
	 */
	@SuppressWarnings("unused")
	private ControlAddComponent control;
	
	//METODOS
	/**
	 * Constructor con par�metros: genera una herramienta con el contenido de la pesta�a de almacenar datos.
	 * @param c el constrolador asignado. 
	 */
	public TabTool(ControlAddComponent c){
		
		control = c;
		
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(8,2));
		
		JPanel c1 = new JPanel();
		c1.setLayout(new FlowLayout());
		c1.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		etiquetaNombre = new JLabel("Nombre:");
		c1.add(etiquetaNombre);
		panel.add(c1);
		
		JPanel c2 = new JPanel();
		c2.setLayout(new FlowLayout());
		c2.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		nombre = new  JTextField("", 20);
		c2.add(nombre);
		panel.add(c2);
		
		JPanel c3 = new JPanel();
		c3.setLayout(new FlowLayout());
		c3.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		etiquetaMaterial = new JLabel("Material (M):");
		c3.add(etiquetaMaterial);
		panel.add(c3);
		
		JPanel c4 = new JPanel();
		c4.setLayout(new FlowLayout());
		c4.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		material = new  JTextField("", 20);
		c4.add(material);
		panel.add(c4);
		
		JPanel c5 = new JPanel();
		c5.setLayout(new FlowLayout());
		c5.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		etiquetaGeometria = new JLabel("Geometr�a (G):");
		c5.add(etiquetaGeometria);
		panel.add(c5);
		
		JPanel c6 = new JPanel();
		c6.setLayout(new FlowLayout());
		c6.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		geometria = new  JTextField("", 20);
		c6.add(geometria);
		panel.add(c6);
		
		JPanel c7 = new JPanel();
		c7.setLayout(new FlowLayout());
		c7.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		etiquetaRadioNariz = new JLabel("Radio-nariz (m):");
		c7.add(etiquetaRadioNariz);
		panel.add(c7);
		
		JPanel c8 = new JPanel();
		c8.setLayout(new FlowLayout());
		c8.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		JPanel panelRadioNariz = new JPanel();
		radioNariz = new  JTextField("", 20);
		panelRadioNariz.add(radioNariz);
		unidadesRadioNariz = new JTextField("", 3);
		panelRadioNariz.add(unidadesRadioNariz);
		c8.add(panelRadioNariz);
		panel.add(c8);
		
		JPanel c9 = new JPanel();
		c9.setLayout(new FlowLayout());
		c9.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		etiquetaNumeroDientes = new JLabel("N�mero de dientes (n):");
		c9.add(etiquetaNumeroDientes);
		panel.add(c9);
		
		JPanel c10 = new JPanel();
		c10.setLayout(new FlowLayout());
		c10.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		numeroDientes = new  JTextField("", 20);
		c10.add(numeroDientes);
		panel.add(c10);
		
		JPanel c11 = new JPanel();
		c11.setLayout(new FlowLayout());
		c11.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		etiquetaFilos = new JLabel("N�mero de filos:");
		c11.add(etiquetaFilos);
		panel.add(c11);
		
		JPanel c12 = new JPanel();
		c12.setLayout(new FlowLayout());
		c12.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		filos = new  JTextField("", 20);
		c12.add(filos);
		panel.add(c12);
		
		JPanel c13 = new JPanel();
		c13.setLayout(new FlowLayout());
		c13.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		etiquetaLongitud = new JLabel("Longitud:");
		c13.add(etiquetaLongitud);
		panel.add(c13);
		
		JPanel c14 = new JPanel();
		c14.setLayout(new FlowLayout());
		c14.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		JPanel panelLongitud = new JPanel();
		longitud = new  JTextField("", 20);
		panelLongitud.add(longitud);
		unidadesLongitud = new JTextField("", 3);
		panelLongitud.add(unidadesLongitud);
		c14.add(panelLongitud);
		panel.add(c14);
		
		JPanel c15 = new JPanel();
		c15.setLayout(new FlowLayout());
		c15.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		etiquetaRecubrimiento = new JLabel("Recubrimiento:");
		c15.add(etiquetaRecubrimiento);
		panel.add(c15);
		
		JPanel c16 = new JPanel();
		c16.setLayout(new FlowLayout());
		c16.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		recubrimiento = new  JTextField("", 20);
		c16.add(recubrimiento);
		panel.add(c16);
	
		this.add(panel);
		
	}
	
	
	/**
	 * M�todo que traduce la interfaz.
	 * @param s el idioma a traducir: ingl�s o espa�ol.
	 */
	public void setIdioma(String s){
		idioma = s;
		if (s.equals("espa�ol")){
			nombreTab = "herramienta";
			etiquetaNombre.setText("Nombre:");
			etiquetaMaterial.setText("Material (M):");
			etiquetaGeometria.setText("Geometr�a (G):");
			etiquetaRadioNariz.setText("Radio-nariz (m):");
			etiquetaNumeroDientes.setText("N�mero de dientes (n):");
			etiquetaFilos.setText("N� de filos:");
			etiquetaLongitud.setText("Longitud:");
			etiquetaRecubrimiento.setText("Recubrimiento:");
				
		}else{
			nombreTab = "tool";
			etiquetaNombre.setText("Name:");
			etiquetaMaterial.setText("Material (M):");
			etiquetaGeometria.setText("Geometry (G):");
			etiquetaRadioNariz.setText("Nose radius (m):");
			etiquetaNumeroDientes.setText("Number of teeth (n):");
			etiquetaFilos.setText("Number of edges:");
			etiquetaLongitud.setText("Length:");
			etiquetaRecubrimiento.setText("Coating:");
		}
	}
	
	/**
	 * Comprueba si el formato de la ventana es v�lido.
	 * @return si el formato del contenido de la ventana es v�lido.
	 */
	public String esFormatoValido(){
		String salida = "";
		String rad = radioNariz.getText();
		if (!Auxiliar.esNumerica(rad)){
			if (idioma.equals("espa�ol")){
				salida = salida + "El valor de radio nariz del husillo debe ser numerico.\n";
			}else{
				salida = salida + "Nose Radius must be a number.\n";
			}
		}
		String num = numeroDientes.getText();
		if (!Auxiliar.esNumerica(num)){
			if (idioma.equals("espa�ol")){
				salida = salida + "El valor de numero de dientes debe ser numerico.\n";
			}else{
				salida = salida + "Number of teeth must be a number.\n";
			}
		}
		String fil = filos.getText();
		if (!Auxiliar.esNumerica(fil)){
			if (idioma.equals("espa�ol")){
				salida = salida + "El valor de numero de filos debe ser numerico.\n";
			}else{
				salida = salida + "Number of edges must be a number.\n";
			}
		}
		String lon = longitud.getText();
		if (!Auxiliar.esNumerica(lon)){
			if (idioma.equals("espa�ol")){
				salida = salida + "El valor de longitud debe ser numerico.\n";
			}else{
				salida = salida + "Length must be a number.\n";
			}
		}
		salida = salida + rellenarCampos();
		return salida;
	}
	
	/**
	 * Comprueba si han quedado campos sin rellenar.
	 * @return si han quedado campos sin rellenar.
	 */
	private String rellenarCampos(){
		String salida = "";
		if (nombre.getText().equals("") || material.getText().equals("")
				|| geometria.getText().equals("")
				|| radioNariz.getText().equals("") || unidadesRadioNariz.getText().equals("")
				|| numeroDientes.getText().equals("") || filos.getText().equals("")
				|| longitud.getText().equals("") || recubrimiento.getText().equals("")){
			if (idioma.equals("espa�ol")){
				salida =  "Le faltan campos por rellenar.\n";
			}else{
				salida =  "You must complete all the text fields.\n";
			}
		}
		return salida;
	}
	
	/**
	 * Devuelve el estado actual del tab en una cadena.
	 * @return el estado actual del tab en una cadena.
	 */
	public String toString(){
		String s = nombreTab + "\n";
		s = s + etiquetaNombre.getText() + " " + nombre.getText() + "\n";
		s = s + etiquetaMaterial.getText() + " " + material.getText() + "\n";
		s = s + etiquetaGeometria.getText() + " " + geometria.getText() + "\n";
		s = s + etiquetaRadioNariz.getText() + " " + radioNariz.getText() + " " + unidadesRadioNariz.getText() + "\n";
		s = s + etiquetaNumeroDientes.getText() + " " + numeroDientes.getText() + "\n";
		s = s + etiquetaFilos.getText() + " " + filos.getText() + "\n";
		s = s + etiquetaLongitud.getText() + " " + longitud.getText() + " " + unidadesLongitud.getText() + "\n";
		s = s + etiquetaRecubrimiento.getText() + " " + recubrimiento.getText() + "\n";
		return s;
	}

}
