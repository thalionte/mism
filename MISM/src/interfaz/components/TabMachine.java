package interfaz.components;

import java.awt.ComponentOrientation;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.Auxiliar;

/**
 * Implementa la pesta�a de nuevo componente (m�quina).
 * @author M� Angeles Broull�n Lozano y Ana M� Fern�ndez Hern�ndez.
 * @version 2.0
 */
public class TabMachine extends JPanel{
	
	//ATRIBUTOS
	/**
	 * El serial de Component.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * El idioma actual de la ventana.
	 */
	private String idioma;
	/**
	 * El nombre del tab de m�quina.
	 */
	private String nombreTab;
	/**
	 * La etiqueta de nombre del componente.
	 */
	private JLabel etiquetaNombre;
	/**
	 * El nombre del componente.
	 */
	private JTextField nombre;
	/**
	 * Etiqueta del campo velocidad de rotaci�n del husillo.
	 */
	private JLabel etiquetaVelocidadHusillo;
	/**
	 * Etiqueta del campo potencia.
	 */
	private JLabel etiquetaPotencia;
	/**
	 * Etiqueta del campo profundidad de corte m�xima.
	 */
	private JLabel etiquetaProfundidadMax;
	/**
	 * Etiqueta del campo avance.
	 */
	private JLabel etiquetaAvance;
	/**
	 * Campo velocidad de rotaci�n del husillo.
	 */
	private JTextField velocidadHusillo;
	/**
	 * Campo unidad de velocidad.
	 */
	private JTextField unidadVelocidad;
	/**
	 * Campo potencia.
	 */
	private JTextField potencia;
	/**
	 * Campo unidad de potencia.
	 */
	private JTextField unidadPotencia;
	/**
	 * Campo profundidad de corte m�xima.
	 */
	private JTextField profundidadMax;
	/**
	 * Campo de unidad de prfundidad.
	 */
	private JTextField unidadProfundidad;
	/**
	 * Campo avance.
	 */
	private JTextField avance;
	/**
	 * Campo de unidad de avance.
	 */
	private JTextField unidadAvance;
	
	/**
	 * El controlador de la pesta�a.
	 */
	@SuppressWarnings("unused")
	private ControlAddComponent control;
	
	//METODOS
	/**
	 * Constructor con par�metros: genera una m�quina con el contenido de la pesta�a de almacenar datos.
	 * @param c el constrolador asignado. 
	 */
	public TabMachine(ControlAddComponent c){
		
		control = c;
		
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(5,2));
		
		JPanel c1 = new JPanel();
		c1.setLayout(new FlowLayout());
		c1.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		etiquetaNombre = new JLabel("Nombre:");
		c1.add(etiquetaNombre);
		panel.add(c1);
		
		JPanel c2 = new JPanel();
		c2.setLayout(new FlowLayout());
		c2.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		nombre = new  JTextField("", 20);
		c2.add(nombre);
		panel.add(c2);
		
		JPanel c3 = new JPanel();
		c3.setLayout(new FlowLayout());
		c3.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		etiquetaVelocidadHusillo = new JLabel("Velocidad de rotaci�n del husillo (F):");
		c3.add(etiquetaVelocidadHusillo);
		panel.add(c3);
		
		JPanel c4 = new JPanel();
		c4.setLayout(new FlowLayout());
		c4.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		JPanel panelVelocidad = new JPanel();
		velocidadHusillo = new  JTextField("", 20);
		panelVelocidad.add(velocidadHusillo);
		unidadVelocidad = new  JTextField("", 3);
		panelVelocidad.add(unidadVelocidad);
		c4.add(panelVelocidad);
		panel.add(c4);
		
		JPanel c5 = new JPanel();
		c5.setLayout(new FlowLayout());
		c5.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		etiquetaPotencia = new JLabel("Potencia (p):");
		c5.add(etiquetaPotencia);
		panel.add(c5);
		
		JPanel c6 = new JPanel();
		c6.setLayout(new FlowLayout());
		c6.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		JPanel panelPotencia = new JPanel();
		potencia = new  JTextField("", 20);
		panelPotencia.add(potencia);
		unidadPotencia = new  JTextField("", 3);
		panelPotencia.add(unidadPotencia);		
		c6.add(panelPotencia);
		panel.add(c6);
		
		JPanel c7 = new JPanel();
		c7.setLayout(new FlowLayout());
		c7.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		etiquetaProfundidadMax = new JLabel("Profundidad de corte m�xima (f):");
		c7.add(etiquetaProfundidadMax);
		panel.add(c7);
		
		JPanel c8 = new JPanel();
		c8.setLayout(new FlowLayout());
		c8.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		JPanel panelProfundidad = new JPanel();
		profundidadMax = new  JTextField("", 20);
		panelProfundidad.add(profundidadMax);
		unidadProfundidad = new  JTextField("", 3);
		panelProfundidad.add(unidadProfundidad);
		c8.add(panelProfundidad);
		panel.add(c8);
		
		JPanel c9 = new JPanel();
		c9.setLayout(new FlowLayout());
		c9.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		etiquetaAvance = new JLabel("Avance (S):");
		c9.add(etiquetaAvance);
		panel.add(c9);
		
		JPanel c10 = new JPanel();
		c10.setLayout(new FlowLayout());
		c10.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		JPanel panelAvance = new JPanel();
		avance = new  JTextField("", 20);
		panelAvance.add(avance);
		unidadAvance = new  JTextField("", 3);
		panelAvance.add(unidadAvance);	
		c10.add(panelAvance);
		panel.add(c10);
		
		this.add(panel);
	
	}
	
	
	/**
	 * M�todo que traduce la interfaz.
	 * @param s el idioma a traducir: ingl�s o espa�ol.
	 */
	public void setIdioma(String s){
		idioma = s;
		if (s.equals("espa�ol")){
			nombreTab = "maquina";
			etiquetaNombre.setText("Nombre:");
			etiquetaVelocidadHusillo.setText("Velocidad de rotaci�n del husillo (S):");
			etiquetaPotencia.setText("Potencia (p):");
			etiquetaProfundidadMax.setText("Profundidad de corte m�xima (f):");
			etiquetaAvance.setText("Avance (F):");
				
		}else{
			nombreTab = "machine";
			etiquetaNombre.setText("Name:");
			etiquetaVelocidadHusillo.setText("Spindle (S):");
			etiquetaPotencia.setText("Power (p):");
			etiquetaProfundidadMax.setText("Maximum depth (f):");
			etiquetaAvance.setText("Forward (F):");
		}
	}
	
	/**
	 * Comprueba si el formato de la ventana es v�lido.
	 * @return si el formato del contenido de la ventana es v�lido.
	 */
	public String esFormatoValido(){
		String salida = "";
		String vel = velocidadHusillo.getText();
		if (!Auxiliar.esNumerica(vel)){
			if (idioma.equals("espa�ol")){
				salida = salida + "El valor de velocidad del husillo debe ser numerico.\n";
			}else{
				salida = salida + "Spindle must be a number.\n";
			}
		}
		String pot = potencia.getText();
		if (!Auxiliar.esNumerica(pot)){
			if (idioma.equals("espa�ol")){
				salida = salida + "El valor de potencia debe ser numerico.\n";
			}else{
				salida = salida + "Power must be a number.\n";
			}
		}
		String pro = profundidadMax.getText();
		if (!Auxiliar.esNumerica(pro)){
			if (idioma.equals("espa�ol")){
				salida = salida + "El valor de profundidad maxima debe ser numerico.\n";
			}else{
				salida = salida + "Maximum depth must be a number.\n";
			}
		}
		String av = avance.getText();
		if (!Auxiliar.esNumerica(av)){
			if (idioma.equals("espa�ol")){
				salida = salida + "El valor de avance debe ser numerico.\n";
			}else{
				salida = salida + "Forward must be a number.\n";
			}
		}
		salida = salida + rellenarCampos();
		return salida;
	}
	
	/**
	 * Comprueba si han quedado campos sin rellenar.
	 * @return si han quedado campos sin rellenar.
	 */
	private String rellenarCampos(){
		String salida = "";
		if (nombre.getText().equals("") || velocidadHusillo.getText().equals("")
				|| potencia.getText().equals("")
				|| profundidadMax.getText().equals("") || avance.getText().equals("")){
			if (idioma.equals("espa�ol")){
				salida =  "Le faltan campos por rellenar.\n";
			}else{
				salida =  "You must complete all the text fields.\n";
			}
		}
		return salida;
	}

	/**
	 * Devuelve el estado actual del tab en una cadena.
	 * @return el estado actual del tab en una cadena.
	 */
	public String toString(){
		String s = nombreTab + "\n";
		s = s + etiquetaNombre.getText() + " " + nombre.getText() + "\n";
		s = s + etiquetaVelocidadHusillo.getText() + " " + velocidadHusillo.getText() + " " + unidadVelocidad.getText() + "\n";
		s = s + etiquetaPotencia.getText() + " " + potencia.getText() + " " + unidadPotencia.getText() + "\n";
		s = s + etiquetaProfundidadMax.getText() + " " + profundidadMax.getText() + " " + unidadProfundidad.getText() + "\n";
		s = s + etiquetaAvance.getText() + " " + avance.getText() + " " + unidadAvance.getText() + "\n";
		return s;
	}
	
}
