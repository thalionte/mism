package interfaz.components;


import java.awt.Container;
import java.awt.FlowLayout;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;

/**
 * Implementa la ventana de di�logo para a�adir un nuevo componente o una nueva herramienta.
 * @author M� Angeles Broull�n Lozano y Ana M� Fern�ndez Hern�ndez.
 * @version 2.0
 */
public class WindowAddComponent extends JDialog{
	
	//ATRIBUTOS
	/**
	 * El serial del JFrame.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * El controlador.
	 */
	private ControlAddComponent control;
	/**
	 * El idioma de la ventana.
	 */
	private String idioma;
	/**
	 * Contiene los 2 paneles de componentes posibles.
	 */
	private JTabbedPane panel;
	/**
	 * El nombre del primer tab.
	 */
	private String nombreTab1;
	/**
	 * El nombre del segundo tab.
	 */
	private String nombreTab2;
	/**
	 * El nombre del tercer tab.
	 */
	private String nombreTab3;
	/**
	 * Selecciona la opci�n nuevo componente.
	 */
	protected TabAddComponent tabComponente;
	/**
	 * Selecciona la opci�n nuevo sensor.
	 */
	protected TabAddSensor tabSensor;
	/**
	 * Selecciona la opci�n nueva cantidad.
	 */
	protected TabAddQuantity tabCantidad;
	/**
	 * El boton de aceptar.
	 */
	protected JButton aceptar;
	/**
	 * El boton de cancelar.
	 */
	protected JButton cancelar;
	
	
	//METODOS
	/**
	 * M�todo constructor con un par�metro.
	 * @param s el idioma en que debe mostrarse. 
	 */
	public WindowAddComponent(String s){
		super();
		idioma  = s;
	}
	
	/**
	 * M�todo que a�ade el controlador a la ventana.
	 * @param control el controlador de la ventana.
	 */
	public void a�adirControlador(ControlAddComponent c){
		control = c;
	}
	
	/**
	 * Genera el contanido de la ventana.
	 */
	public void crearVista(){
		Container container = getContentPane();
		container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
		
		//el panel de contenidos
		panel = new JTabbedPane();
		nombreTab1 = "Componente";
		nombreTab2 = "Sensor";
		tabComponente = new TabAddComponent(idioma);
		tabComponente.a�adirControlador(control);
		tabComponente.crearVista();
		panel.addTab(nombreTab1, tabComponente);
		tabSensor = new TabAddSensor(idioma, control);
		panel.addTab(nombreTab2, tabSensor);
		tabCantidad = new TabAddQuantity(idioma, control);
		panel.addTab(nombreTab2, tabCantidad);
		
		container.add(panel);
		
		//separador
		JSeparator s1 = new JSeparator(SwingConstants.HORIZONTAL);
		this.add(s1);
		
		//a�ade los botones
		JPanel botones = new JPanel();
		botones.setLayout(new FlowLayout());
		aceptar = new JButton("Accept");
		aceptar.addActionListener(control);
		botones.add(aceptar);
		cancelar = new JButton("Cancel");
		cancelar.addActionListener(control);
		botones.add(cancelar);
		container.add(botones);
		//visualizaci�n
		setIdioma(idioma);
		if (idioma.equals("ingles")){
			this.setTitle("Add component");
		}else{
			this.setTitle("A�adir componente");
		}
		setSize(600, 500);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);
	}
	
	
	/**
	 * M�todo que traduce la interfaz.
	 * @param s el idioma a traducir: ingl�s o espa�ol.
	 */
	public void setIdioma(String s){
		idioma = s;
		if (s.equals("espa�ol")){
			nombreTab1 = "Componente";
			nombreTab2 = "Sensor";
			nombreTab3 = "Cantidad";
			panel.setTitleAt(0, nombreTab1);
			panel.setTitleAt(1, nombreTab2);
			panel.setTitleAt(2, nombreTab3);
		}else{
			nombreTab1 = "Component";
			nombreTab2 = "Sensor";
			nombreTab3 = "Quantity";
			panel.setTitleAt(0, nombreTab1);
			panel.setTitleAt(1, nombreTab2);
			panel.setTitleAt(2, nombreTab3);
		}
	}
	
	/**
	 * Comprueba si el formato de la ventana es v�lido.
	 * @return si el formato del contenido de la ventana es v�lido.
	 */
	public String esFormatoValido(){
		String salida = "";
		int indice = panel.getSelectedIndex();
		
		if (indice == 2){
			salida = salida + tabCantidad.esFormatoValido();
		}else{
			if (indice == 1){
				//comprueba el sensor
				salida = salida + tabSensor.esFormatoValido();
				
			}else{
				//comprueba el componente
				salida = salida + tabComponente.esFormatoValido();
			}
		}
		return salida;
	}
	
	/**
	 * Devuelve el contenido de la pesta�a activa actualmente en una cadena.
	 * @return el contenido de la pesta�a activa actualmente en una cadena.
	 */
	public String toString(){
		int indice = panel.getSelectedIndex();
		String s = panel.getTitleAt(indice) + ":\n";
		s = s + (panel.getComponentAt(indice)).toString();		
		return s;
	}
}
