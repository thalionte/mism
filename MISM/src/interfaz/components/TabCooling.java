package interfaz.components;

import java.awt.ComponentOrientation;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * Implementa la pesta�a de nuevo componente (sistema de enfriamiento).
 * @author M� Angeles Broull�n Lozano y Ana M� Fern�ndez Hern�ndez.
 * @version 2.0
 */
public class TabCooling extends JPanel{

	//ATRIBUTOS
	/**
	 * El serial de Component.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * El idioma actual de la ventana.
	 */
	private String idioma;
	/**
	 * El nombre del tab de m�quina.
	 */
	private String nombreTab;
	/**
	 * La etiqueta de nombre del componente.
	 */
	private JLabel etiquetaNombre;
	/**
	 * El nombre del componente.
	 */
	private JTextField nombre;
	/**
	 * Etiqueta del campo refrigerante.
	 */
	private JLabel etiquetaRefrigerante;
	/**
	 * Etiqueta del campo forma de aplicacion.
	 */
	private JLabel etiquetaFormaAplicacion;	
	/**
	 * Campo dureza.
	 */
	private JTextField refrigerante;
	/**
	 * Campo forma de aplicaci�n.
	 */
	private JTextField formaAplicacion;
	
	/**
	 * El controlador de la pesta�a.
	 */
	@SuppressWarnings("unused")
	private ControlAddComponent control;
	
	
	//METODOS
	/**
	 * Constructor con par�metros: genera una pieza con el contenido de la pesta�a de almacenar datos.
	 * @param c el constrolador asignado. 
	 */
	public TabCooling(ControlAddComponent c){
		
		control = c;
		
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(3,2));
		
		JPanel c1 = new JPanel();
		c1.setLayout(new FlowLayout());
		c1.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		etiquetaNombre = new JLabel("Nombre:");
		c1.add(etiquetaNombre);
		panel.add(c1);
		
		JPanel c2 = new JPanel();
		c2.setLayout(new FlowLayout());
		c2.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		nombre = new  JTextField("", 20);
		c2.add(nombre);
		panel.add(c2);
		
		JPanel c3 = new JPanel();
		c3.setLayout(new FlowLayout());
		c3.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		etiquetaRefrigerante = new JLabel("Refrigerante:");
		c3.add(etiquetaRefrigerante);
		panel.add(c3);
		
		JPanel c4 = new JPanel();
		c4.setLayout(new FlowLayout());
		c4.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		refrigerante = new  JTextField("", 20);
		c4.add(refrigerante);
		panel.add(c4);
		
		JPanel c5 = new JPanel();
		c5.setLayout(new FlowLayout());
		c5.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		etiquetaFormaAplicacion = new JLabel("Forma de apliacaci�n:");
		c5.add(etiquetaFormaAplicacion);
		panel.add(c5);
		
		JPanel c6 = new JPanel();
		c6.setLayout(new FlowLayout());
		c6.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		formaAplicacion = new  JTextField("", 20);
		c6.add(formaAplicacion);
		panel.add(c6);
		
		this.add(panel);
	
	}
	
	
	/**
	 * M�todo que traduce la interfaz.
	 * @param s el idioma a traducir: ingl�s o espa�ol.
	 */
	public void setIdioma(String s){
		idioma = s;
		if (s.equals("espa�ol")){
			nombreTab = "sistema de enfriamiento";
			etiquetaNombre.setText("Nombre:");
			etiquetaRefrigerante.setText("Refrigerante:");
			etiquetaFormaAplicacion.setText("Forma de aplicaci�n:");
		}else{
			nombreTab = "cooling system";
			etiquetaNombre.setText("Name:");
			etiquetaRefrigerante.setText("Refrigerant:");
			etiquetaFormaAplicacion.setText("Way of application:");
		}
	}
	
	/**
	 * Comprueba si el formato de la ventana es v�lido.
	 * @return si el formato del contenido de la ventana es v�lido.
	 */
	public String esFormatoValido(){
		String salida = rellenarCampos();
		return salida;
	}
	
	/**
	 * Comprueba si han quedado campos sin rellenar.
	 * @return si han quedado campos sin rellenar.
	 */
	private String rellenarCampos(){
		String salida = "";
		if (nombre.getText().equals("") || refrigerante.getText().equals("")
				|| formaAplicacion.getText().equals("") ){
			if (idioma.equals("espa�ol")){
				salida =  "Le faltan campos por rellenar.\n";
			}else{
				salida =  "You must complete all the text fields.\n";
			}
		}
		return salida;
	}
	
	/**
	 * Devuelve el estado actual del tab en una cadena.
	 * @return el estado actual del tab en una cadena.
	 */
	public String toString(){
		String s = nombreTab + "\n";;
		s = s + etiquetaNombre.getText() + " " + nombre.getText() + "\n";
		s = s +	etiquetaRefrigerante.getText() + " " + refrigerante.getText() + "\n";
		s = s + etiquetaFormaAplicacion.getText() + " " + formaAplicacion.getText() + "\n";
		return s;
	}
	
}
