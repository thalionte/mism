package interfaz.components;

import java.awt.ComponentOrientation;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.Auxiliar;

/**
 * Implementa la pesta�a de nuevo cantidad.
 * @author M� Angeles Broull�n Lozano y Ana M� Fern�ndez Hern�ndez.
 * @version 2.0
 */
public class TabAddQuantity extends JPanel{
	
	//ATRIBUTOS
	/**
	 * El serial de Component.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * El idioma actual de la ventana.
	 */
	private String idioma;
	/**
	 * El nombre del tab de m�quina.
	 */
	private String nombreTab;
	/**
	 * La etiqueta de nombre.
	 */
	private JLabel etiquetaNombre;
	/**
	 * La etiqueta de cantidad.
	 */
	private JLabel etiquetaCantidad;
	/**
	 * La etiqueta de unidades.
	 */
	private JLabel etiquetaUnidad;
	/**
	 * El campo de nombre.
	 */
	private JTextField nombre;
	/**
	 * El campo de cantidad.
	 */
	private JTextField cantidad;
	/**
	 * El campo de unidades.
	 */
	private JTextField unidad;
	
	/**
	 * El controlador de la pesta�a.
	 */
	@SuppressWarnings("unused")
	private ControlAddComponent control;
	
	//METODOS
	/**
	 * Constructor con par�metros: genera una cantidad con el contenido de la pesta�a de almacenar datos.
	 * @param s el idioma seleccionado.
	 * @param c el controlador asignado. 
	 */
	public TabAddQuantity(String s, ControlAddComponent c){
		control = c;
		
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(3,2));
		
		JPanel c1 = new JPanel();
		c1.setLayout(new FlowLayout());
		c1.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		etiquetaNombre = new JLabel("Nombre:");
		c1.add(etiquetaNombre);
		panel.add(c1);
		
		JPanel c2 = new JPanel();
		c2.setLayout(new FlowLayout());
		c2.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		nombre = new  JTextField("", 20);
		c2.add(nombre);
		panel.add(c2);
		
		JPanel c3 = new JPanel();
		c3.setLayout(new FlowLayout());
		c3.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		etiquetaCantidad = new JLabel("Cantidad:");
		c3.add(etiquetaCantidad);
		panel.add(c3);
		
		JPanel c4 = new JPanel();
		c4.setLayout(new FlowLayout());
		c4.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		cantidad = new  JTextField(20);
		c4.add("", cantidad);
		panel.add(c4);
		
		JPanel c5 = new JPanel();
		c5.setLayout(new FlowLayout());
		c5.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		etiquetaUnidad = new JLabel("Unidades:");
		c5.add(etiquetaUnidad);
		panel.add(c5);
		
		JPanel c6 = new JPanel();
		c6.setLayout(new FlowLayout());
		c6.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		unidad = new  JTextField("", 20);
		c6.add(unidad);
		panel.add(c6);
		
		this.add(panel);
		setIdioma(s);
	
	}
	
	/**
	 * M�todo observador que devuelve el valor del campo cantidad.
	 * @return el valor del campo cantidad.
	 */
	public String getCantidad(){
		return cantidad.getText();
	}
	
	
	/**
	 * M�todo que traduce la interfaz.
	 * @param s el idioma a traducir: ingl�s o espa�ol.
	 */
	public void setIdioma(String s){
		idioma = s;
		if (s.equals("espa�ol")){
			nombreTab = "cantidad";
			etiquetaNombre.setText("Nombre:");
			etiquetaCantidad.setText("Cantidad:");
			etiquetaUnidad.setText("Unidades:");
		}else{
			nombreTab = "quantity";
			etiquetaNombre.setText("Name:");
			etiquetaCantidad.setText("Quantity:");
			etiquetaUnidad.setText("Units:");
		}
	}
	
	/**
	 * Comprueba si el formato de la ventana es v�lido.
	 * @return si el formato del contenido de la ventana es v�lido.
	 */
	public String esFormatoValido(){
		String salida = "";
		String cant = cantidad.getText();
		if (!Auxiliar.esNumerica(cant)){
			if (idioma.equals("espa�ol")){
				salida = salida + "El valor de cantidad debe ser numerico.";
			}else{
				salida = salida + "Quantity must be a number.";
			}
		}
		salida = salida + rellenarCampos();
		return salida;
	}
	
	/**
	 * Comprueba si han quedado campos sin rellenar.
	 * @return si han quedado campos sin rellenar.
	 */
	private String rellenarCampos(){
		String salida = "";
		if (nombre.getText().equals("") || cantidad.getText().equals("")
				|| unidad.getText().equals("") ){
			if (idioma.equals("espa�ol")){
				salida =  "Le faltan campos por rellenar.\n";
			}else{
				salida =  "You must complete all the text fields.\n";
			}
		}
		return salida;
	}
	
	/**
	 * Devuelve el estado actual del tab en una cadena.
	 * @return el estado actual del tab en una cadena.
	 */
	public String toString(){
		String s = nombreTab + "\n";
		s = s + etiquetaNombre.getText() + " " + nombre.getText() + "\n";
		s = s +	etiquetaCantidad.getText() + " " + cantidad.getText() + "\n";
		s = s + etiquetaUnidad.getText() + " " + unidad.getText() + "\n";
		return s;
	}

}
