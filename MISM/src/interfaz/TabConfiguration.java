package interfaz;


import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import model.Auxiliar;

/**
 * Implementa la pesta�a de dise�o de experimentos.
 * @author M� Angeles Broull�n Lozano y Ana M� Fern�ndez Hern�ndez.
 * @version 2.0
 */
public class TabConfiguration extends JPanel{
	
	//ATRIBUTOS
	/**
	 * El serial de Component.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * El Controlador de la pesta�a.
	 */
	private Control control;
	/**
	 * La barra con los botones de dibujo.
	 */
	private JPanel barraDibujo;
	/**
	 * La etiqueta que delimita el panel de componentes.
	 */
	private JLabel etiquetaComponentes;
	/**
	 * �rea de los componentes.
	 */
	protected JList areaComponentes;
	/**
	 * Lista de componentes.
	 */
	protected DefaultListModel listaComponentes;
	/**
	 * Bot�n que activa el cuadro de di�logo para a�adir un nuevo componente.
	 */
	protected JButton botonA�adirComponente;
	/**
	 * Bot�n que activa el cuadro de di�logo para borrar un componente previamente existente.
	 */
	protected JButton botonBorrarComponente;
	/**
	 * Bot�n que activa el cuadro de di�logo para mostrar un componente previamente existente.
	 */
	protected JButton botonConsultarComponente;
	/**
	 * La etiqueta que delimita el panel de reglas.
	 */
	private JLabel etiquetaReglas;
	/**
	 * �rea las reglas de uso de los componentes.
	 */
	protected JList areaReglas;
	/**
	 * Lista las reglas de uso de los componentes.
	 */
	protected DefaultListModel listaReglas;
	/**
	 * Bot�n que activa el cuadro de di�logo para a�adir una nueva regla.
	 */
	protected JButton botonA�adirRegla;
	/**
	 * Bot�n que activa el cuadro de di�logo para consultar una regla previamente existente.
	 */
	protected JButton botonConsultarRegla;
	/**
	 * Bot�n que activa el cuadro de di�logo para borrar una regla previamente existente.
	 */
	protected JButton botonBorrarRegla;
	/**
	 * La etiqueta que delimita el panel de descripcion.
	 */
	private JLabel etiquetaDescripcion;
	/**
	 * Contendr� la descripci�n del experimento realizado.
	 */
	protected JTextArea areaDescripcion;
	/**
	 * El bot�n para crear una instancia de m�quina.
	 */
	protected JButton botonMaquina;
	/**
	 * El bot�n para crear una instancia de herramienta.
	 */
	protected JButton botonHerramienta;
	/**
	 * El bot�n para crear una instancia de pieza.
	 */
	protected JButton botonPieza;
	/**
	 * El bot�n para crear una instancia de sistema de enfriamiento.
	 */
	protected JButton botonEnfriamiento;
	/**
	 * El bot�n para crear una instancia de sensor.
	 */
	protected JButton botonSensor;
	/**
	 * El bot�n para crear una instancia de cantidad.
	 */
	protected JButton botonCantidad;
	/**
	 * El bot�n para crear una instancia de regla es_parte_de.
	 */
	protected JButton botonEsParteDe;
	/**
	 * El bot�n para crear una instancia de regla mide.
	 */
	protected JButton botonMide;
	/**
	 * El bot�n para borrar el contenido del canvas.
	 */
	protected JButton botonBorrar;
	
	//METODOS
	/**
	 * Constructor con par�metros: genera un componente con el contenido de la pesta�a de dise�ar experimento.
	 * @param c el controlador.
	 * @param i el idioma correspondiente.
	 * @return un componente con el contenido de la pesta�a de dise�ar experimento.
	 */
	public TabConfiguration(Control c, String i){
		
		control = c;

		this.setLayout(new BorderLayout());
		
		//panel con los controles para generar al diagrama
		JPanel controles = setControles();
		this.add(controles, BorderLayout.LINE_END);
		
		//selecciona el idioma
		this.setIdioma(i);
		
	}
	
	/**
	 * Genera el panel con los controles.
	 * @return el panel con los controles.
	 */
	private JPanel setControles(){
		JPanel controles = new JPanel();
		controles.setLayout(new BoxLayout(controles, BoxLayout.X_AXIS));
				
		//separador
		JSeparator s1 = new JSeparator(SwingConstants.VERTICAL);
		this.add(s1);
		
		//el panel con los botones de dibujo
		JPanel iconosDibujo = new JPanel();
		crearBarraDibujo();
		iconosDibujo.add(barraDibujo);
		controles.add(iconosDibujo);
		
		//separador
		JSeparator s2 = new JSeparator(SwingConstants.VERTICAL);
		controles.add(s2);
	
		JPanel info = setInformacion();
		controles.add(info);
		
		return controles;
	}
	
	/**
	 * Genera el panel con los controles.
	 * @return el panel con los controles.
	 */
	private JPanel setInformacion(){
		//paneles de manipulacion de informaci�n
		JPanel info = new JPanel();
		info.setLayout(new BoxLayout(info, BoxLayout.Y_AXIS));
				
		JPanel panelComponentes = new JPanel();
		panelComponentes.setLayout(new BoxLayout(panelComponentes, BoxLayout.Y_AXIS));
		etiquetaComponentes = new JLabel("Components, sensors and quantities:");
		etiquetaComponentes.setAlignmentY(RIGHT_ALIGNMENT);
		panelComponentes.add(etiquetaComponentes, Component.CENTER_ALIGNMENT);
		areaComponentes = new JList();
		areaComponentes.setLayoutOrientation(JList.VERTICAL);
		listaComponentes = new DefaultListModel();

		//TODO
		/*
		 * se debe a a�adir a listaComponentes TODOS los nombres de los componentes, sensores 
		 * y cantidades de la base de conocimientos
		 * lo que se ve entre los dos comentarios es texto de prueba
		 */
		listaComponentes.addElement("Component 1");
		listaComponentes.addElement("Sensor 1");
		listaComponentes.addElement("Component 2");
		listaComponentes.addElement("Sensor 2");
		listaComponentes.addElement("Component 3");
		listaComponentes.addElement("Sensor 3");
		listaComponentes.addElement("Component 4");
		listaComponentes.addElement("Sensor 5");
		listaComponentes.addElement("Sensor 6");
		listaComponentes.addElement("Sensor 7");
		listaComponentes.addElement("Sensor 8");
		listaComponentes.addElement("Sensor 9");
		listaComponentes.addElement("Sensor 10");
		
		/*
		 * aqui termina lo que tienes que modificar
		 */
		
		areaComponentes = new JList(listaComponentes);
		areaComponentes.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		areaComponentes.setVisibleRowCount(10);
		
		JScrollPane listScroller1 = new JScrollPane(areaComponentes);
		
		panelComponentes.add(listScroller1, Component.CENTER_ALIGNMENT);
		
		JPanel controlComponentes = new JPanel();
		controlComponentes.setLayout(new FlowLayout());
		ImageIcon icono1 = Auxiliar.crearIcono("add.gif", "Add");
		botonA�adirComponente = new JButton("Add", icono1);
		botonA�adirComponente.addActionListener(control);
		controlComponentes.add(botonA�adirComponente);
		ImageIcon icono2 = Auxiliar.crearIcono("design.gif", "Query");
		botonConsultarComponente = new JButton("Query", icono2);
		botonConsultarComponente.addActionListener(control);
		controlComponentes.add(botonConsultarComponente);
		ImageIcon icono3 = Auxiliar.crearIcono("remove.gif", "Delete");
		botonBorrarComponente = new JButton("Delete", icono3);
		botonBorrarComponente.addActionListener(control);
		controlComponentes.add(botonBorrarComponente);
		panelComponentes.add(controlComponentes);
		info.add(panelComponentes);
		
		//separador
		JSeparator s3 = new JSeparator(SwingConstants.HORIZONTAL);
		info.add(s3);
		
		JPanel panelReglas = new JPanel();
		panelReglas.setLayout(new BoxLayout(panelReglas, BoxLayout.Y_AXIS));
		etiquetaReglas = new JLabel("Rules:");		
		etiquetaReglas.setAlignmentY(RIGHT_ALIGNMENT);
		panelReglas.add(etiquetaReglas, Component.CENTER_ALIGNMENT);
		
		areaReglas = new JList();
		areaReglas.setLayoutOrientation(JList.VERTICAL);
		listaReglas = new DefaultListModel();

		//TODO
		/*
		 * se debe a a�adir a listaReglas TODOS los nombres de las reglas 
		 * de la base de conocimientos
		 * lo que se ve entre los dos comentarios es texto de prueba
		 */
		listaReglas.addElement("Rule 1");
		listaReglas.addElement("Rule 2");
		listaReglas.addElement("Rule 3");
		listaReglas.addElement("Rule 4");
		listaReglas.addElement("Rule 5");
		listaReglas.addElement("Rule 6");
		listaReglas.addElement("Rule 7");
		listaReglas.addElement("Rule 8");
		listaReglas.addElement("Rule 9");
		listaReglas.addElement("Rule 10");
		/*
		 * aqui termina lo que tienes que modificar
		 */
		
		areaReglas = new JList(listaReglas);
		areaReglas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		areaReglas.setVisibleRowCount(7);		
		
		JScrollPane listScroller2 = new JScrollPane(areaReglas);
		panelReglas.add(listScroller2, Component.CENTER_ALIGNMENT);
		
		JPanel controlReglas = new JPanel();
		controlReglas.setLayout(new FlowLayout());
		ImageIcon icono4 = Auxiliar.crearIcono("add.gif", "Add");
		botonA�adirRegla = new JButton("Add", icono4);
		botonA�adirRegla.addActionListener(control);
		controlReglas.add(botonA�adirRegla);
		ImageIcon icono5 = Auxiliar.crearIcono("design.gif", "Query");
		botonConsultarRegla = new JButton("Query", icono5);
		botonConsultarRegla.addActionListener(control);
		controlReglas.add(botonConsultarRegla);
		ImageIcon icono6 = Auxiliar.crearIcono("remove.gif", "Delete");
		botonBorrarRegla = new JButton("Delete", icono6);
		controlReglas.add(botonBorrarRegla);
		botonBorrarRegla.addActionListener(control);
		panelReglas.add(controlReglas);
		info.add(panelReglas);
		
		//separador
		JSeparator s4 = new JSeparator(SwingConstants.HORIZONTAL);
		info.add(s4);
		
		JPanel panelDescripcion = new JPanel();
		panelDescripcion.setLayout(new BoxLayout(panelDescripcion, BoxLayout.Y_AXIS));
		etiquetaDescripcion = new JLabel("Description:");
		etiquetaDescripcion.setAlignmentY(RIGHT_ALIGNMENT);
		panelDescripcion.add(etiquetaDescripcion, Component.CENTER_ALIGNMENT);
		
		//TODO
		/*
		 * El contenido del JTextArea siguiente debe describir el experimento.
		 * Queda pendiente de verlo juntas, ya que puede ser necesario recuperar informaci�n de
		 * base de datos.
		 * En la version final hay que limpiar la cadena.
		 */
		areaDescripcion =  new JTextArea("The description of our experiment");
		/* 
		 * Fin de lo que debes modificar
		 */
		
		areaDescripcion.setRows(8);
		areaDescripcion.setEditable(false);
		JScrollPane listScroller3 = new JScrollPane(areaDescripcion);
		panelDescripcion.add(listScroller3, Component.CENTER_ALIGNMENT);
		info.add(panelDescripcion);
		
		return info;
	}
	
	/**
	 * M�todo que traduce la interfaz.
	 * @param s el idioma a traducir: ingl�s o espa�ol.
	 */
	public void setIdioma(String s){
		if (s.equals("espa�ol")){
			etiquetaComponentes.setText("Componentes, sensores y cantidades:");
			botonA�adirComponente.setText("A�adir");
			botonConsultarComponente.setText("Consultar");
			botonBorrarComponente.setText("Borrar");
			etiquetaReglas.setText("Reglas:");
			botonA�adirRegla.setText("A�adir");
			botonConsultarRegla.setText("Consultar");
			botonBorrarRegla.setText("Borrar");
			etiquetaDescripcion.setText("Descripci�n:");
			botonBorrar.setToolTipText("borra el diagrama");
			botonMide.setToolTipText("inserta regla 'mide'");
			botonEsParteDe.setToolTipText("inserta regla 'es parte de'");
			botonCantidad.setToolTipText("inserta una instancia de un componente cantidad");
			botonSensor.setToolTipText("inserta una instancia de un componente sensor");
			botonEnfriamiento.setToolTipText("inserta una instancia de un componente sistema de refrigeracion");
			botonPieza.setToolTipText("inserta una instancia de un componente pieza");
			botonHerramienta.setToolTipText("inserta una instancia de un componente herramienta");
			botonMaquina.setToolTipText("inserta una instancia de un componente maquina");
		}else{
			etiquetaComponentes.setText("Components, sensors and quantities:");
			botonA�adirComponente.setText("Add");
			botonConsultarComponente.setText("Query");
			botonBorrarComponente.setText("Delete");
			etiquetaReglas.setText("Rules:");
			botonA�adirRegla.setText("Add");
			botonConsultarRegla.setText("Query");
			botonBorrarRegla.setText("Delete");
			etiquetaDescripcion.setText("Description:");
			botonBorrar.setToolTipText("deletes the diagrama");
			botonMide.setToolTipText("inserts a 'meassures' rule ");
			botonEsParteDe.setToolTipText("inserts a 'is parte of' rule");
			botonCantidad.setToolTipText("inserts an instance of quatity");
			botonSensor.setToolTipText("inserts an instance of sensor");
			botonEnfriamiento.setToolTipText("inserts an instance of cooling system");
			botonPieza.setToolTipText("inserts an instance of part");
			botonHerramienta.setToolTipText("inserts an instance of tool");
			botonMaquina.setToolTipText("inserts an instance of machine");
		}
	}
	
	/**
	 * M�todo que genera la barra de dibujo.
	 */
	private void crearBarraDibujo(){
		barraDibujo = new JPanel();
		barraDibujo.setLayout(new BoxLayout(barraDibujo, BoxLayout.Y_AXIS));
		
		//botones de elementos
		ImageIcon icono1 = Auxiliar.crearIcono("maquina.png", "maquina");
		botonMaquina = new JButton("", icono1);
		botonMaquina.addActionListener(control);
		barraDibujo.add(botonMaquina);
		ImageIcon icono2 = Auxiliar.crearIcono("herramienta.png", "herramienta");
		botonHerramienta = new JButton("", icono2);
		barraDibujo.add(botonHerramienta);
		botonHerramienta.addActionListener(control);
		ImageIcon icono3 = Auxiliar.crearIcono("pieza.png", "dibujoCubo");
		botonPieza = new JButton("", icono3);
		botonPieza.addActionListener(control);
		barraDibujo.add(botonPieza);
		ImageIcon icono4 = Auxiliar.crearIcono("enfriamiento.png", "enfriamiento");
		botonEnfriamiento= new JButton("", icono4);
		botonEnfriamiento.addActionListener(control);
		barraDibujo.add(botonEnfriamiento);
		ImageIcon icono5 = Auxiliar.crearIcono("sensor.png", "dibujoCubo");
		botonSensor = new JButton("", icono5);
		botonSensor.addActionListener(control);
		barraDibujo.add(botonSensor);
		ImageIcon icono6 = Auxiliar.crearIcono("cantidad.png", "dibujoCubo");
		botonCantidad = new JButton("", icono6);
		botonCantidad.addActionListener(control);
		barraDibujo.add(botonCantidad);
		
		//los botones de reglas
		ImageIcon icono7 = Auxiliar.crearIcono("esParte.png", "dibujoCubo");
		botonEsParteDe = new JButton("", icono7);
		botonEsParteDe.addActionListener(control);
		barraDibujo.add(botonEsParteDe);
		ImageIcon icono8 = Auxiliar.crearIcono("mide.png", "dibujoCubo");
		botonMide = new JButton("", icono8);
		botonMide.addActionListener(control);
		barraDibujo.add(botonMide);
		
		//el boton de borrado
		ImageIcon icono9 = Auxiliar.crearIcono("goma.png", "dibujoCubo");
		botonBorrar = new JButton("", icono9);
		botonBorrar.addActionListener(control);
		barraDibujo.add(botonBorrar);
	}
	
	/**
	 * M�todo observador que devuelve la descripci�n del experimento.
	 * @return la descripci�n del experimento.
	 */
	public String getDescripcion(){
		return areaDescripcion.getText();
	}
	
	/**
	 * M�todo modificador que almacena la descripci�n del experimento.
	 * @param la nueva descripci�n del experimento.
	 */
	public void setDescripcion(String s){
		areaDescripcion.setText(s);
	}

}
