package model.render;

import java.awt.BorderLayout;
import java.awt.GraphicsConfigTemplate;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.FileOutputStream;
import java.util.Enumeration;

import javax.media.j3d.Alpha;
import javax.media.j3d.AmbientLight;
import javax.media.j3d.Appearance;
import javax.media.j3d.Background;
import javax.media.j3d.BoundingSphere;
import javax.media.j3d.Bounds;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Canvas3D;
import javax.media.j3d.DirectionalLight;
import javax.media.j3d.GraphicsConfigTemplate3D;
import javax.media.j3d.Group;
import javax.media.j3d.ImageComponent;
import javax.media.j3d.ImageComponent2D;
import javax.media.j3d.Locale;
import javax.media.j3d.Material;
import javax.media.j3d.PhysicalBody;
import javax.media.j3d.PhysicalEnvironment;
import javax.media.j3d.RotationInterpolator;
import javax.media.j3d.SceneGraphObject;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.media.j3d.View;
import javax.media.j3d.ViewPlatform;
import javax.media.j3d.VirtualUniverse;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.ToolTipManager;
import javax.swing.WindowConstants;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;
import com.sun.j3d.utils.geometry.Box;

/**
 * Clase que dibuja una serie de figuras tridimensionales.
 * @author M� Angeles Broull�n Lozano.
 * @version 1.0
 */
public class PanelRender extends JPanel{
	
	
	//ATRIBUTOS
	/**
	 * El serial del panel.
	 */
	
	private static final long serialVersionUID = 1L;
	/**
	 * El controlador de la ventana.
	 */
	private ControlRender control;
	
	/**
	 * El centro de contenci�n de la esfera.
	 */
	protected BranchGroup sceneBranchGroup = null;

	/**
	 * Interpolaci�n de rotaci�n autom�tica.
	 */
	protected RotationInterpolator rotator = null;

	/**
	 * Canvas3D para capturar pantallas.
	 */
	private Canvas3D offScreenCanvas3D = null;

	/**
	 * LA imagen resultante de las capturas de pantalla.
	 */
	private ImageComponent2D imageComponent = null;

	/**
	 * El ancho del Canvas3D
	 */
	private static final int offScreenWidth = 400;

	/**
	 * El alto del Canvas3D
	 */
	private static final int offScreenHeight = 400;
	
	
	//METODOS
	
	//constructor
	
	/**
	 * Constructor sin par�metros.
	 */
	public PanelRender() {
		setLayout(new BorderLayout());
		iniciar();
	}
	
	//inicializaci�n
	/**
	 * Inicializa el Canvas3D.
	 */
	protected void iniciar() {
		VirtualUniverse universo = crearVirtualUniverse();
		Locale local = crearLocale(universo);
		BranchGroup sceneBranchGroup = crearSceneBranchGroup();
		Background fondo = crearFondo();

		if (fondo != null){
			sceneBranchGroup.addChild(fondo);
		}

		ViewPlatform vp = crearViewPlatform();
		BranchGroup viewBranchGroup = crearViewBranchGroup(
				getViewTransformGroupArray(), vp);
		local.addBranchGraph(sceneBranchGroup);
		a�adirViewBranchGroup(local, viewBranchGroup);

		crearVista(vp);
	}
	
	/**
	 * A�ade un Canvas3D al Panel.
	 * @param c3d el Canvas3D a a�adir.
	 */
	protected void a�adirCanvas3D(Canvas3D c3d) {
		add(c3d, BorderLayout.CENTER);
	}
	
	/**
	 * A�ade la vista lateral al escenario de Locale.
	 * @param local el local.
	 * @param bg el BrachGroup de la vista.
	 */
	protected void a�adirViewBranchGroup(Locale local, BranchGroup bg) {
		local.addBranchGraph(bg);
	}

	/**
	 * Crea un Locale para el VirtualUniverse.
	 * @param u el VirtualUniverse.
	 */
	protected Locale crearLocale(VirtualUniverse u) {
		return new Locale(u);
	}
	
	/**
	 * Crear el fondo del Canvas3D.
	 * @return el fondo del Canvas3D.
	 */
	protected Background crearFondo() {
		Background back = new Background(new Color3f(0.9f, 0.9f, 0.9f));
		back.setApplicationBounds(crearApplicationBounds());
		return back;
	}
	
	/**
	 * Crea un objeto Bounds para el 3D.
	 */
	protected Bounds crearApplicationBounds() {
		return new BoundingSphere(new Point3d(0.0, 0.0, 0.0), 100.0);
	}
	
	/**
	 * Crea un Canvas3D.
	 * @param offscreen true para especificar un offscreen canvas.
	 * @return un Canvas3D
	 */
	protected Canvas3D crearCanvas3D(boolean offscreen) {
		GraphicsConfigTemplate3D gc3D = new GraphicsConfigTemplate3D();
		gc3D.setSceneAntialiasing(GraphicsConfigTemplate.PREFERRED);
		GraphicsDevice gd[] = GraphicsEnvironment.getLocalGraphicsEnvironment()
				.getScreenDevices();
		Canvas3D c3d = new Canvas3D(gd[0].getBestConfiguration(gc3D), offscreen);
		c3d.setSize(500, 500);

		return c3d;
	}
	
	/**
	 * Inicializa un offscreen Canvas3D.
	 * @return un offscreen Canvas3D.
	 */
	protected Canvas3D crearOffscreenCanvas3D() {
		offScreenCanvas3D = crearCanvas3D(true);
		offScreenCanvas3D.getScreen3D()
				.setSize(offScreenWidth, offScreenHeight);
		offScreenCanvas3D.getScreen3D().setPhysicalScreenHeight(
				0.0254 / 90 * offScreenHeight);
		offScreenCanvas3D.getScreen3D().setPhysicalScreenWidth(
				0.0254 / 90 * offScreenWidth);

		RenderedImage renderedImage = new BufferedImage(offScreenWidth,
				offScreenHeight, BufferedImage.TYPE_3BYTE_BGR);
		imageComponent = new ImageComponent2D(ImageComponent.FORMAT_RGB8,
				renderedImage);
		imageComponent.setCapability(ImageComponent2D.ALLOW_IMAGE_READ);
		offScreenCanvas3D.setOffScreenBuffer(imageComponent);

		return offScreenCanvas3D;
	}
	
	/**
	 * Crea la escena lateral de la escenograf�a.
	 * @return la escena lateral de la escenograf�a.
	 */
	protected BranchGroup crearSceneBranchGroup() {
		// crea la raiz de la escena lateral
		BranchGroup objRoot = new BranchGroup();

		// crea un TransformGroup para hacer rotar los objetos
		// establece los bits de capacidad del TransformGroup
		// de forma que pueda modificarse en tiempo de ejecuci�n
		TransformGroup objTrans = new TransformGroup();
		objTrans.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
		objTrans.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);

		// crea un volumen esf�rico
		BoundingSphere bounds = new BoundingSphere(new Point3d(0.0, 0.0, 0.0),
				100.0);

		// crea una matriz de transformaci�n de 4x4.
		Transform3D yAxis = new Transform3D();

		// crea un interpoladir alfa que genere autom�ticamente
		// las modificaciones para el componente de rotacion de la matriz de transformaci�n.
		Alpha rotationAlpha = new Alpha(-1, Alpha.INCREASING_ENABLE, 0, 0,
				4000, 0, 0, 0, 0, 0);

		// crea un  comportamiento de RotationInterpolator que afecte al TransformGroup
		rotator = new RotationInterpolator(rotationAlpha, objTrans, yAxis,
				0.0f, (float) Math.PI * 2.0f);

		// establece los limites de actuaci�n
		rotator.setSchedulingBounds(bounds);

		// se a�ade el comportamiento.
		objTrans.addChild(rotator);

		// creamos el BranchGroup que contendr� los objetos
		// que a�adiremos o eleminaremos del escanario
		sceneBranchGroup = new BranchGroup();

		// permite al BranchGroup a�adirle hijos en tiempo de ejecuci�n.
		sceneBranchGroup.setCapability(Group.ALLOW_CHILDREN_EXTEND);
		sceneBranchGroup.setCapability(Group.ALLOW_CHILDREN_READ);
		sceneBranchGroup.setCapability(Group.ALLOW_CHILDREN_WRITE);

		// a�ade las figuras al escenario.
		sceneBranchGroup.addChild(crearCubo());
		sceneBranchGroup.addChild(crearEsfera());

		// crea los colores de las luces.
		Color3f lColor1 = new Color3f(0.7f, 0.7f, 0.7f);
		Vector3f lDir1 = new Vector3f(-1.0f, -1.0f, -1.0f);
		Color3f alColor = new Color3f(0.2f, 0.2f, 0.2f);

		// crea el ambiente luminoso
		AmbientLight aLgt = new AmbientLight(alColor);
		aLgt.setInfluencingBounds(bounds);

		// crea lusz direccional
		DirectionalLight lgt1 = new DirectionalLight(lColor1, lDir1);
		lgt1.setInfluencingBounds(bounds);

		// a�ade las luces al escenario.
		objRoot.addChild(aLgt);
		objRoot.addChild(lgt1);

		// se une el escenario.
		objTrans.addChild(sceneBranchGroup);
		objRoot.addChild(objTrans);

		// se devuelve el objeto raiz
		return objRoot;
	}
	
	/**
	 * Crea el cuerpo f�sico de la vista.
	 * @return el cuerpo f�sico de la vista.
	 */
	protected PhysicalBody crearCuerpoFisico() {
		return new PhysicalBody();
	}

	/**
	 * Crea el PhysicalEnvironment para la vista.
	 * @return el PhysicalEnvironment para la vista.
	 */
	protected PhysicalEnvironment crearEntornoFisico() {
		return new PhysicalEnvironment();
	}
	
	/**
	 * Crea un View Platform pa la vista.
	 * @return un View Platform pa la vista.
	 */
	protected ViewPlatform crearViewPlatform() {
		ViewPlatform vp = new ViewPlatform();
		vp.setViewAttachPolicy(View.RELATIVE_TO_FIELD_OF_VIEW);
		vp.setActivationRadius(getViewPlatformActivationRadius());

		return vp;
	}
	
	/**
	 * Crea BranchGroup de vista lateral. El ViewPlatform se una al TransformGroups.
	 * @param tsArray el Array de transformaciones.
	 * @param vp la plataforma de vistas.
	 * @return el BranchGroup de vista lateral.
	 */
	protected BranchGroup crearViewBranchGroup(TransformGroup[] tgArray,
			ViewPlatform vp) {
		BranchGroup vpBranchGroup = new BranchGroup();

		if (tgArray != null && tgArray.length > 0) {
			Group parentGroup = vpBranchGroup;
			TransformGroup curTg = null;

			for (int n = 0; n < tgArray.length; n++) {
				curTg = tgArray[n];
				parentGroup.addChild(curTg);
				parentGroup = curTg;
			}

			tgArray[tgArray.length - 1].addChild(vp);
		} else
			vpBranchGroup.addChild(vp);

		return vpBranchGroup;
	}

	/**
	 * Crea el VirtualUniverse para la aplicacion.
	 * @return el VirtualUniverse para la aplicacion.
	 */
	protected VirtualUniverse crearVirtualUniverse() {
		return new VirtualUniverse();
	}
	
	/**
	 * Crea una vista Java3D ya la una a ViewPlatform
	 */
	protected View crearVista(ViewPlatform vp) {
		View view = new View();

		PhysicalBody pb = crearCuerpoFisico();
		PhysicalEnvironment pe = crearEntornoFisico();

		view.setPhysicalEnvironment(pe);
		view.setPhysicalBody(pb);

		if (vp != null)
			view.attachViewPlatform(vp);

		view.setBackClipDistance(getBackClipDistance());
		view.setFrontClipDistance(getFrontClipDistance());

		// create the visible canvas
		Canvas3D c3d = crearCanvas3D(false);
		view.addCanvas3D(c3d);

		// create the off screen canvas
		view.addCanvas3D(crearOffscreenCanvas3D());

		// add the visible canvas to a component
		a�adirCanvas3D(c3d);

		return view;
	}
	
	//generar las figuras y borrarlas
	/**
	 * Crea un BranchGroup que contiene un Cubo.
	 */
	protected BranchGroup crearCubo() {
		BranchGroup bg = new BranchGroup();
		bg.setCapability(BranchGroup.ALLOW_DETACH);
		bg.addChild(new com.sun.j3d.utils.geometry.ColorCube());
		bg.setUserData("Cubo");
		return bg;
	}
	
	/**
	 * Crea un BranchGroup que contiene un cilindro.
	 */
	protected BranchGroup crearCilindro() {
		BranchGroup bg = new BranchGroup();
		bg.setCapability(BranchGroup.ALLOW_DETACH);
		bg.addChild(new com.sun.j3d.utils.geometry.Cylinder());
		bg.setUserData("Cilindro");
		return bg;
	}
	
	/**
	 * Crea un BranchGroup que contiene un prisma.
	 */
	protected BranchGroup crearPrisma() {
		BranchGroup bg = new BranchGroup();
		bg.setCapability(BranchGroup.ALLOW_DETACH);
		
		Appearance app = new Appearance();		

		Box b = new Box(1, 1.5f, 1, app);
		bg.addChild(b);
		bg.setUserData("Prisma");
		return bg;
	}

	/**
	 * Crea un BranchGroup que contiene una esfera.
	 */
	protected BranchGroup crearEsfera() {
		BranchGroup bg = new BranchGroup();
		bg.setCapability(BranchGroup.ALLOW_DETACH);

		Appearance ap = new Appearance();
		Color3f objColor = new Color3f(1.0f, 0.7f, 0.8f);
		Color3f black = new Color3f(0.0f, 0.0f, 0.0f);
		ap.setMaterial(new Material(objColor, black, objColor, black, 80.0f));

		bg.addChild(new com.sun.j3d.utils.geometry.Sphere(1, ap));
		bg.setUserData("Esfera");
		return bg;
	}
	
	/**
	 * Borra un BranchGroup del escenario.
	 * @param nombre el elemento que buscamos elimianr.
	 */
	@SuppressWarnings("unchecked")
	protected void borrarFigura(String nombre) {
		try {
			Enumeration e = sceneBranchGroup.getAllChildren();
			int index = 0;

			while (e.hasMoreElements() != false) {
				SceneGraphObject sgObject = (SceneGraphObject) e.nextElement();
				Object userData = sgObject.getUserData();

				if (userData instanceof String
						&& ((String) userData).compareTo(nombre) == 0) {
					System.out.println("Borrando: " + sgObject.getUserData());
					sceneBranchGroup.removeChild(index);
				}

				index++;
			}
		} catch (Exception e) {
			// si no se sincroniz�...
		}
	}
	
	//getters
	/**
	 * Observador que obtiene el factor de escala para la visi�n lateral del escenario.
	 * @param el factor de escala.
	 */
	protected double getScale() {
		return 3;
	}
	
	/**
	 * Devuelve el radio de activacion de la plataforma.
	 * @return el radio de activacion de la plataforma.
	 */
	protected float getViewPlatformActivationRadius() {
		return 100;
	}
	
	/**
	 * Obtiene el array de TransformGroup para la visi�n lateral del escenario.
	 * @param el array de TransformGroup para la visi�n lateral del escenario.
	 */
	public TransformGroup[] getViewTransformGroupArray() {
		TransformGroup[] tgArray = new TransformGroup[1];
		tgArray[0] = new TransformGroup();

		// msepara un poco la c�mara
		// se debe mover la c�mara como si fuese el obseervador.
		Transform3D t3d = new Transform3D();
		t3d.setScale(getScale());
		t3d.setTranslation(new Vector3d(0.0, 0.0, -20.0));
		t3d.invert();
		tgArray[0].setTransform(t3d);

		return tgArray;
	}
	
	/**
	 * Devuelve la distancia al plano de archivo.
	 * @return la distancia al plano de archivo.
	 */
	protected double getBackClipDistance() {
		return 100.0;
	}

	/**
	 * Devuelve la distancia al plano de archivo cercano.
	 * @return la distancia al plano de archivo cercano.
	 */
	protected double getFrontClipDistance() {
		return 1.0;
	}
	
	
	//a�adir el controlador y los menus
	/**
	 * M�todo que a�ade el controlador.
	 * @param c el controlador que manejar� los eventos de la ventana.
	 */
	public void a�adirControlador(ControlRender c){
		control = c;
	}
	
	/**
	 * M�todo que crea la barra de men�.
	 * @return una barra de men�.
	 */
	public JMenuBar crearBarraMenu() {
		JMenuBar menuBar = new JMenuBar();
		JMenu menu = null;

		menu = new JMenu("Archivo");
		menu.add(crearMenuItem("Archivo", "Guardar la imagen", control));
		menu.add(crearMenuItem("Archivo", "Salir", control));
		menuBar.add(menu);

		menu = new JMenu("Figura");
		menu.add(crearMenuItem("Figura", "Cubo", control));
		menu.add(crearMenuItem("Figura", "Esfera", control));
		menu.add(crearMenuItem("Figura", "Cilindro", control));
		menu.add(crearMenuItem("Figura", "Prisma", control));
		menuBar.add(menu);

		menu = new JMenu("Rotar");
		menu.add(crearMenuItem("Rotar", "Si", control));
		menu.add(crearMenuItem("Rotar", "No", control));
		menuBar.add(menu);

		return menuBar;
	}
	
	/**
	 * M�todo auxiliar para generar los MenuItems.
	 * @param nombreMenu el nombre del menu al que pertenecer�.
	 * @param textoBoton el nombre del boton que a�adimos.
	 * @param control el controlador asociado.
	 * @return un MenuItem con las caracter�sticas indicadas por los par�metros.
	 */
	private JMenuItem crearMenuItem(String nombreMenu, String textoBoton, ActionListener control) {
		JMenuItem menuItem = new JMenuItem(textoBoton);
		menuItem.addActionListener(control);
		menuItem.setActionCommand(nombreMenu + "|" + textoBoton);
		return menuItem;
	}
	
	
	/**
	 * Controla el ALT+F4 y el boton cerrar.
	 * @param la ventana cuyos mensajes de carrado manejaremos.
	 */
	private static void a�adirControlCerrar(JFrame frame) {
		// disable automatic close support for Swing frame.
		frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

		// adds the window listener
		frame.addWindowListener(new WindowAdapter() {
			// handles the system exit window message
			public void windowClosing(WindowEvent e) {
				System.exit(1);
			}
		});
	}
	
	//acciones de los menus
	/**
	 * Guarda el contenido del Canvas como un JPG.
	 */
	protected void guardarImagen() {
		offScreenCanvas3D.renderOffScreenBuffer();
		offScreenCanvas3D.waitForOffScreenRendering();
		System.out.println("Guardando como JPG");

		try {
			FileOutputStream fileOut = new FileOutputStream("image.jpg");

			JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(fileOut);
			encoder.encode(imageComponent.getImage());

			fileOut.flush();
			fileOut.close();
		} catch (Exception e) {
			System.err.println("Error al guardar: " + e);
		}
		System.out.println("Se ha guardado la imagen.");
	}
	
	/**
	 * Borra todas las figuras del panel.
	 */
	public void borrarRender(){
		borrarFigura("Cubo");
		borrarFigura("Esfera");
		borrarFigura("Cilindro");
		borrarFigura("Prisma");
	}
	
	/**
	 * Cambia la figura mostrada.
	 * @param s una cadena indicando el tipo de figura a mostrar.
	 */
	public void cambiarFigura(String s){
		borrarRender();
		if(s.equals("Cubo")){
			this.sceneBranchGroup.addChild(this.crearCubo());
		}else{
			if(s.equals("Esfera")){
				this.sceneBranchGroup.addChild(this.crearEsfera());
			}else{
				if(s.equals("Cilindro")){
					this.sceneBranchGroup.addChild(this.crearCilindro());
				}else{
					if(s.equals("Prisma")){
						this.sceneBranchGroup.addChild(this.crearPrisma());
					}
				}
			}
		}
		
	}
	
	/**
	 * M�todo principal.
	 * @param args irrelevantes en este caso.
	 */
	public static void main(String[] args) {
		JPopupMenu.setDefaultLightWeightPopupEnabled(false);

		ToolTipManager ttm = ToolTipManager.sharedInstance();
		ttm.setLightWeightPopupEnabled(false);

		JFrame ventana = new JFrame();

		PanelRender figuras = new PanelRender();
		ControlRender c = new ControlRender(figuras);
		figuras.a�adirControlador(c);
		ventana.setJMenuBar(figuras.crearBarraMenu());

		ventana.getContentPane().add(figuras);
		ventana.setSize(550, 550);
		a�adirControlCerrar(ventana);

		ventana.setVisible(true);
	}

}
