package model.render;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ControlRender implements ActionListener{
	
	//ATRIBUTOS
	/**
	 * La ventana que controlar�.
	 */
	private PanelRender ventanaControlada;
	
	
	
	//METODOS
	/**
	 * Constructor con un par�metro.
	 * @param v la ventana que controlar�.
	 */
	public ControlRender(PanelRender v){
		ventanaControlada = v;
	}
	
	/**
	 * AWT callback to indicate that an items has been selected from a menu.
	 */
	public void actionPerformed(ActionEvent ae) {
		System.out.println("Acci�n realizada: " + ae.getActionCommand());

		java.util.StringTokenizer toker = new java.util.StringTokenizer(ae
				.getActionCommand(), "|");

		String menu = toker.nextToken();
		String command = toker.nextToken();

		if (menu.equals("Archivo")) {
			if (command.equals("Salir")) {
				System.exit(0);
			} else if (command.equals("Guardar Imagen")) {
				ventanaControlada.guardarImagen();
			}
		} else if (menu.equals("Figura")) {
			if (command.equals("Cubo")) {
				ventanaControlada.borrarFigura("Esfera");
				ventanaControlada.borrarFigura("Cilindro");
				ventanaControlada.borrarFigura("Prisma");
				ventanaControlada.sceneBranchGroup.addChild(ventanaControlada.crearCubo());
			} else if (command.equals("Esfera")) {
				ventanaControlada.borrarFigura("Cubo");
				ventanaControlada.borrarFigura("Cilindro");
				ventanaControlada.borrarFigura("Prisma");
				ventanaControlada.sceneBranchGroup.addChild(ventanaControlada.crearEsfera());
			} else if (command.equals("Cilindro")) {
				ventanaControlada.borrarFigura("Cubo");
				ventanaControlada.borrarFigura("Esfera");
				ventanaControlada.borrarFigura("Prisma");
				ventanaControlada.sceneBranchGroup.addChild(ventanaControlada.crearCilindro());
			} else if (command.equals("Prisma")) {
				ventanaControlada.borrarFigura("Cubo");
				ventanaControlada.borrarFigura("Cilindro");
				ventanaControlada.borrarFigura("Esfera");
				ventanaControlada.sceneBranchGroup.addChild(ventanaControlada.crearPrisma());
			}
		} else if (menu.equals("Rotar")) {
			if (command.equals("Si")) {
				ventanaControlada.rotator.setEnable(true);
			} else if (command.equals("No")) {
				ventanaControlada.rotator.setEnable(false);
			}
		}
	}

}
