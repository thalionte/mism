package model;

import interfaz.Window;

import javax.swing.ImageIcon;

/**
 * Clase con m�todos auxiliares.
 * @author M� Angeles Broull�n Lozano y Ana M� Fern�ndez Hern�ndez.
 * @version 2.0
 */
public class Auxiliar {
	
	//METODOS
	/** 
	 * M�todo que devuelve una imagen lista para su uso, al recibir su ruta.
	 * @return un ImageIcon, o null si la ruta no es v�lida.
	 */
    public static ImageIcon crearIcono(String path, String description) {
        java.net.URL imagenURL = Window.class.getResource(path);
        if (imagenURL != null) {
            return new ImageIcon(imagenURL, description);
        } else {
            System.err.println("No se pudo encontrar: " + path);
            return null;
        }
    }
    
    /**
     * Separa una cadena de xml de carga en 3 subcadenas.
     * @param entrada la entrada a separar,
     * @return una cadena de xml de carga en 3 subcadenas.
     */
    public static String[] separarTestCanvasDesc(String entrada){
    	String[] salida = new String[3];
 
    	String[] array = entrada.split(" ");

    	String test = obtener("Test", array);
    	String canvas = obtener("Canvas", array);
    	String descripcion = obtener("Desc", array);
    	
    	salida[0] = test.trim();
    	salida[1] = canvas.trim();
    	salida[2] = descripcion.trim();
    	
    	return salida;
    }
    
    /**
     * Separa una cadena de test en sus 5 campos.
     * @param la cadena de entrada en xml.
     * @return  una cadena de test separada en sus campos.
     */
    public static String[] separarTest(String entrada){
    	String[] salida = new String[5];
    	
    	String[] array = entrada.split(" ");
    	String name = obtener("Name", array);
    	String timeLength = obtener("TimeLength", array);
    	String timeLengthUnits = obtener("TimeLengthUnits", array);
    	String start = obtener("Start", array);
    	String end = obtener("End", array);
    	
    	salida[0] = name.trim();
    	salida[1] = timeLength.trim();
    	salida[2] = timeLengthUnits.trim();
    	salida[3] = start.trim();
    	salida[4] = end.trim();    	
    	
    	return salida;
    }
    
    /**
     * Separa una cadena de canvas en sus 2 campos.
     * @param la cadena de entrada en xml.
     * @return  una cadena de canvas separada en sus campos.
     */
    public static String[] separarCanvas(String entrada){
    	String[] salida = new String[2];
    	
    	String[] array = entrada.split(" ");
    	String vertices = obtener("Vertices", array);
    	String edges = obtener("Edges", array);
    	
    	salida[0] = vertices.trim();
    	salida[1] = edges.trim();
    	
    	return salida;
    }
    
    /**
    * Separa una cadena de lista en sus diversos campos.
    * @param la cadena de entrada en xml.
    * @return una cadena de lista en sus diversos campos.
    */
    public static String[] separarLista(String entrada){
    	String[] array = entrada.split(" @@@ ");
    	return array;
    }
    
    /**
     * Separa una cadena de vertice en sus 4 campos.
     * @param la cadena de entrada en xml.
     * @return  una cadena de vertice separada en sus campos.
     */
    public static String[] separarVertice(String entrada){
    	String[] salida = new String[4];
    	
    	String[] array = entrada.split(" ");
    	String name = obtener("Name", array);
    	String type = obtener("Type", array);
    	String posX = obtener("PosX", array);
    	String posY = obtener("PosY", array);
    	
    	salida[0] = name.trim();
    	salida[1] = type.trim();
    	salida[2] = posX.trim();
    	salida[3] = posY.trim();
    	
    	return salida;
    }
    
    /**
     * Separa una cadena de arista en sus 4 campos.
     * @param la cadena de entrada en xml.
     * @return una cadena de arista separada en sus campos.
     */
    public static String[] separarArista(String entrada){
    	String[] salida = new String[4];
    	
    	String[] array = entrada.split(" ");
    	String name = obtener("Name", array);
    	String v1 = obtener("V1", array);
    	String v2= obtener("V2", array);
    	String type = obtener("TypeAr", array);
    	
    	salida[0] = name.trim();
    	salida[1] = v1.trim();
    	salida[2] = v2.trim();
    	salida[3] = type.trim();
    	
    	return salida;
    }
    
    /**
     * Devuelve la extensi�n de un archivo.
     * @param entrada el nombre del archivo completo.
     * @return la extensi�n del archivo.
     */
    public static String separarExtensionArchivo(String entrada){
    	String salida = "";
    	boolean b = false;
    	int i = entrada.length()-1;
    	while(!b && i>0){
    		char c =  entrada.charAt(i);
    		if (c == '.'){
    			b = true;
    			salida = entrada.substring(i+1, entrada.length());
    		}
    		i--;
    	}
    	return salida;
    }
    
    /**
     * Devuelve el tipo de una pieza.
     * @param entrada el nombre de la pieza y su tipo.
     * @return el tipo de una pieza.
     */
    public static String separarTipo(String entrada){
    	String salida = "";
    	boolean b = false;
    	int i = 0;
    	while(!b && i<entrada.length()){
    		char c =  entrada.charAt(i);
    		if (c == ':'){
    			b = true;
    			salida = entrada.substring(i+2, entrada.length());
    		}
    		i++;
    	}
    	return salida;
    }
    
  //auxilair de deserializacion
	/**
	 * Obtiene el enlace.
	 * @param tag el tag que delimitara el contenido.
	 * @param array el array en el que buscar.
	 * @return el enlace deserializado.
	 */
	private static String obtener(String tag, String[] array){
		//busca el identificador de inicio del tag
		int inicio = 0;
		while(!(array[inicio].equals("<"+tag+">"))){
			inicio++;
		}
		//busca el identificador de fin del tag
		int fin = inicio;
		while(!(array[fin].equals("<\\" + tag + ">"))){
			fin++;
		}
		//transcribe lo que hay entre ambos
		String salida = "";
		for (int i=inicio+1; i<fin; i++){
			salida = salida + array[i] + " ";
		}
		return salida;
	}
	
	/**
	 * M�todo que detecta si una cadena es num�rica (numeros 0-9 y un posible punto).
	 * @param in la cadena que queremos reconocer.
	 * @return si es numerica.
	 */
	public static boolean esNumerica(String s){
		boolean b = true;
		boolean salida = false;
		int i=0;
		int puntos = 0; 
		while(b && puntos <2 && i<s.length()){
			char c = s.charAt(i);
			if (c == '.'){
				puntos++;
				if (puntos >= 2){
					b = false;
				}
				i++;
			}else{
				b = b && esNumero(c);
				i++;
			}			
		}
		if(b){
			salida = true;
		}
		return salida;		
	}
	
	/**
	 * Comprueba si un caracter es un n�mero o no.
	 * @param c el caracter se entrada a comprobar.
	 * @return si es un n�mero o no.
	 */
	private static boolean esNumero(char c){
		boolean b = (c>=48 && c<=57);
		return b;
	}
	
	public static void main(String[] args){
    	String s = "<Test> <Name> Test1 <\\Name> <TimeLength> 10 <\\TimeLength> <TimeLengthUnits> ms <\\TimeLengthUnits> <Start> 12:00 <\\Start> <End> 12:10 <\\End> <\\Test> <Canvas> <Vertices> <Vertex> <Name> CP-1: machine1 <\\Name> <Type> machine <\\Type> <PosX> 178 <\\PosX> <PosY> 120 <\\PosY> <Length> 103 <\\Length> <Width> 10 <\\Width> <\\Vertex> @@@ <Vertex> <Name> CP-2: cooling1 <\\Name> <Type> cooling system <\\Type> <PosX> 10 <\\PosX> <PosY> 10 <\\PosY> <Length> 103 <\\Length> <Width> 10 <\\Width> <\\Vertex> <\\Vertices> <Edges> <Edge> <Name> R-3 <\\Name> <V1> <Vertex> <Name> CP-1: machine1 <\\Name> <Type> machine <\\Type> <PosX> 178 <\\PosX> <PosY> 120 <\\PosY> <Length> 103 <\\Length> <Width> 10 <\\Width> <\\Vertex> <\\V1> <V2> <Vertex> <Name> CP-2: cooling1 <\\Name> <Type> cooling system <\\Type> <PosX> 10 <\\PosX> <PosY> 10 <\\PosY> <Length> 103 <\\Length> <Width> 10 <\\Width> <\\Vertex> <\\V2> <TypeAr> parte <\\TypeAr> <\\Edge> <\\Edges> <\\Canvas> <Desc> The description of our experiment <\\Desc>";
    	    	
    	String[] test1 = separarTestCanvasDesc(s);
    	System.out.println("\n\n PRIMERA SEPARACION");
    	System.out.println(test1[0]);
    	System.out.println(test1[1]);
    	System.out.println("+" + test1[2] + "+");
    	
    	String[] test2 = separarTest(test1[0]);
    	System.out.println("\n\n SEPARACION DE TEST");
    	System.out.println("+" + test2[0] + "+");
    	System.out.println("+" + test2[1] + "+");
    	System.out.println("+" + test2[2] + "+");    	
    	System.out.println("+" + test2[3] + "+");
    	System.out.println("+" + test2[4] + "+");
    	
    	String[] test3 = separarCanvas(test1[1]);
    	System.out.println("\n\n SEPARACION DE CANVAS");
    	System.out.println("+" + test3[0] + "+");
    	System.out.println("+" + test3[1] + "+");
    	
    	String[] test4 = separarLista(test3[0]);
    	System.out.println("\n\n SEPARACION DE VERTICES");
    	for (int i=0; i<test4.length; i++){
    		System.out.println("+" + test4[i] + "+");
    		System.out.println("\nUN VERTICE");
    		String[] vert = separarVertice(test4[i]);
    		for (int j=0; j<vert.length; j++){
    			System.out.println("+" + vert[j] + "+");
    		}
    	}
    	
    	String[] test5 = separarLista(test3[1]);
    	System.out.println("\n\n SEPARACION DE ARISTAS");
    	for (int i=0; i<test5.length; i++){
    		System.out.println("+" + test5[i] + "+");
    		System.out.println("\nUNA ARISTA");
    		String[] ar = separarArista(test5[i]);
    		for (int j=0; j<ar.length; j++){
    			System.out.println("+" + ar[j] + "+");
    		}
    	}
    	
	}

}

