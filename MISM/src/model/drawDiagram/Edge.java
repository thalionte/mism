package model.drawDiagram;

import java.awt.Color;
import java.awt.Graphics;

public class Edge {
	//ATRIBUTOS
	/**
	 * V�rtice con las coordenada (x1,y1), un extremo.
	 */
	private Vertex v1;
	/**
	 * V�rtice con las coordenada (x2,y2), un extremo.
	 */
	private Vertex v2;
	/**
	 * Color del �rea.
	 */
	private Color color;
	/**
	 * El nombre del v�rtice
	 */
	private String nombre;
	/**
	 * El tipo de arista
	 */
	private String tipo;
	
	//M�TODOS
	
	//constructor
	/**
	 * Constructor con par�metros.
	 * @param v1 v�rtice con las coordenadas (x1,y1), un extremo.
	 * @param v2 v�rtice con las coordenadas (x2,y2), un extremo.
	 * @param n el nombre de la arista.
	 * @param t el tipo de la arista.
	 */
	public Edge(Vertex v1, Vertex v2, String n, String t) {
		this.v1 = v1;
		this.v2 = v2;
		nombre = n;
		tipo = t;
		if (t.equals("parte")){
    		color = Color.RED;
    	}else{
    		color = Color.BLACK;
    	}
	}

	//getters
	/**
	 * Devuelve si un vertice es extremo de la arista, actualizando su posici�n.
	 * @param v el v�rtice a comprobar.
	 * @return true si v es un v�rtice de esta arista, false en caso contrario.
	 */
	public boolean pertenece(Vertex v) {
		boolean b = false;
		if (v1.equals(v)){
			setPosicion(v, v2);
			b = true;
		}else{
			if (v2.equals(v)){
				setPosicion(v1, v);
				b = true;
			}
		}
		return b;
	}

	/**
	 * Devuelve la coordenada x del primer extremo de la arista. 
	 * @return la coordenada x del primer extremo de la arista.
	 */
	public int getX1() {
		return v1.getX();
	}

	/**
	 * Devuelve la coordenada y del primer extremo de la arista. 
	 * @return la coordenada y del primer extremo de la arista.
	 */
	public int getY1() {
		return v1.getY();
	}
	
	/**
	 * Devuelve la coordenada x del segundo extremo derecha de la arista. 
	 * @return la coordenada x del segundo extremo de la arista.
	 */
	public int getX2() {
		return v2.getX();
	}

	/**
	 * Devuelve la coordenada y del segundo extremo de la arista. 
	 * @return la coordenada y del segundo extremo de la arista.
	 */
	public int getY2() {
		return v2.getY();
	}
	
	//setters
	/**
	 * Fija los extremos de la arista.
	 * @param v1 el nuevo primer extremo.
	 * @param v2 el nuevo segundo extremo.
	 */
	public void setPosicion(Vertex v1, Vertex v2) {
		this.v1 = v1;
		this.v2 = v2;
	}
	
	/**
	 * Dibuja la arista en el Graphics que se le pasa. 
	 * @param g Graphics en el que dibujar.
	 */
	public void dibujar(Graphics g) {
	    //dibuja la arista
	    g.setColor(color);
	    g.drawLine(getX1(), getY1(), getX2(), getY2());
	}
	
	//m�todos heredados de Object.
	/**
	 * Compara si un objeto es igual la instancia actual.
	 * @param o  el objeto a comparar.
	 * @return true si o es igual a la instancia actual, false en caso contrario.
	 */
	public boolean equals(Object o){
		boolean b = false;
		if (o instanceof Vertex){
			Edge e = (Edge) o;
			b = (e.nombre).equals(nombre);
		}
		return b;
	}
	
	/**
	 * Devuelve el contenido de la instancia como una cadena de caracteres.
	 * @return el contenido de la instancia como una cadena de caracteres.
	 */
	public String toString(){
		String s = "<Edge> ";
		s = s + "<Name> " + nombre + " <\\Name> ";
		s =  s + "<V1> " + v1.toString() + " <\\V1> ";
		s =  s + "<V2> " + v2.toString() + " <\\V2> ";
		s = s + "<TypeAr> " + tipo + " <\\TypeAr> ";
		s = s + "<\\Edge>";
		return s;
	}
}
