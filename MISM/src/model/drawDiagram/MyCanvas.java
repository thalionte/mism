package model.drawDiagram;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.FileOutputStream;
import java.util.LinkedList;
import java.util.List;

import javax.imageio.ImageIO;

/**
 * Canvas sobre el que arrastrar los elementos.
 * @author F�lix del Rio Benigno y M� Angeles Broull�n Lozano.
 * @version 2.0
 */
public class MyCanvas extends Canvas implements MouseMotionListener{

	//ATRIBUTOS
	/**
	 * El serial del Canvas.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Un numerador de elementos en el gr�fico, para identificar.
	 */
	private static int numElem = 1;
	/**
	 * Un numerador de aristasen el gr�fico, para identificar.
	 */
	private static int numArista = 1;	
	/** 
     * Lista de vertices a dibujar.
     */
    private LinkedList<Vertex> listaVertices = new LinkedList<Vertex>();
    
    /** 
     * Lista de aristas a dibujar.
     */
    private LinkedList<Edge> listaAristas = new LinkedList<Edge>();

    /**
     * Si actualmente se est� arrastrando o no la figura.
     */
    private Vertex verticeArrastrandose = null;

    /**
     * La coordenada x en la que estaba anteriormente el rat�n.
     */
    private int xAnteriorRaton;

    /**
     * La coordenada y en la que estaba anteriormente el rat�n.
     */
    private int yAnteriorRaton;

    
    //M�TODOS
    
    //constructor
    /**
     * Constructor.
     */
    public MyCanvas() {
    	listaVertices = new LinkedList<Vertex>();
    	listaAristas = new LinkedList<Edge>();
    	addMouseMotionListener(this);
    	this.setSize(500, 550);
    	this.setBackground(Color.WHITE);
    	//test();
    }
    
    //setters      
    /**
     * Le da un tama�o por defecto al Canvas de dibujo.
     * @return Dimension por defecto.
     */
    public Dimension getPreferredSize() {
        return new Dimension(500, 500);
    }
    
    /**
     * A�ade un nuevo v�rtice, creado desde los par�metros de entrada.
     * @param tipoElem el tipo de elemento que representar�.
     * @param nombreComp el nombre del componente que representar� (nombre de la m�quina, como KernEVO, etc...).
     */
    public void addNuevoVerticeAuto(String tipoElem, String nombreComp){
    	String nombre = "CP-" + numElem + ": " + nombreComp;
    	numElem++;
    	this.addNuevoVertice(tipoElem, nombre);
    }
    
    /**
     * A�ade un nuevo v�rtice, creado desde los par�metros de entrada.
     * @param tipoElem el tipo de elemento que representar�.
     * @param nombre el nombre de la etiqueta del v�rtice.
     */
    private void addNuevoVertice(String tipoElem, String nombre){
    	int posX = 10;
    	int posY = 10;
    	Vertex v1 = new Vertex(posX, posY, nombre, tipoElem);
    	if (tipoElem.equals("machine") || tipoElem.equals("maquina")){
    		v1 = new Vertex(posX, posY, nombre, tipoElem);
    	}else{
    		if (tipoElem.equals("tool") || tipoElem.equals("herramienta")){
        		v1 = new Vertex(posX, posY, nombre, tipoElem);
        	}else{
        		if (tipoElem.equals("part") || tipoElem.equals("pieza")){
            		v1 = new Vertex(posX, posY, nombre,tipoElem);
            	}else{
            		if (tipoElem.equals("cooling system") || tipoElem.equals("sistema de enfriamiento")){
                		v1 = new Vertex(posX, posY, nombre, tipoElem);
                	}else{
                		if (tipoElem.equals("sensor")){
                    		v1 = new Vertex(posX, posY, nombre, tipoElem);
                    	}else{
                    		if (tipoElem.equals("quantity") || tipoElem.equals("cantidad")){
                        		v1 = new Vertex(posX, posY, nombre, tipoElem);
                        	}else{
                        		
                        	}
                    	}
                	}
            	}
        	}
    	}
        this.addVertice(v1);
        repaint();
    }

    /**
	 * A�ade un nuevo vertice.
	 * @param x coordenada x de la esquina superior izquierda
	 * @param y coordenada y de la esquina superior izquierda
	 * @param n el nombre del vertice.
	 * @param t el tipo del vertice.
	 */
    public void addVertice(int x, int y, String n, String t){
    	Vertex v = new Vertex(x, y, n, t);
    	addVertice(v);
    	repaint();
    }
    
    /**
     * A�ade una figura a la lista de vertices a dibujar.
     * @param v el nuevo vertice a dibujar.
     */
    protected void addVertice(Vertex v){
        listaVertices.add(v);
    }

    /**
     * Quita el vertice en la lista de vertices a dibujar.
     * @param v el vertice a quitar de la lista.
     */
    public void removeVertice(Vertex v) {
        listaVertices.remove(v);
    }
    
    /**
     * Elimina todos los v�rtices.
     */
    public void removeAllVertices(){
    	listaVertices = new LinkedList<Vertex>();
    	repaint();
    }
    
    /**
	 * A�ade una arista.
	 * @param v1 v�rtice con las coordenadas (x1,y1), un extremo.
	 * @param v2 v�rtice con las coordenadas (x2,y2), un extremo.
	 * @param n el nombre de la arista.
	 * @param t el tipo de la arista.
	 */
    public void addArista(Vertex v1, Vertex v2, String n, String t){
    	Edge e = new Edge(v1, v2, n, t);
    	addArista(e);
    	repaint();
    }
    
    /**
     * A�ade una arista a la lista de aristas a dibujar.
     * @param e la nueva arista a dibujar.
     */
    protected void addArista(Edge e){
        listaAristas.add(e);
    }
    
    /**
     * A�ade una arista a la lista de aristas a dibujar.
     * @param v1 el primer v�rtice de la arista.
     * @param v2 el segundo v�rtice de la arista.
     * @param tipoArsita el tipo de la arista para ponerle el color apropiado.
     */
    public void addArista(Vertex v1, Vertex v2, String tipoArista){
    	String nombreAr = "R-" + numElem;
    	numArista++;
    	Edge e1 = new Edge(v1, v2, nombreAr, tipoArista);    	   	
        this.addArista(e1);
        repaint();
    }
    
    /**
     * A�ade una arista a la lista de aristas a dibujar.
     * @param v1 el nombre del primer v�rtice de la arista.
     * @param v2 el nombre del segundo v�rtice de la arista.
     * @param tipoArista el tipo de la arista para ponerle el color apropiado.
     */
    public void addArista(String v1, String v2, String tipoArista){
    	this.addArista(getVertice(v1), getVertice(v2), tipoArista);
    }

    /**
     * Quitala arista la lista de aristas a dibujar.
     * @param e la arista a quitar de la lista.
     */
    public void removeArista(Edge e) {
        listaVertices.remove(e);
    }
    
    /**
     * Elimina todas las aristas.
     */
    public void removeAllAristas(){
    	listaAristas = new LinkedList<Edge>();
    	repaint();
    }
    
    //getters
    /**
     * Devuelve el vertice del nombre preguntado si existe.
     * @param el nombre por el que preguntar.
     * @return el vertice del nombre preguntado si existe.
     */
    private Vertex getVertice(String nombre){
    	Vertex v =  null;
    	boolean b = false;
    	int i = 0;
    	while (!b && i<listaVertices.size()){
    		v = listaVertices.get(i);
    		if (v.getNombre().equals(nombre)){
    			b = true;
    		}
    		i++;
    	}
    	if (!b){
    		v = null;
    	}
    	return v;
    }
    
    /**
     * M�todo observador que devuelve los nombres de los vertices.
     * @return los nombres de los vertices.
     */
    public String[] getListaVertices(){
    	String[] lista = new String[listaVertices.size()];
    	for (int i=0; i<listaVertices.size(); i++){
    		lista[i] = (listaVertices.get(i)).getNombre();
    	}
    	return lista;
    }
    
    /**
     * M�todo observador que devuelve los nombres de los vertices componentes.
     * @return los nombres de los vertices componentes.
     */
    public String[] getListaComponentes(){    	
    	List<String> listaE = new LinkedList<String>();
    	
    	for (int i=0; i<listaVertices.size(); i++){
    		Vertex v = (listaVertices.get(i));
    		if(v.getTipo().equals("machine") || v.getTipo().equals("maquina") || 
    				v.getTipo().equals("tool") || v.getTipo().equals("herramienta") || 
    				v.getTipo().equals("part") || v.getTipo().equals("pieza") || 
    				v.getTipo().equals("cooling system") || v.getTipo().equals("sistema de enfriamiento")){
    			listaE.add(v.getNombre());
    		}    		
    	}
    	
    	String[] lista = new String[listaE.size()];
    	for (int i=0; i<listaE.size(); i++){
    		lista[i] = listaE.get(i);
    	}
    	
    	return lista;
    }
    
    /**
     * M�todo observador que devuelve los nombres de los vertices sensores.
     * @return los nombres de los sensores.
     */
    public String[] getListaSensores(){    	
    	List<String> listaE = new LinkedList<String>();
    	
    	for (int i=0; i<listaVertices.size(); i++){
    		Vertex v = (listaVertices.get(i));
    		if(v.getTipo().equals("sensor")){
    			listaE.add(v.getNombre());
    		}    		
    	}
    	
    	String[] lista = new String[listaE.size()];
    	for (int i=0; i<listaE.size(); i++){
    		lista[i] = listaE.get(i);
    	}
    	
    	return lista;
    }
    
    /**
     * M�todo observador que devuelve los nombres de los vertices sensores.
     * @return los nombres de los sensores.
     */
    public String[] getListaCantidades(){    	
    	List<String> listaE = new LinkedList<String>();
    	
    	for (int i=0; i<listaVertices.size(); i++){
    		Vertex v = (listaVertices.get(i));
    		if(v.getTipo().equals("cantidad") || v.getTipo().equals("quantity")){
    			listaE.add(v.getNombre());
    		}    		
    	}
    	
    	String[] lista = new String[listaE.size()];
    	for (int i=0; i<listaE.size(); i++){
    		lista[i] = listaE.get(i);
    	}
    	
    	return lista;
    }
    
    /**
     * M�todo observador que devuelve los nombres de los vertices sensores.
     * @return los nombres de los sensores.
     */
    public String[] getListaPiezas(){    	
    	List<String> listaE = new LinkedList<String>();
    	
    	for (int i=0; i<listaVertices.size(); i++){
    		Vertex v = (listaVertices.get(i));
    		if(v.getTipo().equals("pieza") || v.getTipo().equals("part")){
    			listaE.add(v.getNombre());
    		}    		
    	}
    	
    	String[] lista = new String[listaE.size()];
    	for (int i=0; i<listaE.size(); i++){
    		lista[i] = listaE.get(i);
    	}
    	
    	return lista;
    }
    
    /**
     * Para ver si el rat�n est� dentro del rect�ngulo.
     * Si est� dentro, puede comenzar el arrastre.
     * @param e el evento de rat�n
     * @return true si el rat�n est� dentro del rect�ngulo, false en caso contrario.
     */
    private Vertex getVertice(MouseEvent e){
        for (Vertex v : listaVertices){
            if (v.estaDentro(e.getX(), e.getY())) {
                return v;
            }
        }
        return null;
    }
    
    /**
     * Recoloca las aristas si un v�rtice pertenece a las mismas.
     * @param v el vertice a comprobar de cara a las aristas.
     */
    private void setAristas(Vertex v){
        for (int i=0; i<listaAristas.size(); i++){
        	Edge e1 = listaAristas.get(i);
            if (e1.pertenece(v)) {
            	//System.out.println("Redibujando arista " + e1.toString());
                //System.out.println("Fin de redibujar aristas");
            }
        }        
    }
    
    //m�todos de Canvas
    /**
     * Dibuja la figura actualmente seleccionada en la posici�n indicada por (x,y), y redibuja el resto.
     * @param g Graphics con el que dibujar.
     */
    public void paint(Graphics g){
        for (Vertex v : listaVertices) {
            v.dibujar(g);
        }
        for (Edge e : listaAristas) {
            e.dibujar(g);
        }
    }

    //m�todos de MouseMotionListener
    /**
     * M�todo al que se llama cuando se arrastra el rat�n.
     * Se comprueba con el atributo verticeArrastrandose si est� empezando el arrastre o
     * ya se esta en medio del mismo.
     * Si se comienza el arrastre, se guardan las coordenadas del rat�n que
     * vienen en el evento MouseEvent y se cambia el valor del atributo arrastrando.
     * Si se est� en medio de un arrastre, se calcula la nueva posici�n del
     * rect�ngulo y se llama al m�todo repaint() para que se pinte.
     * @param e el evento del rat�n.
     */
    public void mouseDragged(MouseEvent e){
        // Si comienza el arrastre ...
        if (verticeArrastrandose == null){
            // Se guardan las posiciones del rat�n
            xAnteriorRaton = e.getX();
            yAnteriorRaton = e.getY();
            // y se marca que ha comenzado el arrastre.
            verticeArrastrandose = getVertice(e);
        }else{
        	// Si ya hab�a empezado el arrastre, se calculan las nuevas coordenadas
        	int x = verticeArrastrandose.getX() + (e.getX() - xAnteriorRaton);
        	int y = verticeArrastrandose.getY() + (e.getY() - yAnteriorRaton);
        	if (salirDelCanvas(x, y)){
        		//no hacer nada
        	}else{        		
                verticeArrastrandose.setPosicion(x, y);

                // Se guarda la posici�n del rat�n para el siguiente c�lculo
                xAnteriorRaton = e.getX();
                yAnteriorRaton = e.getY();
                
                //se actualizan las aristas que lo tienen por extremo
                setAristas(verticeArrastrandose);

                // y se manda repintar el Canvas
                repaint();
        	}            
        }
    }

    /**
     * El rat�n se mueve sin arrastrar. Se marca fin de arrastre.
     * @param e el evento del rat�n.
     */
    public void mouseMoved(MouseEvent e){
        verticeArrastrandose = null;
    }
    
    /**
     * M�todo que indica si en la posici�n actual se sale del canvas.
     * @param x la coordenada x.
     * @param y la coordenada y.
     * @return  si en la posici�n actual se sale del canvas.
     */
    private boolean salirDelCanvas(int x, int y){
    	boolean b = false;
    	if (x<2 || x>490){
    		b = true;
    	}
    	if (y<2 || y>540){
    		b = true;
    	}
    	return b;
    }
    
    /**
     * M�todo para guardar el contenido del canvas en un jpg.
     * @param dir la direccion donde guardar.
     */
    public void save(String dir){
    	Dimension size = this.getSize();

    	// create a buffered image the correct size
    	BufferedImage image = new BufferedImage(size.width, size.height, BufferedImage.TYPE_INT_RGB);

    	// paint the canvas to the image's graphics context
    	Graphics2D graphics = image.createGraphics();
    	this.paintAll(graphics);

    	// tidy up
    	graphics.dispose();

    	// save the buffered image to a file
    	FileOutputStream fos = null;
    	try {
    	fos = new FileOutputStream(dir);
    	ImageIO.write(image, "jpg", fos);
    	fos.close();
    	System.out.println("saved");
    	}
    	catch (Exception e) {
    	e.printStackTrace();
    	}
    }
    
    /**
     * Devuelve el contenido del canvas en formato xml.
     * @return el contenido del canvas en formato xml.
     */
    public String toString(){
    	String salida =  "<Canvas> ";
    	salida = salida + "<Vertices> ";
    	if (!listaVertices.isEmpty()){
    		salida = salida + listaVertices.get(0).toString();
    		for (int i=1; i<listaVertices.size(); i++){
        		salida = salida + " @@@ " + listaVertices.get(i).toString();
        	}
    	}    	
    	salida = salida + " <\\Vertices>";
    	salida = salida + " <Edges> ";
    	if (!listaAristas.isEmpty()){
    		salida = salida + listaAristas.get(0).toString();
    		for (int i=1; i<listaAristas.size(); i++){
        		salida = salida + " @@@ " + listaAristas.get(i).toString();
        	}
    	}
    	salida = salida + " <\\Edges> ";
    	salida =  salida + "<\\Canvas> ";
    	return salida;
    }
    
    
}
