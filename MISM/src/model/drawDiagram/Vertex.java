package model.drawDiagram;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;

public class Vertex{

	//ATRIBUTOS
	/**
	 * Coordenada x del rectangulo, esquina superior izquierda.
	 */
	private int x;

	/**
	 * Coordenada y del rectangulo, esquina superior izquierda.
	 */
	private int y;

	/**
	 * Ancho del rectangulo.
	 */
	private int ancho;

	/**
	 * Alto del alto.
	 */
	private int alto;

	/**
	 * Color del �rea.
	 */
	private Color color;
	
	/**
	 * El nombre del v�rtice
	 */
	private String nombre;
	
	/**
	 * El tipo de v�rtice
	 */
	private String tipo;
	
	//M�TODOS
	
	//constructor
	/**
	 * Constructor con par�metros.
	 * @param x coordenada x de la esquina superior izquierda
	 * @param y coordenada y de la esquina superior izquierda
	 * @param n el nombre del vertice.
	 * @param t el tipo del vertice.
	 */
	public Vertex(int x, int y, String n, String t) {
		this.x = x;
		this.y = y;
		ancho = 10;
		alto = 10;
		nombre = n;
		tipo = t;
		if (t.equals("machine") || t.equals("maquina")){
			color = Color.RED;
    	}else{
    		if (t.equals("tool") || t.equals("herramienta")){
        		color = Color.CYAN;
        	}else{
        		if (t.equals("part") || t.equals("pieza")){
            		color = Color.GREEN;
            	}else{
            		if (t.equals("cooling system") || t.equals("sistema de enfriamiento")){
                		color = Color.ORANGE;
                	}else{
                		if (t.equals("sensor")){
                    		color = Color.YELLOW;
                    	}else{
                    		if (t.equals("quantity") || t.equals("cantidad")){
                        		color = Color.GRAY;
                        	}else{
                        		//nada mas
                        	}
                    	}
                	}
            	}
        	}
    	}
	}

	//getters
	/**
	 * M�todo observador que devuelve el nombre que identifica al v�rtice.
	 * @param return el nombre que identifica al v�rtice.
	 */
	public String getNombre(){
		return nombre;
	}
	
	/**
	 * M�todo observador que devuelve el tipo de v�rtice.
	 * @return el tipo de v�rtice.
	 */
	public String getTipo(){
		return tipo;
	}
	
	/**
	 * Devuelve si (x,y) esta dentro del rectangulo.
	 * @param x coordenada x del punto que se quiere saber si esta dentro del rectangulo.
	 * @param y coordenada y del punto que se quiere saber si esta dentro del rectangulo. 
	 * @return true si (x,y) esta dentro del rectangulo, false en caso contrario.
	 */
	public boolean estaDentro(int x, int y) {
		boolean b = false;
		if ((x > this.x) && (x < (this.x + ancho))
				&& (y > this.y)	&& (y < (this.y + alto))) {
			b = true;
		}

		return b;
	}

	/**
	 * Devuelve la coordenada x de la esquina superior izquierda del rectangulo. 
	 * @return la coordenada x de la esquina superior izquierda del rectangulo.
	 */
	public int getX() {
		return x;
	}

	/**
	 * Devuelve la coordenada y de la esquina superior izquierda del rectangulo. 
	 * @return la coordenada y de la esquina superior izquierda del rectangulo.
	 */
	public int getY() {
		return y;
	}
	
	//setters
	/**
	 * Fija la esquina superior izquierda del rectangulo.
	 * @param x la nueva coordenada x.
	 * @param y la nueva coordenada y.
	 */
	public void setPosicion(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Dibuja el v�rtice en el Graphics que se le pasa. 
	 * @param g Graphics en el que dibujar.
	 */
	public void dibujar(Graphics g) {
		
		Font monoFont = new Font("Monospaced", Font.BOLD | Font.ITALIC, 12);
		g.setFont(monoFont);
	    FontMetrics fm = g.getFontMetrics();
	    int w = fm.stringWidth(nombre);
	    int h = fm.getAscent();
	    
	    //dibuja el recuadro
	    g.setColor(color);
	    ancho = w+5;
	    alto = 10;
		g.fillRect(x, y, ancho, alto);
		
		//dibuja el texto
	    g.setColor(Color.black);
	    g.drawString(nombre, x , y + (3*h / 4));
	}
	
	/**
	 * Compara si un objeto es igual la instancia actual.
	 * @param o  el objeto a comparar.
	 * @return true si o es igual a la instancia actual, false en caso contrario.
	 */
	public boolean equals(Object o){
		boolean b = false;
		if (o instanceof Vertex){
			Vertex v = (Vertex) o;
			b = (v.nombre).equals(nombre);
		}
		return b;
	}
	
	/**
	 * Devuelve el contenido de la instancia como una cadena de caracteres.
	 * @return el contenido de la instancia como una cadena de caracteres.
	 */
	public String toString(){
		String s = "<Vertex> "; 
		s = s + "<Name> " + nombre + " <\\Name> ";
		s = s + "<Type> " + tipo + " <\\Type> "; 
		s =  s + "<PosX> " + x + " <\\PosX> ";
		s =  s + "<PosY> " + y + " <\\PosY> ";
		s =  s + "<\\Vertex>";
		return s;
	}

}
