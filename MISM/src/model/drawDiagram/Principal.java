package model.drawDiagram;

import java.awt.FlowLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

/**
 * Clase principal para hacer una prueba.
 * @author M� Angeles Broull�n Lozano.
 * @version 1.0.
 */
public class Principal {
	
	/**
     * M�todo principal.
     * @param args irrelevantes en este caso.
     */
    public static void main(String[] args) {
        JFrame v = new JFrame("El arrastre... TM");
        MyCanvas c = new MyCanvas();
        //se a�aden los vertices
        Vertex v1 = new Vertex(2, 3, "Vertice 1", "maquina");
        c.addVertice(v1);
        Vertex v2 = new Vertex(7, 22, "Vertice 2", "herramienta");
        c.addVertice(v2);
        Vertex v3 = new Vertex(11, 44, "Vertice 3", "pieza");
        c.addVertice(v3);
        Vertex v4 = new Vertex(6, 8, "Vertice 4", "sistema de enfriamiento");
        c.addVertice(v4);
        Vertex v5 = new Vertex(34, 8, "Vertice 5", "sensor");
        c.addVertice(v5);
        Vertex v6 = new Vertex(64, 8, "Vertice 6", "cantidad");
        c.addVertice(v6);
        //se a�aden las aristas
        Edge e1 = new Edge(v1, v2, "Arista 1", "parte");
        c.addArista(e1);
        
        //y si meto mas cosas?
        JLabel et = new JLabel("Probando, probando, probando");
        
        //y todo a la ventana
        v.getContentPane().setLayout(new FlowLayout());
        v.getContentPane().add(c);
        v.getContentPane().add(et);
        v.pack();
        v.setVisible(true);
        v.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        
    }

}
