package model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Clase para el manejo r�pido de un fichero
 * @author M� Angeles Broull�n Lozano, Ana M� Fern�ndez Hern�ndez
 *
 */
public class FileManager {
	
	/**
	 * M�todo que lee el contenido de un fichero l�nea a l�nea, guardando cada una de ella en un nodo de una lista.
	 * @param ruta la ruta del archivo a leer.
	 * @throws Exception si no se puede acceder al archivo.
	 * @return el contenido de un fichero por l�neas.
	 */
	public static List<String> leer(String ruta) throws Exception{
		List<String> contenido = new LinkedList<String>();
		File archivo = new File(ruta);
		FileReader fr = null;
		BufferedReader br = null;
		String leido = "";
			
		// Lectura del fichero		
		fr = new FileReader (archivo);
		br = new BufferedReader(fr);
		leido = br.readLine();
		while (leido != null){
			contenido.add(leido);
			leido = br.readLine();
		}			
		
		// cerramos el fichero
	    if(fr != null ){   
	    	fr.close();     
	    }                  
	    
		return contenido;
	}
	
	/**
	 * M�todo que lee el contenido de un fichero, guardandolo todo en una cadena.
	 * @param ruta la ruta del archivo a leer.
	 * @throws Excpetion si no se ha podido leer.
	 * @return el contenido de un fichero, todo en una cadena de texto.
	 */
	public static String leerTodo(String ruta) throws Exception{
		String contenido = "";
		List<String> listaContenido = leer(ruta);
		for (int i=0; i<listaContenido.size(); i++){
			contenido = contenido + listaContenido.get(i) + "\n";
		}
		return contenido;
	}
	
	/**
	 * M�todo que escribe el contenido de una lista de cadenas en un fichero, a continuaci�n de lo existente.
	 * @param ruta la ruta del archivo en el que queremos escribir.
	 * @param entrada lista con que contiene en cada nodo las l�neas a escribir.
	 * @throws Excpetion si no se pudo acceder al fichero.
	 */
	public static void escribir(String ruta, List<String> entrada) throws Exception{
		//manejador de fichero y puntero para escribir
		FileWriter fichero = null;
		PrintWriter pw = null;
		Iterator<String> it = entrada.iterator();
		
		//abre a�adiendos la l�neas nuevas al cotenido previo. 
		fichero = new FileWriter(ruta, true);
		pw = new PrintWriter(fichero, true);
		while(it.hasNext()){
			pw.println(it.next());
		}			
		
		
        // nos asegurarnos de que se cierra el fichero.
	    if(null != fichero){
	       fichero.close();
	    }
	    
	}
	
	/**
	 * M�todo que escribe el contenido de una lista de cadenas en un fichero, eliminado todo el contenido previo del fichero.
	 * @param ruta la ruta del archivo en el que queremos escribir.
	 * @param entrada lista con que contiene en cada nodo las l�neas a escribir.
	 * @throws Excpetion si no se puede acceder al fichero.
	 */
	public static void sobreescribir(String ruta, List<String> entrada) throws Exception{
		//manejador de fichero y puntero para escribir
		FileWriter fichero = null;
		PrintWriter pw = null;
		Iterator<String> it = entrada.iterator();
		
		//abre a�adiendos la l�neas nuevas al cotenido previo. 
		fichero = new FileWriter(ruta);
		pw = new PrintWriter(fichero, true);
		while(it.hasNext()){
			pw.println((String) it.next());
		}			
		
        // nos asegurarnos de que se cierra el fichero.
	    if(null != fichero){
	       fichero.close();
	    }
	    
	}
	
}


