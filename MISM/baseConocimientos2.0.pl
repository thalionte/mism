
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Base de conocimientos
%Realizado por:
%M� Angeles Broullon Lozano.
%Ana M� Fernandez Hernandez.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%
%Este programa guarda los datos necesarios para realizar estimaciones de calidad
%%%%%%%%%%%%%%%%%

%MODO DE USO.
%Escribir menu en el promp.

:- dynamic machine/5.
:- dynamic tool/8.
:- dynamic part/3.
:- dynamic cool/4.
:- dynamic quantity/3.
:- dynamic position/4.
:- dynamic rule/6.
:- dynamic mesure/4.


%test(name,pos_X, pos_Y,advancs, spindle, valuequality).		
%part(name, material, hardness)
%cool(name, refri, appli)
%tool(name, material, geometri, nose, n_teeth, n_edges, length, coating)
%machine(name, spindle, power, max_dep, forward)
%sensor(name,element, max, min)
%quantity(name, number)
%position(name,pos_X, pos_Y, advance, spindle)
%Rule(number, element1, element2, mesure(sensor, element1,component,result),mesure(sensor, element2,component,result))
%mesure(name,sensor, element, component, result)
%quality(name, number).

tool('125CNC01.1','acero medio','fresado',48.7, 5,4,50,'dry').
rule(1,'125CNC01.1','placa acero',mesure('laser','125CNC01.1','velocidad corte',375),mesure('laser','longitud',500)).

test(N,X,Y,A,S,V):-
	quality(N,V),
	assert(position(N,X,Y,A,S)).
		
quality(Ex,N):-
   	rule(_,E1,E2,mesure(Ex,S,E1,_,R1),mesure(Ex,S,E2,_,R2)),
   	Y is (R1 + R2)/2,
   	N is (Y * 100).

mesure(Ex,S,E,C,R):-
	((machine(E,C,_,_,_);machine(E,_,C,_,_);machine(E,_,_,C,_);machine(E,_,_,_,C));
         (tool(E,C,_,_,_,_,_,_);tool(E,_,C,_,_,_,_,_);tool(E,_,_,C,_,_,_,_);tool(E,_,_,_,C,_,_,_);
          tool(E,_,_,_,_,C,_,_);tool(E,_,_,_,_,_,C,_);tool(E,_,_,_,_,_,_,C));
	     (part(E,C,_);part(E,_,C))
         (cool(E,C,_,_);cool(E,_,C,_);cool(E,_,_,C));
		 (sensor(N,B,Ma,Mi),
			position(B,X,Y,A,P), ((Ma>X , Mi<X ,Ma>Y, Mi<Y);
								  (Ma>A ,Mi<A);
								  (Ma>P, Mi<P)))),
	 (((sensor(N,C,Ma,Mi),
	  C<Ma,C>Mi), R is 1);
	  R is 0 ).



add(mesure):-
			write('Name:'),nl, read(N),nl,
	( (meaure(N,_,_,_,_), write('Sensor already exists.'));
	  (write('Sensor:'),nl, read(S),nl,
	   write('element:'),nl, read(E),nl,
	   write('Component:'),nl, read(C),nl,
	   write('Result:'),nl, read(R),nl,
	   assertz(sensor(N,S,E,C,R)))).


add(rule):-
	R is 0,!,
	R is (R+1),
	( rule(R,_,_,_), fail;
	 (write('Element1:'),nl,read(E1),nl,
	  write('Element2:'),nl,read(E2),nl,
	  write('Result:'),nl,read(Re),nl,
	  assertz(rule(R,E1,E2,Re)))).



add(machine):-
	write('Name:'),nl, read(N),nl,
	( (machine(N,_,_,_,_), write('Machine already exists.'));
	  (write('Spindle:'),nl, read(S),nl,
	   write('Power:'),nl, read(P),nl,
	   write('Maximun depth:'),nl, read(M),nl,
	   write('Forward:'),nl, read(F),nl,
	   assertz(machine(N,S,P,M,F)))).


add(tool):-
	write('Name:'),nl, read(N),nl,
	( (tool(N,_,_,_,_,_,_,_), write('Tool already exists.'));
	  (write('Material:'),nl, read(M),nl,
	   write('Geometry:'),nl, read(G),nl,
	   write('Nose radius:'),nl, read(NR),nl,
	   write('Number of teeth:'),nl, read(NT),nl,
	   write('Number of edges:'),nl, read(NE),nl,
	   write('Length:'),nl, read(L),nl,
	   write('Coating:'),nl, read(C),nl,
	   assertz(machine(N,M,G,NR,NT,NE,L,C)))).


add(part):-
	write('Name:'),nl, read(N),nl,
	( (part(N,_,_), write('Part already exists.'));
	  (write('Material:'),nl, read(M),nl,
	   write('Hardness:'),nl, read(H),nl,
	   assertz(machine(N,M,H)))).

add(cool):-
	write('Name:'),nl, read(N),nl,
	( (cool(N,_,_), write('Cool already exists.'));
	  (write('Refrigerant:'),nl, read(R),nl,
	   write('Way of application:'),nl, read(W),nl,
	   assertz(machine(N,R,W)))).


add(sensor):-
	write('Name:'),nl, read(N),nl,
	( (cool(N,_,_), write('Sensor already exists.'));
	  (write('Maximun:'),nl, read(Ma),nl,
	   write('Minimun:'),nl, read(Mi),nl,
	   write('Meassurement units:'),nl, read(Me),nl,
	   assertz(machine(N,Ma,Mi,Me)))).


add(quantity):-
	write('Name:'),nl, read(N),nl,
	( (quantity(N,_,_), write('Quantityl already exists.'));
	  (write('Number:'),nl, read(Nu),nl,
	   write('Unit:'),nl, read(U),nl,
	   assertz(machine(N,Nu,U)))).


